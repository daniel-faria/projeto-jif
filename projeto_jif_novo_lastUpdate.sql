-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema projetojif
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema projetojif
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `projetojif` DEFAULT CHARACTER SET utf8 ;
USE `projetojif` ;

-- -----------------------------------------------------
-- Table `projetojif`.`aluno`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetojif`.`aluno` (
  `ra` VARCHAR(15) NOT NULL,
  `nome` VARCHAR(50) NOT NULL,
  `data_nascimento` DATE NOT NULL,
  `sexo` VARCHAR(2) NOT NULL,
  `rg` VARCHAR(20) NOT NULL,
  `campus` VARCHAR(50) NOT NULL,
  `telefone` VARCHAR(45) NOT NULL,
  `foto` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`ra`),
  UNIQUE INDEX `rg` (`rg` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `projetojif`.`foto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetojif`.`foto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(100) NULL DEFAULT NULL,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `nome`),
  UNIQUE INDEX `nome` (`nome` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `projetojif`.`jogo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetojif`.`jogo` (
  `dataHoraInicio` DATETIME NOT NULL,
  `dataHoraFim` DATETIME NOT NULL,
  `local` VARCHAR(100) NOT NULL,
  `idJogo` INT(11) NOT NULL AUTO_INCREMENT,
  `sumula` VARCHAR(80) NULL,
  PRIMARY KEY (`idJogo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `projetojif`.`modalidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetojif`.`modalidade` (
  `idModalidade` INT(11) NOT NULL AUTO_INCREMENT,
  `esporte` VARCHAR(45) NOT NULL,
  `coletivo` TINYINT(1) NOT NULL,
  `qtdAtleta` INT(11) NOT NULL,
  `masculino` TINYINT(1) NOT NULL,
  PRIMARY KEY (`idModalidade`))
ENGINE = InnoDB
AUTO_INCREMENT = 25
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `projetojif`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetojif`.`usuario` (
  `cpf` VARCHAR(15) NOT NULL,
  `nome` VARCHAR(50) NOT NULL,
  `siape` VARCHAR(15) NULL DEFAULT NULL,
  `email` VARCHAR(45) NOT NULL,
  `campus` VARCHAR(50) NULL DEFAULT NULL,
  `telefone` VARCHAR(45) NULL DEFAULT NULL,
  `senha` VARCHAR(60) NOT NULL,
  `permissao` VARCHAR(45) NOT NULL,
  `data_ultimo_login` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00',
  `data_cadastro` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`cpf`),
  UNIQUE INDEX `email` (`email` ASC),
  UNIQUE INDEX `cpf` (`cpf` ASC),
  UNIQUE INDEX `siape` (`siape` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `projetojif`.`postagem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetojif`.`postagem` (
  `idpostagem` INT(11) NOT NULL,
  `dataHora` DATETIME NOT NULL,
  `titulo` VARCHAR(80) NOT NULL,
  `texto` LONGTEXT NOT NULL,
  `foto` VARCHAR(45) NULL DEFAULT NULL,
  `Usuario_cpf` VARCHAR(15) NOT NULL,
  `aprovada` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`idpostagem`),
  INDEX `fk_postagem_Usuario1_idx` (`Usuario_cpf` ASC),
  CONSTRAINT `fk_postagem_Usuario1`
    FOREIGN KEY (`Usuario_cpf`)
    REFERENCES `projetojif`.`usuario` (`cpf`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `projetojif`.`time`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetojif`.`time` (
  `idTime` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeTime` VARCHAR(50) NOT NULL,
  `Tecnico_cpf` VARCHAR(15) NOT NULL,
  `Modalidade_idModalidade` INT(11) NOT NULL,
  PRIMARY KEY (`idTime`),
  INDEX `fk_Time_Tecnico1_idx` (`Tecnico_cpf` ASC),
  INDEX `fk_Time_Modalidade1_idx` (`Modalidade_idModalidade` ASC),
  CONSTRAINT `fk_Time_Modalidade1`
    FOREIGN KEY (`Modalidade_idModalidade`)
    REFERENCES `projetojif`.`modalidade` (`idModalidade`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Time_Tecnico1`
    FOREIGN KEY (`Tecnico_cpf`)
    REFERENCES `projetojif`.`usuario` (`cpf`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 18
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `projetojif`.`time_has_aluno`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetojif`.`time_has_aluno` (
  `time_idTime` INT(11) NOT NULL,
  `aluno_ra` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`time_idTime`, `aluno_ra`),
  INDEX `fk_time_has_aluno_aluno1_idx` (`aluno_ra` ASC),
  INDEX `fk_time_has_aluno_time1_idx` (`time_idTime` ASC),
  CONSTRAINT `fk_time_has_aluno_aluno1`
    FOREIGN KEY (`aluno_ra`)
    REFERENCES `projetojif`.`aluno` (`ra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_time_has_aluno_time1`
    FOREIGN KEY (`time_idTime`)
    REFERENCES `projetojif`.`time` (`idTime`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `projetojif`.`placar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetojif`.`placar` (
  `time_idTime` INT(11) NOT NULL,
  `jogo_idJogo` INT(11) NOT NULL,
  `placar` INT NULL,
  PRIMARY KEY (`time_idTime`, `jogo_idJogo`),
  INDEX `fk_time_has_jogo_jogo1_idx` (`jogo_idJogo` ASC),
  INDEX `fk_time_has_jogo_time1_idx` (`time_idTime` ASC),
  CONSTRAINT `fk_time_has_jogo_time1`
    FOREIGN KEY (`time_idTime`)
    REFERENCES `projetojif`.`time` (`idTime`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_time_has_jogo_jogo1`
    FOREIGN KEY (`jogo_idJogo`)
    REFERENCES `projetojif`.`jogo` (`idJogo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
