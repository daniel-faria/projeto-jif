<?php

namespace JIF\Rotas;

//arquivo separado
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$rotas = new RouteCollection();

//Inicio
$rotas->add('inicio', new Route('/', array('_controller' => 'JIF\Controller\ControllerIndex', '_method' => 'show')));
$rotas->add('logado', new Route('/painel', array('_controller' => 'JIF\Controller\ControllerIndex', '_method' => 'showDashboard')));
$rotas->add('showFotos', new Route('/fotos', array('_controller' => 'JIF\Controller\ControllerIndex', '_method' => 'showFotos')));
$rotas->add('fotoGravar', new Route('/fotos/gravar', array('_controller' => 'JIF\Controller\ControllerIndex', '_method' => 'fotoGravar')));
$rotas->add('buscarCracha', new Route('/cracha', array('_controller' => 'JIF\Controller\ControllerIndex', '_method' => 'showCracha')));

$rotas->add('buscarCrachaEfetivar', new Route('/cracha/search', array('_controller' => 'JIF\Controller\ControllerIndex', '_method' => 'searchCracha')));

//Tecnico
$rotas->add('cadastrarTecnico', new Route('/tecnico-cadastro', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'cadastrarTecnico')));
$rotas->add('formCadastroTecnico', new Route('/cadastro/efetivar', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'formCadastroTecnico')));


//Admin 
//Usuário (administrador, tecnico, aluno)
$rotas->add('telaTecnicoAdmin', new Route('/painel/cadastro', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'telaTecnicoAdmin')));
$rotas->add('inserirTecnicoAdmin', new Route('/painel/cadastro/inserir', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'inserirTecnicoAdmin')));
$rotas->add('telaAtualizaTecnicoAdmin', new Route('/painel/update', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'telaAtualizaTecnicoAdmin')));
$rotas->add('atualizaTecnicoAdmin', new Route('/painel/update/efetivar', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'atualizaTecnicoAdmin')));
$rotas->add('telaRemoveTecnicoAdmin', new Route('/painel/remove', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'telaRemoveTecnicoAdmin')));
$rotas->add('removeTecnicoAdmin', new Route('/painel/remove/efetivar', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'removeTecnicoAdmin')));
$rotas->add('telaBuscaTecnicoAdmin', new Route('/painel/view', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'telaBuscaTecnicoAdmin')));
$rotas->add('busca', new Route('/painel/busca', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'busca')));
$rotas->add('buscaEfetivar', new Route('/painel/busca/efetivar', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'buscaEfetivar')));
$rotas->add('telaAlterarSenha', new Route('/painel/profile', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'telaSenha')));
$rotas->add('telaUpdateSenha', new Route('/painel/profile/efetivar', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'formAlteraSenha')));
//Login
$rotas->add('login', new Route('/login', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'login')));
$rotas->add('loginEfetivar', new Route('/login/efetivar', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'loginEfetivar')));
$rotas->add('logoff', new Route('/exit', array('_controller' => 'JIF\Controller\ControllerUsuario', '_method' => 'logoff')));



//CRUD alunos modificado

$rotas->add('telaAtualizaAluno', new Route('/painel/aluno/update/{id}', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'telaAtualizaAluno', "id" => "")));

$rotas->add('telaBuscaEfetivadaAluno', new Route('/painel/aluno/view', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'telaBuscaAluno')));
$rotas->add('telaRemoveAluno', new Route('/painel/aluno/remove', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'telaRemoveAluno')));

$rotas->add('cadastroAluno', new Route('/painel/aluno', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'viewAluno')));
$rotas->add('gravarAluno', new Route('/painel/aluno/gravar', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'gravarAluno')));
$rotas->add('atualizarAluno', new Route('/painel/aluno/atualizar/efetivar', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'atualizarAluno')));

$rotas->add('telaMostrar', new Route('/painel/aluno/mostrar', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'telaMostrar')));
$rotas->add('buscarAluno', new Route('/painel/aluno/buscar', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'buscarAluno')));

$rotas->add('telaAlterarAluno', new Route('/painel/aluno/alterar', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'pesquisarAluno')));

$rotas->add('removeRemoveAluno', new Route('/painel/aluno/remove/efetivar', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'removerAluno')));


$rotas->add('buscaEfetivarAluno', new Route('/painel/aluno/busca/efetivar', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'buscaEfetivarAluno')));

$rotas->add('deleteAluno', new Route('/painel/aluno/delete/{id}', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'deleteAluno', "id" => "")));


$rotas->add('relatorioAlunos', new Route('/painel/aluno/relatorio', array('_controller' => 'JIF\Controller\ControllerAluno', '_method' => 'relatorio')));

//Modalidade

$rotas->add('cadastrarModalidade', new Route('/painel/modalidade/cadastrar/{id}', array('_controller' => 'JIF\Controller\ControllerModalidade', '_method' => 'cadastrarModalidade', "id" => "")));
$rotas->add('salvarModalidade', new Route('/painel/modalidade/salvar', array('_controller' => 'JIF\Controller\ControllerModalidade', '_method' => 'salvarModalidade')));
$rotas->add('pesquisarModalidade', new Route('/painel/modalidade/pesquisar', array('_controller' => 'JIF\Controller\ControllerModalidade', '_method' => 'showPesquisarModalidade')));
$rotas->add('selectModalidade', new Route('/painel/modalidade/select', array('_controller' => 'JIF\Controller\ControllerModalidade', '_method' => 'selectModalidade')));
$rotas->add("atualizarModalidade", new Route("/painel/modalidade/atualizar/{id}", array("_controller" => "JIF\Controller\ControllerModalidade", "_method" => "atualizarModalidadeById", "id" => "")));
$rotas->add('updateModalidade', new Route('/painel/modalidade/update', array('_controller' => 'JIF\Controller\ControllerModalidade', '_method' => 'atualizarModalidade')));
$rotas->add('exibirModalidade', new Route('/painel/modalidade/exibir/{id}', array('_controller' => 'JIF\Controller\ControllerModalidade', '_method' => 'exibirModalidade', "id" => "")));

$rotas->add('deleteModalidade', new Route('/painel/modalidade/delete/{id}', array('_controller' => 'JIF\Controller\ControllerModalidade', '_method' => 'deleteModalidade', "id" => "")));

$rotas->add("removerModalidade", new Route("/painel/modalidade/remove/", array("_controller" => "JIF\Controller\ControllerModalidade", "_method" => "removerModalidadeById")));

//Times

$rotas->add('telaTime', new Route('/painel/time', array('_controller' => 'JIF\Controller\ControllerTime', '_method' => 'telaTime')));
$rotas->add('formarTime', new Route('/painel/time/formar', array('_controller' => 'JIF\Controller\ControllerTime', '_method' => 'criarTime')));


$rotas->add('telaListarTimes', new Route('/painel/time/edit', array('_controller' => 'JIF\Controller\ControllerTime', '_method' => 'telaListarTimes')));
$rotas->add('telaMostrarTimeEdit', new Route('/painel/time/edit/show', array('_controller' => 'JIF\Controller\ControllerTime', '_method' => 'telaMostrarTimeEdit')));
$rotas->add('removerdoTime', new Route('/painel/time/removeralunos', array('_controller' => 'JIF\Controller\ControllerTime', '_method' => 'removerdoTime')));
$rotas->add('deletaTime', new Route('/painel/time/delete', array('_controller' => 'JIF\Controller\ControllerTime', '_method' => 'deletaTime')));
$rotas->add('salvaTime', new Route('/painel/time/salva', array('_controller' => 'JIF\Controller\ControllerTime', '_method' => 'salvaTime')));
$rotas->add('addAlunoTime', new Route('/painel/time/studentadd', array('_controller' => 'JIF\Controller\ControllerTime', '_method' => 'addAlunoTime')));


//Jogos

$rotas->add('telaJogos', new Route('/jogo/all', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'telaListarJogos')));
$rotas->add('novoJogo', new Route('/jogo/novo', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'criarJogo')));
$rotas->add('removeJogo', new Route('/jogo/delete/{id}', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'removeJogo', "id" => "")));
$rotas->add('telaChangeJogo', new Route('/jogo/change/{id}', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'telaChangeJogo', "id" => "")));
$rotas->add('changeJogo', new Route('/jogo/efetivarAlteracao', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'criarJogo')));
$rotas->add('crachasPDF', new Route('/painel/gerarCrachas', array('_controller' => 'JIF\Controller\ControllerTime', '_method' => 'relatorioEspecial')));
$rotas->add('telaJogoView', new Route('/jogo/view/{id}', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'telaJogoView', "id" => "")));
$rotas->add('atualizaTempoReal', new Route('/jogo/atualizaReal', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'atualizaTempoReal')));
$rotas->add('atualizaTempoUser', new Route('/jogo/atualizaRealUser/{id}', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'atualizaRealUser', "id" => "")));
$rotas->add('enviarSumula', new Route('/jogo/sumula/enviar', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'atualizaSumula')));
$rotas->add('telaCadastro', new Route('/jogo/cadastro', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'telaNovoJogo')));


$rotas->add('alteraStatus', new Route('/jogo/status', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'atualizaStatus')));

$rotas->add('telaJogosScript', new Route('/jogo/all/search', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'listarJogos')));
$rotas->add('telaAlterarJogo', new Route('/jogo/change', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'telaAlteraJogo')));
$rotas->add('telaAlterarJogoEfetivar', new Route('/jogo/change/efetivar', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'alteraJogo')));

$rotas->add('telaRemoveJogoEfetivar', new Route('/jogo/delete/efetivar', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'RemoveJogo')));
$rotas->add('telaNovoJogo', new Route('/jogo', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'telaJogo')));

$rotas->add('cadastraTempoReal', new Route('/jogo/view/novo', array('_controller' => 'JIF\Controller\ControllerJogo', '_method' => 'statusDoJogo')));

$rotas->add('telaCrachas', new Route('/painel/crachas', array('_controller' => 'JIF\Controller\ControllerTime', '_method' => 'telaGerarCracha')));

//sobre o site
$rotas->add('telaSobreParacatu', new Route('/paracatu', array('_controller' => 'JIF\Controller\ControllerIndex', '_method' => 'telaSobreParacatu')));
$rotas->add('telaSobreDesen', new Route('/team', array('_controller' => 'JIF\Controller\ControllerIndex', '_method' => 'telaSobreDesen')));
$rotas->add('telaPontosDeInte', new Route('/pontos-de-interesse', array('_controller' => 'JIF\Controller\ControllerIndex', '_method' => 'telaPontos')));


//postagem
$rotas->add('telaAlteraPostagem', new Route('/postagem/change/{id}', array('_controller' => 'JIF\Controller\ControllerPostagem', '_method' => 'telaChangePostagem')));
$rotas->add('pendente', new Route('/postagem/pendente/{id}', array('_controller' => 'JIF\Controller\ControllerPostagem', '_method' => 'aceitarPendente',  "id" => "")));


$rotas->add('changePostagem', new Route('/alterar/postagem', array('_controller' => 'JIF\Controller\ControllerPostagem', '_method' => 'atualizarPostagem')));


$rotas->add('telaShowPostagem', new Route('/postagem/view', array('_controller' => 'JIF\Controller\ControllerPostagem', '_method' => 'telaNovaPostagem')));
$rotas->add('gravarPostagem', new Route('/gravar/postagem', array('_controller' => 'JIF\Controller\ControllerPostagem', '_method' => 'gravarPostagem')));
$rotas->add('telaPostagem', new Route('/postagem/{id}', array('_controller' => 'JIF\Controller\ControllerPostagem', '_method' => 'telaPostagem', "id" => "")));
$rotas->add('removePost', new Route('/postagem/excluir/{id}', array('_controller' => 'JIF\Controller\ControllerPostagem', '_method' => 'removePostagem', "id" => "")));


return $rotas;
