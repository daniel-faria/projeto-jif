<?php

namespace JIF\Models;

use JIF\Util\Conexao;
use JIF\Entity\Time;
use PDO;
use \PDOException;

class ModelTime {

    public function __construct() {
        
    }

    function cadastrarTime(Time $time, $campus) {

        try {
            $sql = "insert into time (nomeTime,Tecnico_cpf,Modalidade_idModalidade, campus) values(?,?,?,?)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $time->getNome(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $time->getTecnico(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $time->getModalidade(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $campus, PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return Conexao::getInstance()->lastInsertId();
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
               
                    <strong>Falha! </strong> Inserção de time não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
               
                    <strong>Falha! </strong> Inserção da modalidade não executada (Modalidade já existente)!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            return false;
        }
    }

    function buscarCrachaTodosTimes() {
        try {
            $sql = "select distinct * from aluno join time_has_aluno join time where time_has_aluno.aluno_ra = aluno.ra and time.idTime = time_has_aluno.time_idTime order by aluno.campus";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();

            //return $p_sql->rowCount();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            //print_r($e);
        }
    }

    function buscarCrachaTimeAtual($idTime) {
        try {
            $sql = "select distinct * from aluno join time_has_aluno join time where time_has_aluno.aluno_ra = aluno.ra and time.idTime = time_has_aluno.time_idTime and time.idTime = :idTime order by aluno.nome";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":idTime", $idTime);
            $p_sql->execute();

            //return $p_sql->rowCount();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            //print_r($e);
        }
    }

    function buscarCrachaCampusAtual($campus) {
        try {
            $sql = "select distinct * from aluno join time_has_aluno join time where time_has_aluno.aluno_ra = aluno.ra and aluno.campus = :campus and time.idTime = time_has_aluno.time_idTime order by aluno.nome";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":campus", $campus);
            $p_sql->execute();

            //return $p_sql->rowCount();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            //print_r($e);
        }
    }

    function listarTimesAluno($idAluno) {
        try {

            $sql = "select * from aluno join time_has_aluno join time where time_has_aluno.aluno_ra = aluno.ra and time.idTime = time_has_aluno.time_idTime and aluno.ra = :idAluno ";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":idAluno", $idAluno);
            $p_sql->execute();

            //return $p_sql->rowCount();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            //print_r($e);
        }
    }

    function removerAlunoTime(Time $time) {
        try {
            $sql = "DELETE FROM time_has_aluno WHERE aluno_ra = ?";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $time->getAluno());

            if ($p_sql->execute()) {
                return 1;
            } else {
                return 0;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Não foi possível excluir aluno do time!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            return false;
        }
    }

    function deletaTime($id) {

        try {
            $sql = "DELETE FROM time WHERE idTime = ?";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $id);

            if ($p_sql->execute()) {
                return 1;
            } else {
                return 0;
            }
        } catch (\PDOException $e) {
            //print_r($e);


            return 0;
        }
    }

    function salvaTime(Time $time, $id) {

        try {
            $sql = "UPDATE time SET nomeTime = ? , Modalidade_idModalidade =? WHERE time.idTime = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $time->getNome());
            $p_sql->bindValue(2, $time->getModalidade());
            $p_sql->bindValue(3, $id);

            if ($p_sql->execute()) {
                return 1;
            } else {
                return 0;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function cadastrarTimeHasAluno(Time $time, $lastId) {

        try {
            $sql = "insert into time_has_aluno (time_idTime,aluno_ra) values(?,?)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $lastId, PDO::PARAM_STR);
            $p_sql->bindValue(2, $time->getAluno(), PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return true;
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
               
                    <strong>Falha! </strong> Inserção de time não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function buscarTimeTecnico($cpf) {

        try {

            $sql = "SELECT * FROM time WHERE Tecnico_cpf =:cpf";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':cpf' => $cpf))) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarTimeCampus($campus) {

        try {

            $sql = "SELECT * FROM time WHERE campus =:campus";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':campus' => $campus))) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscaQuantidade($idTime) {

        try {
            $sql = " SELECT * FROM TIME JOIN time_has_aluno WHERE time_has_aluno.time_idTime = time.idTime AND time.idTime = :idTime ";


            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute(array(':idTime' => $idTime))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);
                if ($p_sql->rowCount() > 0) {

                    return $p_sql->rowCount();
                } else {
                    return 0;
                }
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function buscaModalidade(Time $time) {

        try {
            $sql = "select * from modalidade where idModalidade  = :idModalidade ";

            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute(array(':idModalidade' => $time->getModalidade()))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);
                if ($p_sql->rowCount() > 0) {

                    return $row['qtdAtleta'];
                } else {
                    return 0;
                }
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function verificaLimite($id) {

        try {
            $sql = "select * from time_has_aluno where time_idTime =:idTime";

            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':idTime' => $id))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);
                if ($p_sql->rowCount() > 0) {
                    return $p_sql->rowCount();
                } else {
                    return 0;
                }
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    /* function buscarAluno($ra) {

      try {
      $sql = "select * from aluno where ra = ?";
      $p_sql = Conexao::getInstance()->prepare($sql);
      $p_sql->bindValue(1, $ra, PDO::PARAM_STR);
      $p_sql->execute();
      return $p_sql->fetch(PDO::FETCH_OBJ);
      } catch (Exception $e) {

      }
      } */

    function procuraAluno(Aluno $aluno) {

        try {


            $sql = "SELECT * FROM aluno WHERE ra = :ra";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':ra' => $aluno->getRa()))) {
                $rows = $p_sql->rowCount();
                $row = $p_sql->fetch(PDO::FETCH_SERIALIZE);

                $aluno->setRa($row['ra']);
                $aluno->setNome($row['nome']);
                $aluno->setData_nascimento($row['data_nascimento']);
                $aluno->setSexo($row['sexo']);
                $aluno->setRg($row['rg']);
                $aluno->setCampus($row['campus']);
                $aluno->setTelefone($row['telefone']);
                $aluno->setImagem($row['foto']);

                return $rows;
            } else {
                return null;
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function pesquisaAlunos() {

        try {

            $sql = "SELECT * FROM aluno";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarTimeJoinAluno($idTime) {

        try {


            $sql = "select * from time join aluno join time_has_aluno tha where idTime =:idTime and tha.time_idTime = time.idTime and aluno.ra = tha.aluno_ra ";

            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':idTime' => $idTime))) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function buscarTimeJoinAlunoInative($id, $campus) {

        try {


            $sql = "select * from aluno where aluno.ra not in (select time_has_aluno.aluno_ra from time_has_aluno where time_has_aluno.time_idTime = :id) and aluno.campus = :campus";




            $p_sql = Conexao::getInstance()->prepare($sql);


            if ($p_sql->execute(array(':id' => $id, ':campus' => $campus))) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function selectTime($id) {

        try {


            $sql = "select * from time where idTime = :idTime";

            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':idTime' => $id))) {

                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function atualizarAlunoSemFoto(Aluno $aluno) {

        try {
            $sql = "update aluno set ra = ?, nome = ?, data_nascimento = ?, sexo = ?, rg = ?, campus = ?, telefone = ? where ra = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $aluno->getRa(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $aluno->getNome(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $aluno->getData_nascimento(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $aluno->getSexo(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $aluno->getRg(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $aluno->getCampus(), PDO::PARAM_STR);
            $p_sql->bindValue(7, $aluno->getTelefone(), PDO::PARAM_STR);
            $p_sql->bindValue(8, $aluno->getRa(), PDO::PARAM_STR);

            if ($p_sql->execute()) {

                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            return false;
        }
    }

    function buscarAlunos() {

        try {

            $sql = "SELECT * FROM aluno";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarAlunosCampus($campus) {

        try {

            $sql = "SELECT * FROM aluno where campus = ?";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $campus, PDO::PARAM_STR);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    /* function atualizarAluno(Aluno $aluno) {


      $sql = "SELECT * FROM aluno";
      $p_sql = Conexao::getInstance()->prepare($sql);
      $p_sql->execute();
      return $p_sql->fetchAll(PDO::FETCH_OBJ);
      } catch (Exception $e) {
      print 'Ocorreu um erro ao tentar executar esta ação. ';
      }
      } */

    function atualizarAluno(Aluno $aluno) {

        try {
            $sql = "update aluno set ra = ?, nome = ?, data_nascimento = ?, sexo = ?, rg = ?, campus = ?, telefone = ?, foto = ? where ra = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $aluno->getRa(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $aluno->getNome(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $aluno->getData_nascimento(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $aluno->getSexo(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $aluno->getRg(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $aluno->getCampus(), PDO::PARAM_STR);
            $p_sql->bindValue(7, $aluno->getTelefone(), PDO::PARAM_STR);
            $p_sql->bindValue(8, $aluno->getImagem(), PDO::PARAM_STR);
            $p_sql->bindValue(9, $aluno->getRa(), PDO::PARAM_STR);

            if ($p_sql->execute()) {

                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    /* function ativarAluno($ra) {
      try {
      $sql = "update aluno set status = true where ra = ?";
      $p_sql = Conexao::getInstance()->prepare($sql);
      $p_sql->bindValue(1, $ra, PDO::PARAM_BOOL);
      $p_sql->execute();
      return $p_sql->rowCount();
      } catch (Exception $e) {

      }
      } */

    function removeAluno($ra) {
        try {

            $sql = "delete from aluno where ra = ?";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $ra, PDO::PARAM_BOOL);
            $p_sql->execute();
            return $p_sql->rowCount();
        } catch (Exception $e) {
            //print_r($e);
            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong>Não foi possível excluir Atleta!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
        }
    }

}
