<?php

namespace JIF\Models;

use \PDOException;
use JIF\Util\Conexao;
use JIF\Entity\Usuario;
use PDO;

class ModelUsuario {

    public function __construct() {
        
    }

    function listarPessoas() {

        try {

            $sql = "SELECT * FROM usuario";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute()) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function procuraUsuario(Usuario $usuario) {

        try {

            $sql = "SELECT * FROM usuario WHERE cpf=:cpf AND permissao!=:permissao";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':cpf' => $usuario->getCpf(), ':permissao' => $usuario->getPermissao()))) {
                $rows = $p_sql->rowCount();
                $row = $p_sql->fetch(PDO::FETCH_SERIALIZE);

                $usuario->setCpf($row['cpf']);
                $usuario->setSiape($row['siape']);
                $usuario->setCampus($row['campus']);
                $usuario->setNome($row['nome']);
                $usuario->setTelefone($row['telefone']);
                $usuario->setEmail($row['email']);
                $usuario->setPermissao($row['permissao']);

                return $rows;
            } else {
                
            }
        } catch (\PDOException $e) {
            //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function ultimoLogin(Usuario $usuario) {
        try {

            $sql = "UPDATE usuario SET data_ultimo_login = ? WHERE usuario.cpf = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $usuario->getData_ultimo_login());
            $p_sql->bindValue(2, $usuario->getCpf());

            $p_sql->execute();
        } catch (\PDOException $e) {
            // print_r($e);
            return $erro = 1;
        }
    }

    function senhaSemAlterar(Usuario $usuario) {


        try {
            $sql = "select data_ultimo_login from usuario where cpf  = :cpf";

            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute(array(':cpf' => $usuario->getCpf()))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);
                if ($p_sql->rowCount() > 0) {

                    $usuario->setData_ultimo_login($row['data_ultimo_login']);
                } else {
                    return 0;
                }
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            // print_r($e);
            return false;
        }
    }

    function removeTecnico(Usuario $usuario) {
        try {

            $sql = "DELETE FROM usuario WHERE usuario.cpf = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $usuario->getCpf());
            $p_sql->execute();
            return $sucesso = 2;
        } catch (\PDOException $e) {
            return $erro = 1;
        }
    }

    function buscarTecnico($cpf) {

        try {

            $sql = "SELECT * FROM usuario WHERE cpf= :cpf";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cpf", $cpf);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function existeSenha($usuario, $atual) {

        try {

            $sql = "SELECT * FROM usuario WHERE cpf= :cpf";
            $p_sql = Conexao::getInstance()->prepare($sql);


            if ($p_sql->execute(array(':cpf' => $usuario->getCpf()))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);

                //print_r('<br/>' . $atual . '<br/>');
                //print_r($row['senha']);

                if (password_verify($atual, $row['senha'])) {


                    return $achou = 1;
                } else {
                    return $fail = 0;
                }
            }
        } catch (\PDOException $e) {
            //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function autenticar(Usuario $usuario) {

        try {

            $sql = "SELECT * FROM usuario WHERE cpf= :cpf";
            $p_sql = Conexao::getInstance()->prepare($sql);


            if ($p_sql->execute(array(':cpf' => $usuario->getCpf()))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);

                if (password_verify($usuario->getSenha(), $row['senha'])) {
                    $usuario->setPermissao($row['permissao']);
                    $usuario->setNome($row['nome']);
                    $usuario->setSiape($row['siape']);
                    $usuario->setEmail($row['email']);
                    $usuario->setTelefone($row['telefone']);
                    $usuario->setCampus($row['campus']);
                    $usuario->setCpf($row['cpf']);
                    $usuario->setSenha($row['senha']);

                    return $achou = 1;
                } else {
                    return $fail = 0;
                }
            }
        } catch (\PDOException $e) {
            //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function cadastrarTecnico(Usuario $usuario) {


        try {
            $sql = "INSERT INTO usuario (cpf, nome, siape, email, campus, telefone, senha, permissao, data_cadastro) VALUES(?,?,?,?,?,?,?,?,?)";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $usuario->getCpf());
            $p_sql->bindValue(2, $usuario->getNome());
            $p_sql->bindValue(3, $usuario->getSiape());
            $p_sql->bindValue(4, $usuario->getEmail());
            $p_sql->bindValue(5, $usuario->getCampus());
            $p_sql->bindValue(6, $usuario->getTelefone());
            $p_sql->bindValue(7, $usuario->getSenha());
            $p_sql->bindValue(8, $usuario->getPermissao());
            $p_sql->bindValue(9, $usuario->getData_cadastro());

            if ($p_sql->execute()) {

                return $sucesso = 2;
            } else {

                return $falha = 1;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return $erro = 1;
        }
    }

    function atualizarTecnicoBefore(Usuario $usuario) {


        try {

            if ($usuario->getSiape() == '') {
                $sql = "UPDATE usuario SET nome = ? , senha =? , telefone =?, email =? WHERE usuario.cpf = ?";
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(1, $usuario->getNome());
                $p_sql->bindValue(2, $usuario->getSenha());
                $p_sql->bindValue(3, $usuario->getTelefone());
                $p_sql->bindValue(4, $usuario->getEmail());
                $p_sql->bindValue(5, $usuario->getCpf());
            } else {
                $sql = "UPDATE usuario SET nome = ? , senha =? , telefone =? ,siape=?, email =? WHERE usuario.cpf = ?";
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(1, $usuario->getNome());
                $p_sql->bindValue(2, $usuario->getSenha());
                $p_sql->bindValue(3, $usuario->getTelefone());
                $p_sql->bindValue(4, $usuario->getSiape());
                $p_sql->bindValue(5, $usuario->getEmail());
                $p_sql->bindValue(6, $usuario->getCpf());
            }


            if ($p_sql->execute()) {
                return $sucesso = 2;
            } else {
                return $falha = 1;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return $erro = 4;
        }
    }

    function atualizarTecnico(Usuario $usuario) {


        try {
            $sql = "UPDATE usuario SET nome = ? , telefone =? ,siape=?,email =?,permissao =?, campus =? WHERE usuario.cpf = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $usuario->getNome());
            $p_sql->bindValue(2, $usuario->getTelefone());
            $p_sql->bindValue(3, $usuario->getSiape());
            $p_sql->bindValue(4, $usuario->getEmail());
            $p_sql->bindValue(5, $usuario->getPermissao());
            $p_sql->bindValue(6, $usuario->getCampus());
            $p_sql->bindValue(7, $usuario->getCpf());


            if ($p_sql->execute()) {

                return $sucesso = 2;
            } else {

                return $falha = 1;
            }
        } catch (\PDOException $e) {
            // print_r($e);
            return $erro = 1;
        }
    }

    function ativarLoginTecnico(Usuario $usuario) {


        try {

            $sql = "SELECT * FROM usuario WHERE cpf=:cpf AND senha=:senha";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':cpf' => $usuario->getCpf(), ':senha' => null))) {
                $rows = $p_sql->rowCount();
            } else {
                echo 'Falha ao executar a ação!';
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }


        if ($rows == 0) {
            try {
                $sql = "UPDATE usuario SET nome = ? , senha =? , telefone =? ,siape=?,email =?,data_cadastro =? WHERE usuario.cpf = ?";

                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(1, $usuario->getNome());
                $p_sql->bindValue(2, $usuario->getSenha());
                $p_sql->bindValue(3, $usuario->getTelefone());
                $p_sql->bindValue(4, $usuario->getSiape());
                $p_sql->bindValue(5, $usuario->getEmail());
                $p_sql->bindValue(6, $usuario->getData_cadastro());
                $p_sql->bindValue(7, $usuario->getCpf());
                $p_sql->execute();
                return $sucesso = 2;
            } catch (\PDOException $e) {
                return $erro = 1;
            }
        } else {
            //print_r($e);
            return $erro = 4;
        }
    }

}
