<?php

namespace JIF\Models;

use JIF\Util\Conexao;
use JIF\Entity\Modalidade;
use PDO;
use \PDOException;

class ModelModalidade {

    public function __construct() {
        
    }

    function inserirModalidade(Modalidade $modalidade) {

        try {
            $sql = "INSERT INTO modalidade(esporte, coletivo, qtdAtleta, masculino) VALUES (?,?,?,?)";
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $modalidade->getEsporte(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $modalidade->getColetivo(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $modalidade->getQtdAtletas(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $modalidade->getMasculino(), PDO::PARAM_STR);

            if ($p_sql->execute()) {

                return 'Modalidade Cadastratada com Sucesso!'; // retorna o id do aluno inserido
            } else {

                return 'Erro ao inserir!!';
            }
        } catch (\Exception $ex) {
            //return $ex;
        }
    }

    function pesquisarModalidade($id) {

        try {

            $sql = "SELECT * FROM modalidade WHERE idModalidade = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarModalidades() {

        try {

            $sql = "SELECT * FROM modalidade";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarModalidadesGenotipo(Modalidade $modalidade) {

        try {

            $sql = "SELECT * FROM modalidade WHERE coletivo = :tipo and masculino = :genero";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":tipo", $modalidade->getColetivo());
            $p_sql->bindValue(":genero", $modalidade->getMasculino());
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarModalidadesTipo($tipo) {

        try {

            $sql = "SELECT * FROM modalidade WHERE coletivo = :tipo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":tipo", $tipo);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarModalidadesGenero($genero) {

        try {

            $sql = "SELECT * FROM modalidade WHERE masculino = :genero";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":genero", $genero);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function atualizarModalidades(Modalidade $modalidade) {

        try {

            $sql = "UPDATE modalidade SET esporte= :esp,coletivo= :col,qtdAtleta= :qtd,masculino= :mas WHERE idModalidade= :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":esp", $modalidade->getEsporte());
            $p_sql->bindValue(":col", $modalidade->getColetivo());
            $p_sql->bindValue(":qtd", $modalidade->getQtdAtletas());
            $p_sql->bindValue(":mas", $modalidade->getMasculino());
            $p_sql->bindValue(":id", $modalidade->getId());

            $p_sql->execute();
        } catch (\Exception $e) {
            print 'Ocorreu um erro ao atualizar modalidade. ';
        }
    }

    function deleteModalidade($id) {
        try {
            $sql = "DELETE from modalidade WHERE idModalidade= :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();

            return $p_sql->rowCount();
            
        } catch (Exception $e) {
           // print_r($e);
        }
    }

}
