<?php

namespace JIF\Models;

use JIF\Util\Conexao;
use JIF\Entity\Postagem;
use PDO;
use \PDOException;

class ModelPostagem {

    public function __construct() {
        
    }

    function listarPostagem($status) {

        try {


            //$sql = "SELECT * FROM postagem ORDER BY dataHora DESC, id DESC";
            $sql = "SELECT * FROM postagem WHERE status = :status ORDER BY dataHora DESC";
            $p_sql = Conexao::getInstance()->prepare($sql);

            //  if ($p_sql->execute(array(':aprovada' => $status))) {
            if ($p_sql->execute(array(':status' => $status))) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function listarPostagemAll() {

        try {


            //$sql = "SELECT * FROM postagem ORDER BY dataHora DESC, id DESC";
            $sql = "SELECT * FROM postagem ORDER BY dataHora DESC";
            $p_sql = Conexao::getInstance()->prepare($sql);

            //  if ($p_sql->execute(array(':aprovada' => $status))) {
            if ($p_sql->execute(array())) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function cadastrarPostagem(Postagem $postagem) {

        try {
            $sql = "insert into postagem (dataHora,titulo,texto,foto,Usuario_cpf,topo) values(?,?,?,?,?,?)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $postagem->getDataHora(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $postagem->getTitulo(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $postagem->getTexto(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $postagem->getFoto(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $postagem->getUsuario_cpf(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $postagem->getTopo(), PDO::PARAM_STR);


            if ($p_sql->execute()) {
                //print_r(Conexao::getInstance()->lastInsertId());
                return true; // retorna o id do aluno inserido
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            // print_r($e);
            return false;
        }
    }

    function cadastrarPostagemPendente(Postagem $postagem) {

        try {
            $sql = "insert into postagem (dataHora,titulo,texto,foto,Usuario_cpf,topo,status) values(?,?,?,?,?,?,?)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $postagem->getDataHora(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $postagem->getTitulo(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $postagem->getTexto(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $postagem->getFoto(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $postagem->getUsuario_cpf(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $postagem->getTopo(), PDO::PARAM_STR);
            $p_sql->bindValue(7, 1, PDO::PARAM_STR);

            if ($p_sql->execute()) {
                //print_r(Conexao::getInstance()->lastInsertId());
                return true; // retorna o id do aluno inserido
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function cadastrarPostagemSemFoto(Postagem $postagem) {

        try {
            $sql = "insert into postagem (dataHora,titulo,texto,Usuario_cpf,topo) values(?,?,?,?,?)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $postagem->getDataHora(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $postagem->getTitulo(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $postagem->getTexto(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $postagem->getUsuario_cpf(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $postagem->getTopo(), PDO::PARAM_STR);


            if ($p_sql->execute()) {
                //print_r(Conexao::getInstance()->lastInsertId());
                return true; // retorna o id do aluno inserido
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            print_r($e);
            return false;
        }
    }

    function cadastrarPostagemSemFotoPendente(Postagem $postagem, $status) {

        try {
            $sql = "insert into postagem (Usuario_cpf,topo,status, dataHora, titulo, texto) values(?,?,?,?,?,?)";

            $p_sql = Conexao::getInstance()->prepare($sql);


            $p_sql->bindValue(1, $postagem->getUsuario_cpf(), PDO::PARAM_STR);
            // print_r($postagem->getUsuario_cpf());echo '<br/>';
            $p_sql->bindValue(2, $postagem->getTopo(), PDO::PARAM_STR);
            //print_r($postagem->getTopo());echo '<br/>';
            $p_sql->bindValue(3, $status, PDO::PARAM_STR);
            // print_r($status);echo '<br/>';
            $p_sql->bindValue(4, $postagem->getDataHora(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $postagem->getTitulo(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $postagem->getTexto(), PDO::PARAM_STR);

            if ($p_sql->execute()) {
                //print_r(Conexao::getInstance()->lastInsertId());
                return true; // retorna o id do aluno inserido
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function buscarPostagem($id) {

        try {

            $sql = "SELECT * FROM postagem WHERE id =:id";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':id' => $id))) {

                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
            //  print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function atualizarPostagemComFotoPendente(Postagem $postagem) {

        try {

            $sql = "update postagem set dataModificado = ?, titulo = ?, texto = ?, dataModificadoPendente = ?, tituloPendente = ?, textoPendente = ?, fotoPendente = ?, status = ? where postagem.id = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, NULL, PDO::PARAM_STR);
            $p_sql->bindValue(2, NULL, PDO::PARAM_STR);
            $p_sql->bindValue(3, NULL, PDO::PARAM_STR);
            $p_sql->bindValue(4, $postagem->getDataHora(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $postagem->getTitulo(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $postagem->getTexto(), PDO::PARAM_STR);
            //echo $postagem->getTexto();
            $p_sql->bindValue(7, $postagem->getFoto(), PDO::PARAM_STR);
            $p_sql->bindValue(8, 2);
            $p_sql->bindValue(9, $postagem->getId(), PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            print_r($e);
            return false;
        }
    }

    function efetivaCadastroPostagem(Postagem $postagem) {

        try {

            $sql = "update postagem set dataHora = ?, titulo = ?, texto = ?, foto = ?, Usuario_cpf = ?, status = ? where postagem.id = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);


            $p_sql->bindValue(1, $postagem->getDataHora(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $postagem->getTitulo(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $postagem->getTexto(), PDO::PARAM_STR);
            //echo $postagem->getTexto();
            $p_sql->bindValue(4, $postagem->getFoto(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $postagem->getUsuario_cpf(), PDO::PARAM_STR);
            $p_sql->bindValue(6, 0);
            $p_sql->bindValue(7, $postagem->getId(), PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            print_r($e);
            return false;
        }
    }

    function efetivaAlteracaoPostagem(Postagem $postagem) {

        try {

            $sql = "update postagem set dataModificado = ?, titulo = ?, texto = ?, foto = ?, Usuario_cpf = ?, status = ? where postagem.id = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);


            $p_sql->bindValue(1, $postagem->getDataHora(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $postagem->getTitulo(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $postagem->getTexto(), PDO::PARAM_STR);
            //echo $postagem->getTexto();
            $p_sql->bindValue(4, $postagem->getFoto(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $postagem->getUsuario_cpf(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $postagem->getTopo());
            $p_sql->bindValue(7, $postagem->getId(), PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            print_r($e);
            return false;
        }
    }

    function deletePostagemPendente($id) {

        try {

            $sql = "update postagem set status = ? where postagem.id = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, 3);
            $p_sql->bindValue(2, $id, PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function atualizarPostagemComFoto(Postagem $postagem) {

        try {

            $sql = "update postagem set dataModificado = ?, titulo = ?, texto = ?,dataModificadoPendente = ?, tituloPendente = ?, textoPendente = ?, fotoPendente = ?, Usuario_cpf = ? where postagem.id = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);


            $p_sql->bindValue(1, NULL, PDO::PARAM_STR);
            $p_sql->bindValue(2, NULL, PDO::PARAM_STR);
            $p_sql->bindValue(3, NULL, PDO::PARAM_STR);

            $p_sql->bindValue(4, $postagem->getDataHora(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $postagem->getTitulo(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $postagem->getTexto(), PDO::PARAM_STR);
            $p_sql->bindValue(7, $postagem->getFoto(), PDO::PARAM_STR);


            $p_sql->bindValue(8, $postagem->getUsuario_cpf(), PDO::PARAM_STR);
            $p_sql->bindValue(9, $postagem->getId(), PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return true;
            } else {

                return false;
            }
        } catch (\PDOException $e) {
            // print_r($e);
            return false;
        }
    }

    function atualizarPostagemSemFoto(Postagem $postagem) {

        try {

            $sql = "update postagem set dataModificado = ?, titulo = ?, texto = ?, dataModificadoPendente = ?, tituloPendente = ?, textoPendente = ?, Usuario_cpf = ? where postagem.id = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, NULL, PDO::PARAM_STR);
            $p_sql->bindValue(2, NULL, PDO::PARAM_STR);
            $p_sql->bindValue(3, NULL, PDO::PARAM_STR);
            
            $p_sql->bindValue(4, $postagem->getDataHora(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $postagem->getTitulo(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $postagem->getTexto(), PDO::PARAM_STR);

            $p_sql->bindValue(7, $postagem->getUsuario_cpf(), PDO::PARAM_STR);
            $p_sql->bindValue(8, $postagem->getId(), PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return true;
            } else {

                return false;
            }
        } catch (\PDOException $e) {
            // print_r($e);
            return false;
        }
    }

    function atualizarPostagemSemFotoPendente(Postagem $postagem) {

        try {

            $sql = "update postagem set dataModificado = ?, titulo = ?, texto = ?, dataModificadoPendente = ?, tituloPendente = ?, textoPendente = ?,  status = ? where postagem.id = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, NULL, PDO::PARAM_STR);
            $p_sql->bindValue(2, NULL, PDO::PARAM_STR);
            $p_sql->bindValue(3, NULL, PDO::PARAM_STR);
            $p_sql->bindValue(4, $postagem->getDataHora(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $postagem->getTitulo(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $postagem->getTexto(), PDO::PARAM_STR);
            //echo $postagem->getTexto();
            $p_sql->bindValue(7, 2);
            $p_sql->bindValue(8, $postagem->getId(), PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            print_r($e);
            return false;
        }
    }

    function procuraPostagem(Postagem $aluno) {

        try {

            $sql = "SELECT * FROM aluno WHERE ra = :ra";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':ra' => $aluno->getRa()))) {
                $rows = $p_sql->rowCount();
                $row = $p_sql->fetch(PDO::FETCH_SERIALIZE);

                $aluno->setRa($row['ra']);
                $aluno->setNome($row['nome']);
                $aluno->setData_nascimento($row['data_nascimento']);
                $aluno->setSexo($row['sexo']);
                $aluno->setRg($row['rg']);
                $aluno->setCampus($row['campus']);
                $aluno->setTelefone($row['telefone']);
                $aluno->setImagem($row['foto']);

                return $rows;
            } else {
                
            }
        } catch (\PDOException $e) {
            // print_r($e);

            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function pesquisaPostagems($campus) {

        try {

            $sql = "SELECT * FROM aluno where campus = :campus";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $campus, PDO::PARAM_STR);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
            // print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function relatorioPostagems() {

        try {

            $sql = "SELECT * FROM aluno ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
            // print_r($e);
            // print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarPostagems() {

        try {

            $sql = "SELECT * FROM aluno";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            // print_r($e);

            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarPostagemsCampus($campus) {

        try {

            $sql = "SELECT * FROM aluno WHERE campus = :campus";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':campus' => $campus))) {

                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
            // print_r($e);
            // print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarPostagemFoto(Postagem $aluno) {

        try {

            $sql = "SELECT * FROM aluno WHERE ra = :ra";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':ra' => $aluno->getRa()))) {
                $rows = $p_sql->rowCount();
                $row = $p_sql->fetch(PDO::FETCH_SERIALIZE);

                $aluno->setRa($row['foto']);

                return 1;
            } else {
                
            }
        } catch (\PDOException $e) {
            // print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function deletePostagem($id) {
        try {
            $sql = "DELETE from postagem WHERE id = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();
            return $p_sql->rowCount();
        } catch (Exception $e) {
            // print_r($e);
        }
    }

    function removePostagem($ra) {
        try {

            $sql = "delete from aluno where ra = ?";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $ra, PDO::PARAM_BOOL);
            $p_sql->execute();
            return $p_sql->rowCount();
        } catch (Exception $e) {
            // print_r($e);
        }
    }

}
