<?php

namespace JIF\Models;

use JIF\Util\Conexao;
use JIF\Entity\Jogo;
use PDO;
use \PDOException;

class ModelJogo {

    public function __construct() {
        
    }

    function listarTime($status) {

        try {

            $sql = "SELECT * FROM jogo WHERE jogo.status = :status ORDER BY idJogo";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':status' => $status))) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarJogo($id) {

        try {

            $sql = "SELECT * FROM jogo WHERE jogo.idJogo = :idJogo";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':idJogo' => $id))) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }
    

    function buscarTime($id) {

        try {

            $sql = "SELECT * FROM time WHERE time.idTime = :idTime";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':idTime' => $id))) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function verPlacar($idTime, $idJogo) {

        try {

            $sql = "SELECT placar FROM placar WHERE placar.jogo_idJogo = :idJogo and placar.time_idTime = :idTime";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':idJogo' => $idJogo, ':idTime' => $idTime))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);
                return $row['placar'];
            } else {
                return 0;
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscaDadosJogo($idJogo) {

        try {

            $sql = "SELECT * FROM jogo WHERE idJogo = :idJogo";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':idJogo' => $idJogo))) {

                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                return 0;
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function verNomeTime($idTime) {

        try {

            $sql = "SELECT nomeTime, campus FROM time WHERE time.idTime = :idTime";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':idTime' => $idTime))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);
                return $row['nomeTime'] . ' ' . $row['campus'];
            } else {
                return 0;
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function listarPlacares() {

        try {

            $sql = "SELECT DISTINCT * FROM placar ORDER BY placar.time_idTime";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute()) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    /* function listarTime2($status) {

      try {

      $sql = "SELECT * FROM jogo join placar JOIN time WHERE jogo.Time_idTime1 = placar.time_idTime and time.idTime = jogo.Time_idTime1 and jogo.status = :status";
      $p_sql = Conexao::getInstance()->prepare($sql);

      if ($p_sql->execute(array(':status' => $status))) {
      return $p_sql->fetchAll(PDO::FETCH_OBJ);
      } else {

      }
      } catch (\PDOException $e) {
      print 'Ocorreu um erro ao tentar executar esta ação. ';
      }
      } */

    function cadastrarJogo(Jogo $jogo) {

        try {
            $sql = "insert into jogo (dataHoraInicio,local,Time_idTime, Time_idTime1) values(?,?,?,?)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $jogo->getDataInicio(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $jogo->getLocal(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $jogo->getTime1ID(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $jogo->getTime2ID(), PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return Conexao::getInstance()->lastInsertId();
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
               
                    <strong>Falha! </strong> Cadastro de Jogo não executado!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
           // print_r($e);
            return false;
        }
    }

    function atualizaAdminPlacarJogo($jogo, $time1, $time2, $scoreTime1, $scoreTime2) {

        try {

            $sql = "update placar set placar = ? where placar.jogo_idJogo =?  and time_idTime = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $scoreTime1, PDO::PARAM_STR);
            $p_sql->bindValue(2, $jogo, PDO::PARAM_STR);
            $p_sql->bindValue(3, $time1, PDO::PARAM_STR);


            if ($p_sql->execute()) {
                $sucesso = true;
            } else {

                $sucesso = false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            $sucesso = false;
        }

        $sql = "update placar set placar = ? where placar.jogo_idJogo =?  and time_idTime = ?";

        $p_sql = Conexao::getInstance()->prepare($sql);

        $p_sql->bindValue(1, $scoreTime2, PDO::PARAM_STR);
        $p_sql->bindValue(2, $jogo, PDO::PARAM_STR);
        $p_sql->bindValue(3, $time2, PDO::PARAM_STR);


        if ($p_sql->execute() && $sucesso == true) {
            return true;
        } else {

            return false;
        }
    }

    
    
    function updateMensagem($id, $msg) {

        try {

            $sql = "update jogo set mensagem = ? where jogo.idJogo =? ";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $msg, PDO::PARAM_STR);
            $p_sql->bindValue(2, $id, PDO::PARAM_STR);

            if ($p_sql->execute()) {
                $return = $p_sql->rowCount();
                
            } else {
                $return = false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            $return = false;
        }

    }
    function updateStatus($id, $status) {

        try {

            $sql = "update jogo set status = ? where jogo.idJogo =? ";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $status, PDO::PARAM_STR);
            $p_sql->bindValue(2, $id, PDO::PARAM_STR);

            if ($p_sql->execute()) {
                $return = $p_sql->rowCount();
                
            } else {
                $return = false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            $return = false;
        }

    }

    function updateJogo(Jogo $jogo, $time1, $time2) {

        try {
            if ($time1 == -1) {
                $sql = "update jogo set dataHoraInicio = ? , local = ? , Time_idTime1=? where jogo.idJogo =? ";

                $p_sql = Conexao::getInstance()->prepare($sql);

                $p_sql->bindValue(1, $jogo->getDataInicio(), PDO::PARAM_STR);
                $p_sql->bindValue(2, $jogo->getLocal(), PDO::PARAM_STR);
                $p_sql->bindValue(3, $jogo->getTime2ID(), PDO::PARAM_STR);
                $p_sql->bindValue(4, $jogo->getIdJogo(), PDO::PARAM_STR);
            } elseif ($time2 == -1) {
                $sql = "update jogo set dataHoraInicio = ? , local = ? ,Time_idTime= ? where jogo.idJogo =? ";

                $p_sql = Conexao::getInstance()->prepare($sql);

                $p_sql->bindValue(1, $jogo->getDataInicio(), PDO::PARAM_STR);
                $p_sql->bindValue(2, $jogo->getLocal(), PDO::PARAM_STR);
                $p_sql->bindValue(3, $jogo->getTime1ID(), PDO::PARAM_STR);
                $p_sql->bindValue(4, $jogo->getIdJogo(), PDO::PARAM_STR);
            } else {
                $sql = "update jogo set dataHoraInicio = ? , local = ? ,Time_idTime= ?, Time_idTime1=? where jogo.idJogo =? ";

                $p_sql = Conexao::getInstance()->prepare($sql);

                $p_sql->bindValue(1, $jogo->getDataInicio(), PDO::PARAM_STR);
                $p_sql->bindValue(2, $jogo->getLocal(), PDO::PARAM_STR);
                $p_sql->bindValue(3, $jogo->getTime1ID(), PDO::PARAM_STR);
                $p_sql->bindValue(4, $jogo->getTime2ID(), PDO::PARAM_STR);
                $p_sql->bindValue(5, $jogo->getIdJogo(), PDO::PARAM_STR);
            }


            if ($p_sql->execute()) {
                return true;
            } else {

                return false;
            }
        } catch (\PDOException $e) {
           // print_r($e);
            return false;
        }
    }

    function listarTimes() {
        try {

            $sql = "select * from time order by time.nomeTime";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();

            //return $p_sql->rowCount();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            //print_r($e);
        }
    }

    function removerAlunoJogo(Jogo $time) {
        try {
            $sql = "DELETE FROM time_has_aluno WHERE aluno_ra = ?";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $time->getAluno());

            if ($p_sql->execute()) {
                return 1;
            } else {
                return 0;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Não foi possível excluir aluno do time!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            return false;
        }
    }

    function deletaPlacar($id) {


        try {

            $sql = "DELETE FROM placar WHERE jogo_idJogo = ?";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $id);

            if ($p_sql->execute()) {
                return 1;
            } else {
                return 0;
            }
        } catch (\PDOException $e) {
            //print_r($e);


            return 0;
        }
    }

    function deletaJogo($id) {


        $sql = "DELETE FROM placar WHERE jogo_idJogo = ?";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->bindValue(1, $id);

        if ($p_sql->execute()) {
            $sucesso = 1;
        } else {
            $sucesso = 0;
        }


        if ($sucesso == 1) {
            try {

                $sql = "DELETE FROM jogo WHERE idJogo = ?";
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(1, $id);

                if ($p_sql->execute()) {
                    return 1;
                } else {
                    return 0;
                }
            } catch (\PDOException $e) {
                //print_r($e);


                return 0;
            }
        } else {
            return 0;
        }
    }

    
    
    function updateEnd($idJogo , $data) {

        try {
            $sql = "UPDATE jogo SET dataHoraFim = ? WHERE idJogo = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $data);
            $p_sql->bindValue(2, $idJogo);

            if ($p_sql->execute()) {
                return 1;
            } else {
                return 0;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }
    
    function updateSumula($idJogo , $data) {

        try {
            $sql = "UPDATE jogo SET sumula = ? WHERE idJogo = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $data);
            $p_sql->bindValue(2, $idJogo);

            if ($p_sql->execute()) {
                return 1;
            } else {
                return 0;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function cadastrarPlacarTime($time, $lastId) {

        try {
            $sql = "insert into placar (jogo_idJogo,time_idTime) values(?,?)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $lastId, PDO::PARAM_STR);
            $p_sql->bindValue(2, $time, PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return true;
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
               
                    <strong>Falha! </strong> Inserção de time não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function buscarJogoTecnico($cpf) {

        try {

            $sql = "SELECT * FROM time WHERE Tecnico_cpf =:cpf";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':cpf' => $cpf))) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscaQuantidade($idJogo) {

        try {
            $sql = " SELECT * FROM TIME JOIN time_has_aluno WHERE time_has_aluno.time_idJogo = time.idJogo AND time.idJogo = :idJogo ";


            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute(array(':idJogo' => $idJogo))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);
                if ($p_sql->rowCount() > 0) {

                    return $p_sql->rowCount();
                } else {
                    return 0;
                }
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function buscaCampus($id) {

        try {
            $sql = "select * from time where idTime  = :idTime ";

            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute(array(':idTime' => $id))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);
                if ($p_sql->rowCount() > 0) {

                    return $row['campus'];
                } else {
                    return 0;
                }
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function buscaModalidade($id) {

        try {
            $sql = "select * from time where idTime  = :idTime ";

            $p_sql = Conexao::getInstance()->prepare($sql);
            if ($p_sql->execute(array(':idTime' => $id))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);
                if ($p_sql->rowCount() > 0) {

                    return $row['Modalidade_idModalidade'];
                } else {
                    return 0;
                }
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function verificaLimite($id) {

        try {
            $sql = "select * from time_has_aluno where time_idJogo =:idJogo";

            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':idJogo' => $id))) {
                $row = $p_sql->fetch(PDO::FETCH_ASSOC);
                if ($p_sql->rowCount() > 0) {
                    return $p_sql->rowCount();
                } else {
                    return 0;
                }
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    /* function buscarAluno($ra) {

      try {
      $sql = "select * from aluno where ra = ?";
      $p_sql = Conexao::getInstance()->prepare($sql);
      $p_sql->bindValue(1, $ra, PDO::PARAM_STR);
      $p_sql->execute();
      return $p_sql->fetch(PDO::FETCH_OBJ);
      } catch (Exception $e) {

      }
      } */

    function procuraAluno(Aluno $aluno) {

        try {


            $sql = "SELECT * FROM aluno WHERE ra = :ra";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':ra' => $aluno->getRa()))) {
                $rows = $p_sql->rowCount();
                $row = $p_sql->fetch(PDO::FETCH_SERIALIZE);

                $aluno->setRa($row['ra']);
                $aluno->setNome($row['nome']);
                $aluno->setData_nascimento($row['data_nascimento']);
                $aluno->setSexo($row['sexo']);
                $aluno->setRg($row['rg']);
                $aluno->setCampus($row['campus']);
                $aluno->setTelefone($row['telefone']);
                $aluno->setImagem($row['foto']);

                return $rows;
            } else {
                return null;
            }
        } catch (\PDOException $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function pesquisaAlunos() {

        try {

            $sql = "SELECT * FROM aluno";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarJogoJoinAluno($idJogo) {

        try {


            $sql = "select * from time join aluno join time_has_aluno tha where idJogo =:idJogo and tha.time_idJogo = time.idJogo and aluno.ra = tha.aluno_ra ";

            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':idJogo' => $idJogo))) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function buscarJogoJoinAlunoInative($id, $campus) {

        try {


            $sql = "select * from aluno where aluno.ra not in (select time_has_aluno.aluno_ra from time_has_aluno where time_has_aluno.time_idJogo = :id) and aluno.campus = :campus";




            $p_sql = Conexao::getInstance()->prepare($sql);


            if ($p_sql->execute(array(':id' => $id, ':campus' => $campus))) {
                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function selectJogo($id) {

        try {


            $sql = "select * from time where idJogo = :idJogo";

            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':idJogo' => $id))) {

                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function atualizarAlunoSemFoto(Aluno $aluno) {

        try {
            $sql = "update aluno set ra = ?, nome = ?, data_nascimento = ?, sexo = ?, rg = ?, campus = ?, telefone = ? where ra = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $aluno->getRa(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $aluno->getNome(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $aluno->getData_nascimento(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $aluno->getSexo(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $aluno->getRg(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $aluno->getCampus(), PDO::PARAM_STR);
            $p_sql->bindValue(7, $aluno->getTelefone(), PDO::PARAM_STR);
            $p_sql->bindValue(8, $aluno->getRa(), PDO::PARAM_STR);

            if ($p_sql->execute()) {

                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            return false;
        }
    }

    function buscarAlunos() {

        try {

            $sql = "SELECT * FROM aluno";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarAlunosCampus($campus) {

        try {

            $sql = "SELECT * FROM aluno where campus = ?";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $campus, PDO::PARAM_STR);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    /* function atualizarAluno(Aluno $aluno) {


      $sql = "SELECT * FROM aluno";
      $p_sql = Conexao::getInstance()->prepare($sql);
      $p_sql->execute();
      return $p_sql->fetchAll(PDO::FETCH_OBJ);
      } catch (Exception $e) {
      print 'Ocorreu um erro ao tentar executar esta ação. ';
      }
      } */

    function atualizarAluno(Aluno $aluno) {

        try {
            $sql = "update aluno set ra = ?, nome = ?, data_nascimento = ?, sexo = ?, rg = ?, campus = ?, telefone = ?, foto = ? where ra = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $aluno->getRa(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $aluno->getNome(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $aluno->getData_nascimento(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $aluno->getSexo(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $aluno->getRg(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $aluno->getCampus(), PDO::PARAM_STR);
            $p_sql->bindValue(7, $aluno->getTelefone(), PDO::PARAM_STR);
            $p_sql->bindValue(8, $aluno->getImagem(), PDO::PARAM_STR);
            $p_sql->bindValue(9, $aluno->getRa(), PDO::PARAM_STR);

            if ($p_sql->execute()) {

                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    /* function ativarAluno($ra) {
      try {
      $sql = "update aluno set status = true where ra = ?";
      $p_sql = Conexao::getInstance()->prepare($sql);
      $p_sql->bindValue(1, $ra, PDO::PARAM_BOOL);
      $p_sql->execute();
      return $p_sql->rowCount();
      } catch (Exception $e) {

      }
      } */

    function removeAluno($ra) {
        try {

            $sql = "delete from aluno where ra = ?";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $ra, PDO::PARAM_BOOL);
            $p_sql->execute();
            return $p_sql->rowCount();
        } catch (Exception $e) {
            //print_r($e);
            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong>Não foi possível excluir Atleta!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
        }
    }

}
