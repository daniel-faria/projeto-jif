<?php

namespace JIF\Models;

use JIF\Util\Conexao;
use JIF\Entity\Aluno;
use PDO;
use \PDOException;

class ModelIndex {

    public function __construct() {
        
    }

    function fotoGravar($descricao, $imagem) {


        try {
            $sql = "insert into foto (descricao,nome) values(?,?)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $descricao, PDO::PARAM_STR);
            $p_sql->bindValue(2, $imagem, PDO::PARAM_STR);

            if ($p_sql->execute()) {
                return true;
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Erro ao gravar imagem!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    function listarFotos() {

        try {

            $sql = "SELECT * FROM foto ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
            //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function listarFotosQtd() {

        try {

            $sql = "SELECT * FROM foto ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->rowCount();
        } catch (\Exception $e) {
            //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function buscarCracha($id) {
      try {
      $sql = "select * from aluno join time_has_aluno join time where time_has_aluno.aluno_ra = aluno.ra and time.idTime = time_has_aluno.time_idTime and aluno.ra = :id ";
      
      $p_sql = Conexao::getInstance()->prepare($sql);
      $p_sql->bindValue(":id", $id);
      $p_sql->execute();

      //return $p_sql->rowCount();
      return $p_sql->fetchAll(PDO::FETCH_OBJ);
      
      } catch (Exception $e) {
      //print_r($e);
      }

      }
      
       function paginacao($atual, $records_per_page) {
        try {

            $sql = "SELECT * FROM foto LIMIT ".$atual.",".$records_per_page."";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute()) {

               // return $p_sql->fetchAll(PDO::FETCH_OBJ);
                return $p_sql->rowCount();
            } else {
                
            }
        } catch (\PDOException $e) {
       // print_r('<br><br><br><br><br>'.$e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }
     function paginacao2($atual, $records_per_page) {
        try {

            $sql = "SELECT * FROM foto LIMIT ".$atual.",".$records_per_page."";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute()) {

                return $p_sql->fetchAll(PDO::FETCH_ASSOC);
               // return $p_sql->rowCount();
            } else {
                
            }
        } catch (\PDOException $e) {
        //print_r('<br><br><br><br><br>'.$e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }
}
