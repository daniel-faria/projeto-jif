<?php

namespace JIF\Models;

use JIF\Util\Conexao;
use JIF\Entity\Aluno;
use PDO;
use \PDOException;

class ModelAluno {

    public function __construct() {
        
    }

    function cadastrarAluno(Aluno $aluno) {


        try {
            $sql = "insert into aluno (ra,nome,data_nascimento,sexo,rg,campus,telefone,foto) values(?,?,?,?,?,?,?,?)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $aluno->getRa(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $aluno->getNome(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $aluno->getData_nascimento(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $aluno->getSexo(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $aluno->getRg(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $aluno->getCampus(), PDO::PARAM_STR);
            $p_sql->bindValue(7, $aluno->getTelefone(), PDO::PARAM_STR);
            $p_sql->bindValue(8, $aluno->getImagem(), PDO::PARAM_STR);


            if ($p_sql->execute()) {

                return true; // retorna o id do aluno inserido
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Query não executada!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    /* function buscarAluno($ra) {

      try {
      $sql = "select * from aluno where ra = ?";
      $p_sql = Conexao::getInstance()->prepare($sql);
      $p_sql->bindValue(1, $ra, PDO::PARAM_STR);
      $p_sql->execute();
      return $p_sql->fetch(PDO::FETCH_OBJ);
      } catch (Exception $e) {

      }
      } */

    function procuraAluno(Aluno $aluno) {

        try {

            $sql = "SELECT * FROM aluno WHERE ra = :ra";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':ra' => $aluno->getRa()))) {
                $rows = $p_sql->rowCount();
                $row = $p_sql->fetch(PDO::FETCH_SERIALIZE);

                $aluno->setRa($row['ra']);
                $aluno->setNome($row['nome']);
                $aluno->setData_nascimento($row['data_nascimento']);
                $aluno->setSexo($row['sexo']);
                $aluno->setRg($row['rg']);
                $aluno->setCampus($row['campus']);
                $aluno->setTelefone($row['telefone']);
                $aluno->setImagem($row['foto']);

                return $rows;
            } else {
                
            }
        } catch (\PDOException $e) {
       //print_r($e);
        
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    function pesquisaAlunos($campus) {

        try {

            $sql = "SELECT * FROM aluno where campus = :campus";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $campus, PDO::PARAM_STR);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
        //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

function relatorioAlunos() {

        try {

            $sql = "SELECT * FROM aluno ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (\Exception $e) {
        //print_r($e);
        //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }
    function atualizarAlunoSemFoto(Aluno $aluno) {

        try {
            $sql = "update aluno set nome = ?, data_nascimento = ?, sexo = ?, rg = ?, telefone = ? where ra = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(1, $aluno->getNome(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $aluno->getData_nascimento(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $aluno->getSexo(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $aluno->getRg(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $aluno->getTelefone(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $aluno->getRa(), PDO::PARAM_STR);

            if ($p_sql->execute()) {

                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
        //print_r($e);
        //print_r($e);
            return false;
        }
    }

    function buscarAlunos() {

        try {

            $sql = "SELECT * FROM aluno";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
        ///print_r($e);
        
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    
    function buscarAlunosCampus($campus) {

        try {

            $sql = "SELECT * FROM aluno WHERE campus = :campus";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':campus' => $campus))) {

                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
        //print_r($e);
        //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

function buscarAlunoRa($ra) {

        try {

            $sql = "SELECT * FROM aluno WHERE ra =:ra";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':ra' => $ra))) {

                return $p_sql->fetchAll(PDO::FETCH_OBJ);
            } else {
                
            }
        } catch (\PDOException $e) {
        //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }
       
       
   
    
    
     function buscarAlunoFoto(Aluno $aluno) {

        try {

            $sql = "SELECT * FROM aluno WHERE ra = :ra";
            $p_sql = Conexao::getInstance()->prepare($sql);

            if ($p_sql->execute(array(':ra' => $aluno->getRa()))) {
                $rows = $p_sql->rowCount();
                $row = $p_sql->fetch(PDO::FETCH_SERIALIZE);

                $aluno->setRa($row['foto']);

                return 1;
            } else {
                
            }
        } catch (\PDOException $e) {
        //print_r($e);
            print 'Ocorreu um erro ao tentar executar esta ação. ';
        }
    }

    /* function atualizarAluno(Aluno $aluno) {


      $sql = "SELECT * FROM aluno";
      $p_sql = Conexao::getInstance()->prepare($sql);
      $p_sql->execute();
      return $p_sql->fetchAll(PDO::FETCH_OBJ);
      } catch (Exception $e) {
      print 'Ocorreu um erro ao tentar executar esta ação. ';
      }
      } */

    function atualizarAluno(Aluno $aluno) {

        try {
            $sql = "update aluno set nome = ?, data_nascimento = ?, sexo = ?, rg = ?, telefone = ?, foto = ? where ra = ?";

            $p_sql = Conexao::getInstance()->prepare($sql);

          
            $p_sql->bindValue(1, $aluno->getNome(), PDO::PARAM_STR);
            $p_sql->bindValue(2, $aluno->getData_nascimento(), PDO::PARAM_STR);
            $p_sql->bindValue(3, $aluno->getSexo(), PDO::PARAM_STR);
            $p_sql->bindValue(4, $aluno->getRg(), PDO::PARAM_STR);
            $p_sql->bindValue(5, $aluno->getTelefone(), PDO::PARAM_STR);
            $p_sql->bindValue(6, $aluno->getImagem(), PDO::PARAM_STR);
            $p_sql->bindValue(7, $aluno->getRa(), PDO::PARAM_STR);

            if ($p_sql->execute()) {

                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            //print_r($e);
            return false;
        }
    }

    /* function ativarAluno($ra) {
      try {
      $sql = "update aluno set status = true where ra = ?";
      $p_sql = Conexao::getInstance()->prepare($sql);
      $p_sql->bindValue(1, $ra, PDO::PARAM_BOOL);
      $p_sql->execute();
      return $p_sql->rowCount();
      } catch (Exception $e) {

      }
      } */

 function deleteAluno($id) {
        try {
            $sql = "DELETE from aluno WHERE ra = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();

            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            ///print_r($e);
        }
    }
    
    function removeAluno($ra) {
        try {

            $sql = "delete from aluno where ra = ?";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(1, $ra, PDO::PARAM_BOOL);
            $p_sql->execute();
            return $p_sql->rowCount();
        } catch (Exception $e) {
            //print_r($e);
        }
    }

}
