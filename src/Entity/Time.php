<?php

namespace JIF\Entity;

use JIF\Entity\Modalidade;
use JIF\Entity\Aluno;
use JIF\Entity\Usuario;

class Time {

    private $nome;
    private $placar;
    private $modalidade;
    private $aluno;
    private $tecnico;
    private $qtdAlunos;

    /* function __construct($nome, $placar, Modalidade $modalidade, Aluno $aluno, Usuario $tecnico) {
      $this->nome = $nome;
      $this->placar = $placar;
      $this->modalidade = $modalidade;
      $this->aluno = $aluno;
      $this->tecnico = $tecnico;
      } */

    function __construct() {
        
    }

    function getQtdAlunos() {
        return $this->qtdAlunos;
    }

    function setQtdAlunos($qtdAlunos) {
        $this->qtdAlunos = $qtdAlunos;
    }

    function getNome() {
        return $this->nome;
    }

    function getPlacar() {
        return $this->placar;
    }

    function getModalidade() {
        return $this->modalidade;
    }

    function getAluno() {
        return $this->aluno;
    }

    function getTecnico() {
        return $this->tecnico;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setPlacar($placar) {
        $this->placar = $placar;
    }

    function setModalidade($modalidade) {
        $this->modalidade = $modalidade;
    }

    function setAluno($aluno) {
        $this->aluno = $aluno;
    }

    function setTecnico($tecnico) {
        $this->tecnico = $tecnico;
    }

}
