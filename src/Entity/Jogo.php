<?php

namespace JIF\Entity;

class Jogo {

    private $dataInicio;
    private $dataHoraFim;
    private $local;
    private $time1ID;
    private $time2ID;
    private $idJogo;
    private $placar1;
    private $placar2;
   
    function getDataInicio() {
        return $this->dataInicio;
    }

    function getDataHoraFim() {
        return $this->dataHoraFim;
    }

    function getLocal() {
        return $this->local;
    }

    function getTime1ID() {
        return $this->time1ID;
    }

    function getTime2ID() {
        return $this->time2ID;
    }

    function getIdJogo() {
        return $this->idJogo;
    }

    function getPlacar1() {
        return $this->placar1;
    }

    function getPlacar2() {
        return $this->placar2;
    }

    function setDataInicio($dataInicio) {
        $this->dataInicio = $dataInicio;
    }

    function setDataHoraFim($dataHoraFim) {
        $this->dataHoraFim = $dataHoraFim;
    }

    function setLocal($local) {
        $this->local = $local;
    }

    function setTime1ID($time1ID) {
        $this->time1ID = $time1ID;
    }

    function setTime2ID($time2ID) {
        $this->time2ID = $time2ID;
    }

    function setIdJogo($idJogo) {
        $this->idJogo = $idJogo;
    }

    function setPlacar1($placar1) {
        $this->placar1 = $placar1;
    }

    function setPlacar2($placar2) {
        $this->placar2 = $placar2;
    }

        function __construct() {
        
    }


}
