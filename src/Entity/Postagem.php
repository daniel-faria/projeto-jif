<?php

namespace JIF\Entity;

class Postagem {

    private $id;
    private $dataHora;
    private $titulo;
    private $texto;
    private $foto;
    private $Usuario_cpf;
    private $aprovada;
    private $topo;

    public function __construct() {
        
    }

    function getId() {
        return $this->id;
    }

    function getDataHora() {
        return $this->dataHora;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getTexto() {
        return $this->texto;
    }

    function getFoto() {
        return $this->foto;
    }

    function getUsuario_cpf() {
        return $this->Usuario_cpf;
    }

    function getAprovada() {
        return $this->aprovada;
    }

    function getTopo() {
        return $this->topo;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDataHora($dataHora) {
        $this->dataHora = $dataHora;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setTexto($texto) {
        $this->texto = $texto;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    function setUsuario_cpf($Usuario_cpf) {
        $this->Usuario_cpf = $Usuario_cpf;
    }

    function setAprovada($aprovada) {
        $this->aprovada = $aprovada;
    }

    function setTopo($topo) {
        $this->topo = $topo;
    }

}
