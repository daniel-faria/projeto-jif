<?php

namespace JIF\Entity;

class Aluno {

    private $ra;
    private $nome;
    private $data_nascimento;
    private $sexo;
    private $rg;
    private $campus;
    private $telefone;
    private $imagem;

    /* public function __construct($ra, $nome, $data_nascimento, $sexo, $rg, $campus, $telefone, $imagem) {
      $this->ra = $ra;
      $this->nome = $nome;
      $this->datanascimento = $datanascimento;
      $this->sexo = $sexo;
      $this->rg = $rg;
      $this->campus = $campus;
      $this->telefone = $telefone;
      $this->imagem = $imagem;
      } */

    public function __construct() {
        
    }

    function getRa() {
        return $this->ra;
    }

    function getNome() {
        return $this->nome;
    }

    function getData_nascimento() {
        return $this->data_nascimento;
    }

    function getSexo() {
        return $this->sexo;
    }

    function getRg() {
        return $this->rg;
    }

    function getCampus() {
        return $this->campus;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function getImagem() {
        return $this->imagem;
    }

    function setRa($ra) {
        $this->ra = $ra;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setData_nascimento($data_nascimento) {
        $this->data_nascimento = $data_nascimento;
    }

    function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    function setRg($rg) {
        $this->rg = $rg;
    }

    function setCampus($campus) {
        $this->campus = $campus;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    function setImagem($imagem) {
        $this->imagem = $imagem;
    }

}
