<?php

namespace JIF\Entity;


class Modalidade{
    
    private $id;
    private $esporte;
    private $coletivo;
    private $qtdAtletas;
    private $masculino;
        
    function __construct($id, $esporte, $coletivo, $qtdAtletas, $masculino) {
        $this->id = $id;
        $this->esporte = $esporte;
        $this->coletivo = $coletivo;
        $this->qtdAtletas = $qtdAtletas;
        $this->masculino = $masculino;
    }
    
    function getId() {
        return $this->id;
    }

    function getEsporte() {
        return $this->esporte;
    }

    function getColetivo() {
        return $this->coletivo;
    }

    function getQtdAtletas() {
        return $this->qtdAtletas;
    }

    function getMasculino() {
        return $this->masculino;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEsporte($esporte) {
        $this->esporte = $esporte;
    }

    function setColetivo($coletivo) {
        $this->coletivo = $coletivo;
    }

    function setQtdAtletas($qtdAtletas) {
        $this->qtdAtletas = $qtdAtletas;
    }

    function setMasculino($masculino) {
        $this->masculino = $masculino;
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

