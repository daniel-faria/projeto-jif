<?php

namespace JIF\Entity;

class Usuario {

    private $cpf;
    private $nome;
    private $siape;
    private $email;
    private $campus;
    private $telefone;
    private $senha;
    private $permissao;
    private $data_cadastro;
    private $data_ultimo_login;

    function __construct() {
        
    }

    function getData_ultimo_login() {
        return $this->data_ultimo_login;
    }

    function setData_ultimo_login($data_ultimo_login) {
        $this->data_ultimo_login = $data_ultimo_login;
    }

    function getData_cadastro() {
        return $this->data_cadastro;
    }

    function setData_cadastro($data_cadastro) {
        $this->data_cadastro = $data_cadastro;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getNome() {
        return $this->nome;
    }

    function getSiape() {
        return $this->siape;
    }

    function getEmail() {
        return $this->email;
    }

    function getCampus() {
        return $this->campus;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function getSenha() {
        return $this->senha;
    }

    function getPermissao() {
        return $this->permissao;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setSiape($siape) {
        $this->siape = $siape;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setCampus($campus) {
        $this->campus = $campus;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function setPermissao($permissao) {
        $this->permissao = $permissao;
    }

}
