<?php

namespace JIF\Util;

class Sessao {

    function __construct() {
       
        //ini_set('session.save_path',realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/../session'));
        
       // ini_set('session.gc_maxlifetime', 7200);
        //ini_set('session.cookie_lifetime', 7200);
        
    }

    function start() {

        return session_start();
    }

    function add($chave, $valor) {
        $_SESSION['jif'][$chave] = $valor;
    }

    function add2($chave, $valor) {
        $_SESSION['game'][$chave] = $valor;
    }

    function get($chave) {
        if (isset($_SESSION['jif'][$chave]))
            return $_SESSION['jif'][$chave];
        return '';
    }

    function rem2($chave) {
        if (isset($_SESSION['game'][$chave]))
            session_unset($_SESSION['game'][$chave]);
    }

    function rem($chave) {
        if (isset($_SESSION['jif'][$chave]))
            session_unset($_SESSION['jif'][$chave]);
    }

    function todaSessao() {
        if (isset($_SESSION['jif']))
            return $_SESSION['jif'];
    }

    function del() {
        if (isset($_SESSION['jif']))
            session_unset($_SESSION['jif']);
        session_destroy();
    }

    function existe($chave) {
        if (isset($_SESSION['jif'][$chave]))
            return true;
        return false;
    }

    function status() {
        return session_status();
    }

}
