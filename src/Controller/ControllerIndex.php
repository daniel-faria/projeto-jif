<?php

namespace JIF\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use JIF\Util\Sessao;
use JIF\Models\ModelIndex;
use JIF\Models\ModelPostagem;
use JIF\Models\ModelUsuario;
use Zebra_Pagination;

class ControllerIndex {

    private $twig;
    private $request;
    private $response;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    function redirect() {
        if ($this->sessao->existe('logado')) {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function redirectSemPermissaoAdmin() {
        if (!$this->sessao->existe('administrador')) {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function redirectSemPermissao() {
        if (!$this->sessao->existe('logado')) {
            $redirect = new RedirectResponse('/login');
            $redirect->send();
        }
    }

    public function show() {
        //$this->redirect();

        $model = new ModelIndex();
        $fotos = $model->listarFotos();

        $campus = $this->sessao->get("campusLogado");
        
        $modelPostagem = new ModelPostagem();
        $modelUsuario = new ModelUsuario();
        
        $usuarios = $modelUsuario->listarPessoas();

        //return $this->response->setContent($this->twig->render("/base.twig", ["postagensInativas" => $postagensInativas, "postagensAtivas" => $postagensAtivas, 'fotos' => $fotos]));
        
        if($this->sessao->get('logado')){
            $postagens = $modelPostagem->listarPostagemAll();
        }
        else{
            $postagens = $modelPostagem->listarPostagem(0);
        }
        return $this->response->setContent($this->twig->render("/base.twig", ["postagens" => $postagens, 'fotos' => $fotos, "usuarios" => $usuarios]));
    }

    public function showCracha() {
        //$this->redirect();
        return $this->response->setContent($this->twig->render("/visualizarCracha.twig"));
    }

    public function telaSobreParacatu() {
        //$this->redirect();
        return $this->response->setContent($this->twig->render("/telaSobreParacatu.twig"));
    }

    public function telaSobreDesen() {
        //$this->redirect();
        return $this->response->setContent($this->twig->render("/telaSobreTeam.twig"));
    }
    public function telaPontos() {
        //$this->redirect();
        return $this->response->setContent($this->twig->render("/telaPontosDeInteresse.twig"));
    }

    public function showFotos() {

        $model = new ModelIndex();
        $fotos = $model->listarFotos();


        /* $records_per_page = 1;
          $pagination = new Zebra_Pagination();

          $conta = ($pagination->get_page() - 1) * $records_per_page;
          $rows = $model->paginacao($conta,$records_per_page);
          $fotos = $model->paginacao2($conta,$records_per_page);

          $pagination->records($rows);
          $pagination->records_per_page($records_per_page); */


        //$array =  (array) $yourObject;
        //$this->redirect();
        //['pagination' => $pagination->render()]

        return $this->response->setContent($this->twig->render("/fotos.twig", ['fotos' => $fotos]));
    }

    public function erro404AdminPage() {
        //$this->redirectSemPermissao();
        return $this->response->setContent($this->twig->render("/404admin.twig"));
    }

    public function erro404() {
        //$this->redirect();
        return $this->response->setContent($this->twig->render("/404.twig"));
    }

    public function showDashboard() {
        $this->redirectSemPermissao();
        return $this->response->setContent($this->twig->render("/baseAdmin.twig"));
    }

    public function searchCracha() {
        $cpf = $this->request->get('ra');
        $nomeDosTimes = ' - ';
        $model = new ModelIndex();

        $cracha = $model->buscarCracha($cpf);

        if ($cracha != null) {

            $i = 0;

            foreach ($cracha as $value) {
                $this->sessao->add("crachaNome", $value->nome);
                $this->sessao->add("crachaCampus", $value->campus);
                $this->sessao->add("crachaFoto", $value->foto);
                // if (++$i == 1)
                break;
            }

            foreach ($cracha as $value) {
                $nomeDosTimes .= '  ' . $value->nomeTime . ' - ';
            }

            $this->sessao->add("crachaTimes", $nomeDosTimes);

            echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Aluno encontrado!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            ?>  <script language="javascript">
                setTimeout(function () {
                    window.location.reload();
                }, 1500);
            </script>

            <?php

        } else {
            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Aluno não encontrado!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
        }
    }

    public function fotoGravar() {


        $descricao = $this->request->get('descricao');
        $imagem = $this->request->files->get('imagem');

        $target_file = __DIR__ . '/../../public_html/img/galeria' . $imagem->getClientOriginalName();
        $miniatura_file = __DIR__ . '/../../public_html/img/miniaturas/' . $imagem->getClientOriginalName();
        $temp_file = $_FILES["imagem"]["tmp_name"];
        $name = $_FILES["imagem"]["name"];
        //$tmpName = $_FILES["imagem"]["tmp_name"];
        $type = $_FILES["imagem"]["type"];
        $sizes = $_FILES["imagem"]["size"];
        $errorMsg = $_FILES["imagem"]["error"];
        $explode = explode(".", $name);
        $extension = end($explode);

        if ($sizes > 5242880) {
            echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Selecione arquivo menor que 5MB!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

            exit();
        } else if (!preg_match("/\.(gif|jpg|png|jpeg)$/i", $name)) {

            echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Formatos permitidos: (gif|jpg|png|jpeg)!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

            exit();
        } else if ($errorMsg == 1) {
            echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong>Ocorreu algum erro ao processar a imagem tente com outra!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            exit();
        } elseif (move_uploaded_file($temp_file, $target_file)) {


            $source = imagecreatefromjpeg($target_file);
            list($width, $height) = getimagesize($target_file);

            $newwidth = $width / 3;
            //$newwidth = 200;
            $newheight = $height / 3;
            //$newheight = 200;

            $destination = imagecreatetruecolor($newwidth, $newheight);
            imagecopyresampled($destination, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

            imagejpeg($destination, $miniatura_file, 100);

            $model = new ModelIndex();

            if ($model->fotoGravar($descricao, $imagem->getClientOriginalName())) {

                //$quantidade = $model->listarFotosQtd(); paginacao
                //$this->sessao->add("quantidadeGaleria",$quantidade);

                echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Foto Salva na galeria!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                ?>  <script language="javascript">
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                </script>

                <?php

            } else {
                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível salvar a foto !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            }
        } else {

            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> A imagem não pode ser movida!!! 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            ?>  <script language="javascript">
                window.location.href = "#";
            </script>

            <?php

        }
    }

    public function removeFoto() {
        
    }

}
