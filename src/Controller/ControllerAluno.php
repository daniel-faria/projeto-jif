<?php

namespace JIF\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use JIF\Models\ModelAluno;
use JIF\Entity\Aluno;
use JIF\Util\Sessao;
use Dompdf\Dompdf;
use Dompdf\Options;

class ControllerAluno {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function removeCRUDMenuSessao() {

        if (!empty($this->sessao->get('removeAluno'))) {
            $this->sessao->add('removeAluno', '');
        } elseif (!empty($this->sessao->get('updateAluno'))) {
            $this->sessao->add('updateAluno', '');
            $this->sessao->add("msg", '');
        } elseif (!empty($this->sessao->get('viewAluno'))) {
            $this->sessao->add('viewAluno', '');
            $this->sessao->add("msg", '');
        }
    }

    public function telaAtualizaAluno($ra) {

        $this->redirectSemPermissaoTec();
        $this->removeCRUDMenuSessao();
        $aluno = new ModelAluno();
        $aluno = $aluno->buscarAlunoRa($ra);

        // $this->sessao->add('updateAluno', 'yes');

        $this->response->setContent($this->twig->render("/atualizaAluno.twig", ['aluno' => $aluno]));
    }

    /* public function telaRemoveAluno() {
      $this->redirectSemPermissaoTec();
      $this->removeCRUDMenuSessao();

      $this->sessao->add('removeAluno', 'yes');

      // $this->response->setContent($this->twig->render("/buscaAluno.twig"));
      } */

    public function telaBuscaAluno() {
        $this->redirectSemPermissaoTec();
        $this->removeCRUDMenuSessao();
        $msg = '';
        $this->sessao->add('viewAluno', 'yes');

        $model = new ModelAluno();

        //var_dump(gd_info());

        if (empty($this->sessao->get('administrador'))) {
            $alunos = $model->pesquisaAlunos($this->sessao->get('campusLogado'));
        } else {
            $alunos = $model->relatorioAlunos();
        }
        return $this->response->setContent($this->twig->render("/buscaAluno.twig", ["alunos" => $alunos]));
        //return $this->response->setContent($this->twig->render("/buscaAluno.twig", ["alunos" => $atletas, "modalidades" => $modalidades]));
    }

    //public function busca() {
    //     $this->redirect();
    //     $this->response->setContent($this->twig->render("/busca.twig"));
    // }

    function redirect() {
        if (!$this->sessao->existe('logado')) {
            //  $redirect = new RedirectResponse('/');
            //$redirect->send();
            ?>  <script language="javascript">
                window.location.href = "/";
            </script>

            <?php

        }
    }

    function redirectSemPermissaoTec() {
        if (!$this->sessao->existe('administrador')) {
            //$redirect = new RedirectResponse('/');
            // $redirect->send();

            if (!$this->sessao->existe('tecnico')) {
                ?>  <script language="javascript">
                    window.location.href = "/";
                </script>

                <?php

            }
        }
    }

    function redirectToAlunos() {

        //$redirect = new RedirectResponse('/');
        // $redirect->send();
        ?>  <script language="javascript">
            window.location.href = "/painel/aluno/view";
        </script>

        <?php

    }

    function redirectSemPermissaoAdmin() {
        if (empty($this->sessao->existe('administrador'))) {
            //$redirect = new RedirectResponse('/');
            // $redirect->send();
            ?>  <script language="javascript">
                window.location.href = "/";
            </script>

            <?php

        }
    }

    function redirectSemPermissao() {
        if (empty($this->sessao->existe('logado'))) {
            //$redirect = new RedirectResponse('/login');
            //$redirect->send();
            ?>  <script language="javascript">
                window.location.href = "/";
            </script>

            <?php

        }
    }

    function viewAluno() {
        $this->response->setContent($this->twig->render("/cadastroAluno.twig"));
    }

    //function pesquisarAluno() {
    //  $this->response->setContent($this->twig->render("/pequisaAluno.twig"));
    //}

    function resize_image($file, $w, $h, $crop = FALSE) {
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width - ($width * abs($r - $w / $h)));
            } else {
                $height = ceil($height - ($height * abs($r - $w / $h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w / $h > $r) {
                $newwidth = $h * $r;
                $newheight = $h;
            } else {
                $newheight = $w / $r;
                $newwidth = $w;
            }
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }

    function gravarAluno() {


        $ra = $this->request->get('ra');
        $nome = $this->request->get('nome');

        $data_nascimento = $this->request->get('data_nascimento');
        $sexo = $this->request->get('sexo');
        $rg = $this->request->get('rg');
        $campus = $this->request->get('campus');
        $telefone = $this->request->get('telefone');


        $date1 = strtr($data_nascimento, '/', '-');
        $date_born = date('Y-m-d', strtotime($date1));
        $date_limit = date('Y', strtotime($date1));


        $imagem = $this->request->files->get('imagem');

        if ($date_limit < 2000) {
            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Ano da data de nascimento mínimo: 2000!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            ?>  <script language="javascript">
                window.location.href = "#";
            </script>

            <?php

        } else {
            if (empty($imagem) AND $imagem == NULL) {
                $aluno = new Aluno();
                $aluno->setRa($ra);
                $aluno->setNome($nome);
                $aluno->setData_nascimento($date_born);
                $aluno->setSexo($sexo);
                $aluno->setRg($rg);
                $aluno->setCampus($campus);
                $aluno->setTelefone($telefone);
                $aluno->setImagem('empty.jpg');

                $modelsAluno = new ModelAluno();

                if ($modelsAluno->cadastrarAluno($aluno)) {

                    echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Atleta Cadastrado no sistema!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                     ?>  <script language="javascript">
                      setTimeout(function () {
                      window.location.href = "/painel/aluno";
                      }, 2000);
                      </script>

                      <?php 
                } else {
                    echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível cadastrar o atleta (RA ou RG já cadastrado) !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                }
            } else {


                $target_file = __DIR__ . '/../../public_html/img/' . $ra . $imagem->getClientOriginalName();
                $miniatura_file = __DIR__ . '/../../public_html/img/miniaturas/' . $ra . $imagem->getClientOriginalName();
                $temp_file = $_FILES["imagem"]["tmp_name"];
                $name = $_FILES["imagem"]["name"];
                //$tmpName = $_FILES["imagem"]["tmp_name"];
                $type = $_FILES["imagem"]["type"];
                $sizes = $_FILES["imagem"]["size"];
                $errorMsg = $_FILES["imagem"]["error"];
                $explode = explode(".", $name);
                $extension = end($explode);

                if ($sizes > 5242880) {
                    echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Selecione arquivo menor que 5MB!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

                    exit();
                } else if (!preg_match("/\.(gif|jpg|png|jpeg)$/i", $name)) {

                    echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Selecione arquivo menor que 5MB!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';


                    exit();
                } else if ($errorMsg == 1) {
                    echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong>Ocorreu algum erro ao processar a imagem tente com outra!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    exit();
                } elseif (move_uploaded_file($temp_file, $target_file)) {


                    $source = imagecreatefromjpeg($target_file);
                    list($width, $height) = getimagesize($target_file);

                    $newwidth = $width / 3;
                    //$newwidth = 200;
                    $newheight = $height / 3;
                    //$newheight = 200;

                    $destination = imagecreatetruecolor($newwidth, $newheight);
                    imagecopyresampled($destination, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                    imagejpeg($destination, $miniatura_file, 100);

                    $aluno = new Aluno();
                    $aluno->setRa($ra);
                    $aluno->setNome($nome);
                    $aluno->setData_nascimento($date_born);
                    $aluno->setSexo($sexo);
                    $aluno->setRg($rg);
                    $aluno->setCampus($campus);
                    $aluno->setTelefone($telefone);
                    $aluno->setImagem($ra . $imagem->getClientOriginalName());

                    $modelsAluno = new ModelAluno();

                    if ($modelsAluno->cadastrarAluno($aluno)) {

                        echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Atleta Cadastrado no sistema!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                        ?>  <script language="javascript">
                      setTimeout(function () {
                      window.location.href = "/painel/aluno";
                      }, 2000);
                      </script>

                      <?php
                    } else {
                        echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível cadastrar o atleta (RA ou RG já cadastrado) !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    }
                } else {

                    echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> A imagem não pode ser movida!!! 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    ?>  <script language="javascript">
                        window.location.href = "#";
                    </script>

                    <?php

                }
            }
        }
    }

    function atualizarAluno() {

        $this->redirectSemPermissaoTec();

        $ra = $this->request->get('ra');

        $nome = $this->request->get('nome');
        $data_nascimento = $this->request->get('data_nascimento');
        $sexo = $this->request->get('sexo');
        $rg = $this->request->get('rg');
        $campus = $this->request->get('campus');
        $telefone = $this->request->get('telefone');
        $imagem = $this->request->files->get('imagem');

        $img = $imagem;

        $date1 = strtr($data_nascimento, '/', '-');
        $date_born = date('Y-m-d', strtotime($date1));
        $date_limit = date('Y', strtotime($date1));


        if ($date_limit < 2000) {
            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Ano da data de nascimento mínimo: 2000!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

            //$this->sessao->add("msgAluno",$msg);
            //$redirect = new RedirectResponse('/painel/aluno/update/' . $id);
            //$redirect->send();
        } else {
            if (!empty($imagem) AND $imagem != NULL) {

                if ($nome == $this->sessao->get("nomeAluno") && $ra == $this->sessao->get("raAluno") &&
                        $data_nascimento == $this->sessao->get("data_nascimentoAluno") &&
                        $sexo == $this->sessao->get("sexoAluno") && $rg == $this->sessao->get("rgAluno") && $telefone == $this->sessao->get("telefoneAluno") && $this->sessao->get("fotoAluno") == $ra . $imagem->getClientOriginalName()) {

                    echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Dados inalterados!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                } else {
                    $target_file = __DIR__ . '/../../public_html/img/' . $ra . $imagem->getClientOriginalName();
                    $miniatura_file = __DIR__ . '/../../public_html/img/miniaturas/' . $ra . $imagem->getClientOriginalName();
                    $temp_file = $_FILES["imagem"]["tmp_name"];
                    $name = $_FILES["imagem"]["name"];
                    //$tmpName = $_FILES["imagem"]["tmp_name"];
                    $type = $_FILES["imagem"]["type"];
                    $sizes = $_FILES["imagem"]["size"];
                    $errorMsg = $_FILES["imagem"]["error"];
                    $explode = explode(".", $name);
                    $extension = end($explode);

                    if ($sizes > 5242880) {
                        echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Selecione arquivo menor que 5MB!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

                        exit();
                    } else if (!preg_match("/\.(gif|jpg|png|jpeg)$/i", $name)) {

                        echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Selecione arquivo menor que 5MB!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';


                        exit();
                    } else if ($errorMsg == 1) {
                        echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong>Ocorreu algum erro ao processar a imagem tente com outra!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                        exit();
                    } elseif (move_uploaded_file($temp_file, $target_file)) {



                        $source = imagecreatefromjpeg($target_file);
                        list($width, $height) = getimagesize($target_file);

                        $newwidth = $width / 3;
                        //$newwidth = 200;
                        $newheight = $height / 3;
                        //$newheight = 200;



                        $destination = imagecreatetruecolor($newwidth, $newheight);
                        imagecopyresampled($destination, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                        imagejpeg($destination, $miniatura_file, 100);


                        $aluno = new Aluno();
                        $aluno->setRa($ra);
                        $aluno->setNome($nome);
                        $aluno->setData_nascimento($date_born);
                        $aluno->setSexo($sexo);
                        $aluno->setRg($rg);
                        $aluno->setCampus($campus);
                        $aluno->setTelefone($telefone);
                        $aluno->setImagem($ra . $imagem->getClientOriginalName());

                        $modelsAluno = new ModelAluno();
                        if ($modelsAluno->atualizarAluno($aluno)) {

                            //$img =  __DIR__ . '/../../public_html/img/'.$img;
                            // echo $img;
                            // chdir($FilePath);
                            // chown($img, 465);
                            // $do = unlink($img);
                            // if ($do == "1") {
                            //    echo "A antiga foto foi removida com sucesso!.";
                            //  } else {
                            //    echo "Ocorreu um erro ao tentar substituir a imagem!";
                            // }

                            echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Dados alterados!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                            $this->sessao->add('raAluno', $ra);
                            $this->sessao->add('nomeAluno', $nome);
                            $this->sessao->add('data_nascimentoAluno', $data_nascimento);
                            $this->sessao->add('sexoAluno', $sexo);
                            $this->sessao->add('rgAluno', $rg);
                            $this->sessao->add('campusAluno', $campus);
                            $this->sessao->add('telefoneAluno', $telefone);
                            $this->sessao->add('fotoAluno', $ra . $imagem->getClientOriginalName());
                            $this->sessao->add('nomeLastFoto', $imagem->getClientOriginalName());
                            ?>  <script language="javascript">
                                setTimeout(function () {
                                    window.location.reload();
                                }, 2000);
                            </script>

                            <?php

                        } else {
                            echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível atualizar o atleta (RA ou RG já cadastrado) !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                        }
                    } else {
                        echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> A imagem não pode ser movida!!! Tente uma imagem de tamanho menor!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    }
                }
            } elseif (empty($imagem) || $imagem == NULL) {

                if ($nome == $this->sessao->get("nomeAluno") && $ra == $this->sessao->get("raAluno") &&
                        $data_nascimento == $this->sessao->get("data_nascimentoAluno") &&
                        $sexo == $this->sessao->get("sexoAluno") && $rg == $this->sessao->get("rgAluno") && $telefone == $this->sessao->get("telefoneAluno")) {

                    echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Dados inalterados!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                } else {
                    $aluno = new Aluno();
                    $aluno->setRa($ra);
                    $aluno->setNome($nome);
                    $aluno->setData_nascimento($date_born);
                    $aluno->setSexo($sexo);
                    $aluno->setRg($rg);
                    $aluno->setCampus($campus);
                    $aluno->setTelefone($telefone);

                    $modelsAluno = new ModelAluno();
                    if ($modelsAluno->atualizarAlunoSemFoto($aluno)) {

                        echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Dados atualizados (foto mantida)!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

                        $this->sessao->add('raAluno', $ra);
                        $this->sessao->add('nomeAluno', $nome);
                        $this->sessao->add('data_nascimentoAluno', $data_nascimento);
                        $this->sessao->add('sexoAluno', $sexo);
                        $this->sessao->add('rgAluno', $rg);
                        $this->sessao->add('campusAluno', $campus);
                        $this->sessao->add('telefoneAluno', $telefone);
                        ?>  <script language="javascript">
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        </script>

                        <?php

                    } else {
                        echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível atualizar o atleta (RA ou RG duplicado) !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

                        echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Ano da data de nascimento mínimo: 2000!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    }
                }
            }
        }
    }

    /* function buscarAluno() {
      $ra = $this->request->get('ra');
      $modelAluno = new ModelAluno();
      $dados = $modelAluno->buscarAluno($ra);

      $this->response->setContent($this->twig->render("/alterarAluno.twig", ["dados" => $dados]));
      }
     */

    public function fechaCRUD() {
        if ($this->sessao->get('updateAluno') != "") {
            $this->sessao->add('buscaEfetivadaUpdateAluno', 'sucesso');
            $this->sessao->add('buscaEfetivada
            Aluno', '');
            $this->sessao->add('buscaEfetivadaViewAluno', '');
        } elseif ($this->sessao->get('removeAluno') != "") {
            $this->sessao->add('buscaEfetivadaUpdateAluno', '');
            $this->sessao->add('buscaEfetivadaRemoveAluno', 'sucesso');
            $this->sessao->add('buscaEfetivadaViewAluno', '');
        } elseif ($this->sessao->get('viewAluno') != "") {
            $this->sessao->add('buscaEfetivadaUpdateAluno', '');
            $this->sessao->add('buscaEfetivadaRemoveAluno', '');
            $this->sessao->add('buscaEfetivadaViewAluno', 'sucesso');
        }
    }

    public function buscaEfetivarAluno() {

        $this->redirectSemPermissaoTec();

        $ra = $this->request->get('ra');

        if (!empty($ra)) {
            $this->sessao->add("relatorio", "");
            $aluno = new Aluno();
            $aluno->setRa($ra);
            $model = new ModelAluno();

            $sucesso = $model->procuraAluno($aluno);

            if ($sucesso > 0) {
                echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Achou um aluno com esse RA!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                ?>  <script language="javascript">
                    window.location.href = "#";
                </script>

                <?php

                $data = date("d/m/Y", strtotime($aluno->getData_nascimento()));

                $this->sessao->add('raAluno', $ra);
                $this->sessao->add('nomeAluno', $aluno->getNome());
                $this->sessao->add('data_nascimentoAluno', $data);
                $this->sessao->add('sexoAluno', $aluno->getSexo());
                $this->sessao->add('rgAluno', $aluno->getRg());
                $this->sessao->add('campusAluno', $aluno->getCampus());
                $this->sessao->add('telefoneAluno', $aluno->getTelefone());
                $this->sessao->add('fotoAluno', $aluno->getImagem());


                $nome = $aluno->getImagem();
                $parte = substr($nome, 11, 1000);


                $this->sessao->add('nomeLastFoto', $parte);

                $this->fechaCRUD();
                ?>  <script language="javascript">

                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);

                </script>



                <?php

                ;
            } else {
                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Aluno não encontrado!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            }
        } else {
            /* $model = new ModelAluno();
              $dados = $model->pesquisaAlunos($ra);
              $this->sessao->add("relatorioAlunos", "sim");
              $this->response->setContent(["dados" => $dados]); */

            //return $this->container->get('templating')->renderResponse("/buscaAluno.twig", ["dados" => $dados]);
            //$twig->render('index.html', ['name' => 'Fabien']);
        }
    }

    function relatorio() {
        //$this->redirectSemPermissaoTec();
        $model = new ModelAluno();

        $campus = $this->sessao->get("campusLogado");
        $dados = $model->pesquisaAlunos($campus);


        $pdfOptions = new Options();
        $pdfOptions->setIsRemoteEnabled(true);
        $pdfOptions->set('defaultFont', 'Arial');

        //$pdfOptions->set('tempDir', '/public_html/directory/pdf-export/tmp');
        //$dompdf = new Dompdf($options);
        //array('enable_remote' => true),
        $dompdf = new Dompdf($pdfOptions);


        $html = '<div class="table-responsive">
            <table style ="
    background-color: #f2f2f2;
    font-family: Arial, Helvetica, sans-serif;

        " class="table table-striped table-bordered">
                <thead style= "
                background-color:#050E33;color: white;
               
            "
                class="thead-dark">
                    <tr>
                        <th>RA: </th>
                        <th>Nome:</th>
                        <th>Data de Nascimento:</th>
                        <th>Sexo:</th>
                        <th>RG:</th>
                        <th>Campus:</th>
                        <th>Telefone:</th>
                        <th>Foto:</th>
                    </tr>
                </thead>
                <tbody>';

        foreach ($dados as $listar) {
            $date1 = strtr($listar->data_nascimento, '/', '-');
            $date_born = date('d-m-Y', strtotime($date1));


            // A few settings
            //$image = 'https://www.jiftm.com/img/' . $listar->foto;
// Read image path, convert to base64 encoding
            //$imageData = base64_encode(file_get_contents($image));


            $f;
            $l;
            if (headers_sent($f, $l)) {
                echo $f, '<br/>', $l, '<br/>';
                die('now detect line');
            }

// Format the image SRC:  data:{mime};base64,{data};
            // $src = 'data:' . mime_content_type($image) . ';base64,' . $imageData;
// Echo out a sample image


            $html .= ''
                    . ''
                    . '<tr style="background-color:#FFF;">'
                    . '<td>' . $listar->ra . '</td>'
                    . '<td>' . $listar->nome . '</td>'
                    . '<td>' . $date_born . '</td>'
                    . '<td>' . $listar->sexo . '</td>'
                    . '<td>' . $listar->rg . '</td>'
                    . '<td>' . $listar->campus . '</td>'
                    . '<td>' . $listar->telefone . '</td>'
                    . '<td><img src="https://www.jiftm.com/img/miniaturas/' . $listar->foto . '"  width="50px" height="50px"  ></td>';
        }

        $html .= "</tbody>";

        $dompdf->loadHtml($html);


        $dompdf->setPaper('A4', 'portrait');

        $dompdf->render();






        $dompdf->stream(
                'relatório-de-alunos.pdf', array(
            "Attachment" => true
                )
        );
    }

    // function ativarAluno($ra) {
    //     $modelAluno = new ModelAluno();
    //     $modelAluno->ativarAluno($ra);
    // }



    function deleteAluno($id) {
        $sucesso = 0;

        $msg = '';
        $aluno = new Aluno();
        $model = new ModelAluno();
        $sucesso = $model->deleteAluno($id);
        $aluno->setRa($id);

        $this->sessao->add("removeAluno", "yes");

        $this->sessao->add("msgModalidade", $msg);

        if ($sucesso > 0) {
            $model->buscarAlunoFoto($aluno);
            $img = $aluno->getImagem();
            //echo $img;


            $FilePath = __DIR__ . '/../../public_html/img/';
            $FilePath2 = __DIR__ . '/../../public_html/img/miniaturas/';

            //chdir($FilePath);
            //chown($img, 465);
            //chdir($FilePath2);
            //chown($img, 465);
            //$do = unlink($img);
            //if ($do == "1") {
            //    echo "A foto foi deletada com sucesso!.";
            //} else {
            //    echo "Ocorreu um erro ao tentar excluir a imagem!";
            //}
            $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Modalidade Removida com Sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';



            $this->sessao->add("removeAluno", "yes");

            $this->sessao->add("msgAluno", $msg);


            $this->sessao->add("msgAluno", $msg);


            $this->redirectToAlunos();
        } else {

            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! Exclusão não efetuada!</strong> A modalidade foi preservada por algum motivo !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

            $this->sessao->add("msgAluno", $msg);
            $this->redirectToAlunos();
        }
    }

    /* function removerAluno() {

      $this->redirectSemPermissaoTec();

      $ra = $this->request->get('ra');
      $img = $this->request->get('foto');

      //echo $ra;
      $modelAluno = new ModelAluno();
      $sucesso = $modelAluno->removeAluno($ra);
      echo $sucesso;

      $msg = '';

      if ($sucesso > 0) {

      $FilePath = __DIR__ . '/../../public_html/img/';
      $FilePath2 = __DIR__ . '/../../public_html/img/miniaturas/';

      chdir($FilePath);
      chown($img, 465);
      chdir($FilePath2);
      chown($img, 465);
      $do = unlink($img);

      if ($do == "1") {
      echo "A foto foi deletada com sucesso!.";
      } else {
      echo "Ocorreu um erro ao tentar excluir a imagem!";
      }

      echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">

      <strong>Atenção! </strong> Usuário Removido com Sucesso!!!
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>';

      $this->sessao->add("buscaEfetivadaRemoveAluno", "");
      $this->sessao->add("removeAluno", "");
      $this->sessao->add("msgAluno", $msg);
      $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">

      <strong>SUCESSO! </strong> Aluno com o RA: ' . $ra . ' excluído !!!
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>';

      $this->sessao->add("msgAluno", $msg);
      } else {

      $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">

      <strong>Atenção! Exclusão não efetuada!</strong> O Aluno do RA: ' . $ra . ' foi preservado por algum motivo !!!
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>';

      $this->sessao->add("msgAluno", $msg);
      }
      } */
}
