<?php

namespace JIF\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use JIF\Util\Sessao;
use JIF\Models\ModelUsuario;
use JIF\Models\ModelAluno;
use JIF\Models\ModelModalidade;
use JIF\Entity\Time;
use JIF\Models\ModelTime;
use \stdClass;
use Dompdf\Dompdf;
use Dompdf\Options;

class ControllerTime {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    function redirectSemPermissao() {
        if (!$this->sessao->existe('administrador')) {
            //$redirect = new RedirectResponse('/');
            // $redirect->send();

            if (!$this->sessao->existe('tecnico')) {
                ?>  <script language="javascript">
                    window.location.href = "/";
                </script>

                <?php

            }
        }
    }

    function telaTime() {
        $this->redirectSemPermissao();
        $modelUser = new ModelUsuario();
        $modelAtleta = new ModelAluno();
        $modelModalidade = new ModelModalidade();
        $campus = $this->sessao->get("campusLogado");

        $modalidades = $modelModalidade->buscarModalidades();
        $atletas = $modelAtleta->buscarAlunosCampus($campus);
        //$usuario = $modelUser->buscarTecnico($this->sessao->get('cpf'));

        return $this->response->setContent($this->twig->render("/formarTime.twig", ["atletas" => $atletas, "modalidades" => $modalidades]));
    }

    function telaGerarCracha() {

        $this->redirectSemPermissao();
        $model = new ModelTime();
        $campus = $this->sessao->get("campusLogado");

        $times = $model->buscarTimeCampus($campus);

        //$usuario = $modelUser->buscarTecnico($this->sessao->get('cpf'));

        return $this->response->setContent($this->twig->render("/geradorCrachasPdf.twig", ["times" => $times]));
    }

    function telaListarTimes() {
        $this->redirectSemPermissao();

        $modelModalidade = new ModelModalidade();
        $modelTime = new ModelTime();

        $cpf = $this->sessao->get("cpfLogado");
        $campus =  $this->sessao->get("campusLogado");

        $modalidades = $modelModalidade->buscarModalidades();
        
        $times = $modelTime->buscarTimeCampus($campus);
        //$times = $modelTime->buscarTimeTecnico($cpf);

        //$buscaQuantidade = $modelTime->buscaQuantidade($cpf);
        //$usuario = $modelUser->buscarTecnico($this->sessao->get('cpf'));

        return $this->response->setContent($this->twig->render("/editaTime.twig", ["times" => $times, "modalidades" => $modalidades]));
    }

    function telaMostrarTimeEdit() {

        $this->redirectSemPermissao();

        $campus = $this->sessao->get("campusLogado");

        $idTime = $this->request->get('idTime');
        //$this->sessao->add("idTimeButton","idTimeButton");

        $modelModalidade = new ModelModalidade();
        $modelTime = new ModelTime();

        $cpf = $this->sessao->get("cpfLogado");
        //$campus =  $this->sessao->get("campusLogado");

        $modalidades = $modelModalidade->buscarModalidades();



        $times = $modelTime->buscarTimeJoinAluno($idTime);


        //$time2 = $modelTime->selectTime($idTime);
        //$time = $modelTime->selectTime($idTime);

        $timeDados = $modelTime->selectTime($idTime);

        if (!empty($times)) {
            
        } else {
            
        }


        $atletas = $modelTime->buscarTimeJoinAlunoInative($idTime, $campus);

        if (!empty($atletas)) {
            
        } else {
            // echo 'O array está vazio';
        }
        //print_r($idTime);
        // print_r($times);
        //$usuario = $modelUser->buscarTecnico($this->sessao->get('cpf'));

        return $this->response->setContent($this->twig->render("/editaTimeMostrar.twig", ["idTime" => $idTime, "timeDados" => $timeDados, "times" => $times, "atletas" => $atletas, "modalidades" => $modalidades]));
    }

    function removerdoTime() {
        $this->redirectSemPermissao();
        $qtd2 = 0;
        $ide = $this->request->get('check_id');
        $ra = $this->request->get('ra');
        //print_r($ide);

        if (!empty($ide)) {
            foreach ($ide as $key => $n) {
                $time = new Time();
                $time->setAluno($n);
                //print_r($ide[$key]);

                $modelTime = new ModelTime();
                $result = $modelTime->removerAlunoTime($time);
                if ($result == 1) {
                    //echo $qtd2 . 'removido';
                }

                $qtd2++;
            }
            $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        
                        <strong>Sucesso!</strong> Atleta(s) removido(s)!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
            echo $msg;
            ?>  <script language="javascript">
                window.location.href = "#";
                window.location.reload();
                //window.location.href
            </script>

            <?php

        } else {
            if (empty($ide)) {
                $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                        
                        <strong>Atenção!</strong> Por favor Marque as caixinhas dos Atletas que deseja remover!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
                echo $msg;
                ?>  <script language="javascript">
                    window.location.href = "#";
                </script>

                <?php

            }
        }
    }

    function salvaTime() {
        $this->redirectSemPermissao();
        $nome = $this->request->get('nome');
        $modalidade = $this->request->get('modalidade');
        $id = $this->request->get("idTime");
        //echo 'iDE'.$id;
        //print_r($modalidade);

        if (!empty($nome) && !empty($modalidade)) {

            $time = new Time();
            $time->setNome($nome);
            $time->setModalidade($modalidade);

            $modelTime = new ModelTime();
            $result = $modelTime->salvaTime($time, $id);
            if ($result == 1) {
                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        
                        <strong>Sucesso!</strong>Dados do time atualizados com sucesso!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
                echo $msg;
                ?>  <script language="javascript">
                    setTimeout(function () {
                        window.location.reload();
                    }, 2500);
                </script>
                <?php

            } else {
                $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        
                        <strong>Atenção!</strong> Time não atualizado!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
                echo $msg;
                ?>  <script language="javascript">

                    window.location.href('#');

                </script>
                <?php

            }
        } else {
            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                        
                        <strong>Atenção!</strong> Selecione os campos!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
            echo $msg;
            ?>  <script language="javascript">

                window.location.href('#');

            </script>
            <?php

        }
    }

    function deletaTime() {

        $this->redirectSemPermissao();
        $id = $this->request->get('idTime');
        //print_r($id);

        if (!empty($id)) {

            $modelTime = new ModelTime();
            $result = $modelTime->deletaTime($id);
            if ($result == 1) {
                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        
                        <strong>Sucesso!</strong> Time removido com sucesso!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';

                $this->sessao->add("msgTime", $msg);

                $this->telaListarTimes();
            } else {

                $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        
                        <strong>Falha!</strong> Time não excluído!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';

                $this->sessao->add("msgTime", $msg);

                $this->telaListarTimes();
            }
        }
    }

    function addAlunoTime() {
        $this->redirectSemPermissao();
        $qtd = 0;
        $modalidade = $this->request->get('modalidade');
        $nomeTime = $this->request->get('nome');
        //$quantidadeTotal = $this->request->get('quantidade');
        //$nomeTime = $this->request->get('nomeTimeClone');
        $ra = $this->request->get('ra');
        $cpf = $this->sessao->get('cpfLogado');
        $ide = $this->request->get('check_id');
        $idTime = $this->request->get('idTime');
        //echo $idTime;
        //echo '</br>nome time'.$nomeTime ;
        //echo $quantidadeTotal;
        //print_r($ide);

        if (empty($ide)) {
            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                        
                        <strong>Atenção!</strong> Por favor Marque as caixinhas dos Atletas que quer adicionar!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
            echo $msg;
            ?>  <script language="javascript">
                window.location.href = "#";
            </script>

            <?php

        } elseif (!empty($ra) && !empty($cpf)) {

            foreach ($ide as &$value) {
                $qtd = $qtd + 1;
            }

            $qtd2 = 0;

            $time = new Time();
            $time->setNome($nomeTime);
            $time->setModalidade($modalidade);
            $time->setTecnico($cpf);

            $modelTime = new ModelTime();
            $qtdModel = $modelTime->buscaModalidade($time);


            //$qtdModel = $time->getQtdAlunos($modalidade );
            //$verificaQtdTimehasAluno = $modelTime->verificaLimite($id);
            //print_r($qtdModel);


            $qtdDeAletasNoTime = $modelTime->verificaLimite($idTime);

            //echo '<br>gente no time' . $qtdDeAletasNoTime;

            $diferenca = $qtdModel - $qtdDeAletasNoTime;

            //echo $qtdModel;
            //echo "Diferença = ".$diferenca."  Quantidade da modalidade ".$qtdModel."- $qtdDeAletasNoTime".$qtdDeAletasNoTime;
            //echo '<br>gente no time' . $qtdDeAletasNoTime;
            //echo '<br>diferença' . $diferenca;
            //echo '<br>qtd' .$qtd;
            //echo $qtd;

            if ($qtdDeAletasNoTime == $qtdModel) {
                $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
		
		                        <strong>Atenção!</strong> Time Cheio!! Por favor selecione até no máximo 
		                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                        <span aria-hidden="true">&times;</span>
		                        </button>
		                        </div>';
                echo $msg;
            } else {



                foreach ($ide as $key => $n) {
                    $time = new Time();
                    $time->setAluno($n);

                    $modelTime = new ModelTime();
                    //echo '</br>Quantidade Total: ' . $qtd;
                    //echo '</br>Diferença: ' . $diferenca;

                    $result2 = $modelTime->cadastrarTimeHasAluno($time, $idTime);


                    if ($result2) {
                        //echo $qtd2 . 'atualizado';
                    }

                    $qtd2++;
                }


                if ($qtd2 <= $diferenca) {
                    $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
						                        
						                        <strong>Êxito!</strong> Atleta(s) Inseridos no time com Suceso!!
						                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						                            <span aria-hidden="true">&times;</span>
						                        </button>
						                         </div>';
                    echo $msg;
                    ?>  <script language="javascript">
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    </script>
                    <?php

                }
            }
        }
    }

    function criarTime() {
        $this->redirectSemPermissao();
        $qtd = 0;
        $modalidade = $this->request->get('modalidade');
        $nomeTime = $this->request->get('nome');


        //$mas = 'Masculino';
        //$fem = 'Feminino';
        //$result = str_replace($mas, "", $nome);
        //$nomeTime = str_replace($fem, ".", $result);
        //$quantidadeTotal = $this->request->get('quantidade');
        //$nomeTime = $this->request->get('nomeTimeClone');
        $ra = $this->request->get('ra');
        $campus = $this->sessao->get('campusLogado');
        $cpf = $this->sessao->get('cpfLogado');
        $ide = $this->request->get('check_id');

        //echo '</br>modalidade'.$modalidade;
        //echo '</br>nome time'.$nomeTime ;
        //echo $quantidadeTotal;

        if (empty($ide)) {
            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                        
                        <strong>Atenção!</strong> Por favor Marque as caixinhas dos Atletas!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
            echo $msg;
            ?>  <script language="javascript">
                window.location.href = "#";
            </script>

            <?php

        } elseif (!empty($ra) && !empty($cpf)) {

            foreach ($ide as &$value) {
                $qtd = $qtd + 1;
            }

            $qtd2 = 0;

            $time = new Time();
            $time->setNome($nomeTime);
            $time->setModalidade($modalidade);
            $time->setTecnico($cpf);

            $modelTime = new ModelTime();
            $qtdModel = $modelTime->buscaModalidade($time);
            //$verificaQtdTimehasAluno = $modelTime->verificaLimite($id);

            $modalid = $qtdModel;

            if ($qtd > $qtdModel) {
                $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        
                        <strong>Falha!</strong> Por favor selecione até ' . $qtdModel . ' atletas!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
                echo $msg;
                ?>  <script language="javascript">
                    window.location.href = "#";
                </script>

                <?php

            } else {

                //$existeNomeTime = $modelTime->verificaTime($nomeTime);

                $lastInsertedID = $modelTime->cadastrarTime($time, $campus);


                //print_r("TIME:::::::".$lastInsertedID);
                //echo '<br/>';
                //print_r($ide);

                if ($lastInsertedID != FALSE) {

                    foreach ($ide as $key => $n) {
                        $time = new Time();
                       // echo($n . '</br>');
                        $time->setAluno($n);

                        $modelTime = new ModelTime();
                        $result2 = $modelTime->cadastrarTimeHasAluno($time, $lastInsertedID);

                        if ($result2) {
                            // echo $qtd2 . 'inserido';
                        }

                        $qtd2++;
                    }
                }

                if ($qtd2 == $qtd) {
                    $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        
                        <strong>Êxito!</strong> Time Criado com Suceso!! 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
                    echo $msg;
                    ?>  <script language="javascript">
                        window.location.href = "#";
                    </script>

                    <?php

                }
            }
        }
    }

    function relatorioEspecial() {
        //$this->redirectSemPermissaoTec();

        $this->redirectSemPermissao();

        $campus = $this->sessao->get("campusLogado");

        //$tipo = $this->request->get('tipo');

        $time = $this->request->get('time');

        $tipo = $time;

        $timeLabel = 'Time(s)';

        $sucesso = 0;
        $html = '';

        $model = new ModelTime();


        if (!empty($time)) {
            $cracha = $model->buscarCrachaTimeAtual($time);
            
        } else {
            if ($this->sessao->get('administrador') != "") {

                $cracha = $model->buscarCrachaTodosTimes();
                
            } else if ($this->sessao->get('tecnico') != "") {

               
                $cracha = $model->buscarCrachaCampusAtual($this->sessao->get('campusLogado'));
            }
        }

        if ($cracha != null) {

            $pdfOptions = new Options();
            $pdfOptions->setIsRemoteEnabled(true);

            $pdfOptions->set('defaultFont', 'Arial');

            //$pdfOptions->set('tempDir', '/public_html/directory/pdf-export/tmp');
            //
                //$dompdf = new Dompdf($options);
            //array('enable_remote' => true),
            $dompdf = new Dompdf($pdfOptions);
            $timeLabel = 'Time(s)';

            $i = 1;


            $html .= '<link rel="stylesheet" type="text/css" href="https://jiftm.com/css/bootstrap.css"><link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">';
            /* if ($i % 2 == 0) {


              } else {

              } */


            $html .= '<div style ="font-family: Roboto, sans-serif;"  >'
                    . '<table border="0" styler=" width: 50%; overflow: hidden;">';

            //$html .= '<tr>'; 
            $html .= '<tbody>';
            foreach ($cracha as $value) {

                $nomeDosTimes = ' - ';

                $foto = $value->foto;

                $model = new ModelTime();


                $result = $model->listarTimesAluno($value->ra);

                foreach ($result as $s) {

                    $nomeDosTimes .= '  ' . $s->nomeTime . ' - ';
                }

                $nomeDosTimes = str_replace('Masculino', '', $nomeDosTimes);
                $nomeDosTimes = str_replace('Feminino', '', $nomeDosTimes);
                // A few settings
                //$image = 'https://www.jiftm.com/img/' . $value->foto;
                //$image = str_replace(" ", "%20", $image);
// Read image path, convert to base64 encoding
                //$imageData = base64_encode(file_get_contents($image));


                $f;
                $l;
                if (headers_sent($f, $l)) {
                    echo $f, '<br/>', $l, '<br/>';
                    die('now detect line');
                }

// Format the image SRC:  data:{mime};base64,{data};
                //$src = 'data:' . mime_content_type($image) . ';base64,' . $imageData;
// Echo out a sample image
//<h6 class="m-0 font-weight-bold text-success"><b>' . $timeLabel . '   :  </b></h6>
// <h6 class="m-0 font-weight-bold text-success"><b>Campus  : </b></h6>

                $float = ($i / 2);
                $html .= (is_float($float) ? '</tr><tr width="640px;">' : '' );

                $html .= '
                        <td width="320px;">
                        <div style="max-width: 320px ; text-align:center; " class="col-xs-6">
                         <center><h4 class="m-4 font-weight-bold text-dark" style="font-size:30px;"><b><img src="https://www.jiftm.com/icon/favicon.ico.ico" class="img-responsive center-block mt-2 mb-2" margin="0 auto" ><a class="text-primary"> JIFTM 2019</a></b></h4></center>
                         
                                
                                <center><img src="https://www.jiftm.com/img/miniaturas/' . $value->foto . '" class="img-responsive center-block mt-2 mb-2" margin="0 auto" width="135px"><br></center><br>
                                
                                
                                <h6 class="m-0 font-weight-bold text-success" ><b><h6 style="font-size:35px;" class="m-0  text-dark">' . $value->nome . '<h6></b></h6><br>
                                
                                 <h6 class="m-1" style="font-size:28px;">' . $nomeDosTimes . '</h6><br>
                                
                               
                                <h6 class="m-1 text-success" style="font-size:25px;">' . 'Campus ' . $value->campus . '</h6></div></td><br>';
                $html .= '</div>';


//$html .= ($i % 2 == 0 ? '</div>' : '' );

                $html .= '</td><br>';


                //$html .= ($i % 2 == 0 ? '</tr>' : '' );


                $i = $i + 1;
            }

            //$dompdf->load_html($this->twig->render('templateCracha.twig'));
            //$this->response->setContent(("/editaTime.twig", ["times" => $times, "modalidades" => $modalidades]));

            $html .= "</tbody>";
            $dompdf->loadHtml($html);


            $dompdf->setPaper('A4', 'portrait');
            header("Content-type:application/pdf");
            $dompdf->render();


            $dompdf->stream(
                    'crachas.pdf', array(
                "Attachment" => true
                    )
            );

            $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Crachás gerados!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

            $sucesso = 1;
        } else {
            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Crachás não foram gerados!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
        }
    }

}
