<?php

namespace JIF\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use JIF\Util\Sessao;
use JIF\Models\ModelUsuario;
use JIF\Models\ModelAluno;
use JIF\Models\ModelModalidade;
use JIF\Entity\Jogo;
use JIF\Models\ModelJogo;
use \stdClass;

class ControllerJogo {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    function redirectSemPermissao() {
        if (!$this->sessao->existe('administrador')) {
            //$redirect = new RedirectResponse('/');
            // $redirect->send();

            if (!$this->sessao->existe('tecnico')) {
                ?>  <script language="javascript">
                                    window.location.href = "/";
                </script>

                <?php

            }
        }
    }

    function telaListarJogos() {

        $campus = $this->sessao->get("campusLogado");

        $modelJogo = new ModelJogo();

        $jogosAtivos = $modelJogo->listarTime(1);

        $jogosInativos = $modelJogo->listarTime(0);

        $jogosFinalizados = $modelJogo->listarTime(2);

        $placares = $modelJogo->listarPlacares();

        $times = $modelJogo->listarTimes();

// $atletas = $modelAtleta->buscarAlunosCampus($campus);
//$usuario = $modelUser->buscarTecnico($this->sessao->get('cpf'));

        return $this->response->setContent($this->twig->render("/listarJogos.twig", ["jogosInativos" => $jogosInativos, "jogosAtivos" => $jogosAtivos, "jogosFinalizados" => $jogosFinalizados, "times" => $times, "placares" => $placares]));
    }

    function telaNovoJogo() {

        //$campus = $this->sessao->get("campusLogado");
        $modelJogo = new ModelJogo();
        //$jogosAtivos = $modelJogo->listarTime(1);
        //$jogosInativos = $modelJogo->listarTime(0);
        // $jogosFinalizados = $modelJogo->listarTime(2);
        //$placares = $modelJogo->listarPlacares();
        $times = $modelJogo->listarTimes();
// $atletas = $modelAtleta->buscarAlunosCampus($campus);
//$usuario = $modelUser->buscarTecnico($this->sessao->get('cpf'));
        $this->redirectSemPermissao();

        return $this->response->setContent($this->twig->render("/jogoCadastro.twig", [ "times" => $times]));
    }

    function atualizaRealUser($idJogo) {
// recebe os dados
//echo $idJogo;
//$this->telaJogoView($idJogo);
        $model = new ModelJogo();

        $jogo = $model->buscaDadosJogo($idJogo);

        foreach ($jogo as &$j) {
            $status = $j->status;
            $idTime1 = $j->Time_idTime;
            $idTime2 = $j->Time_idTime1;

            break;
        }

        $time1 = $model->buscarTime($idTime1);
        $time2 = $model->buscarTime($idTime2);

        $msg = $model->buscarJogo($idJogo);

        foreach ($msg as &$j) {
            $mensagemBanco = $j->mensagem;
            $local = $j->local;

            break;
        }

        foreach ($time1 as &$j) {
            $nomeTime1 = $j->nomeTime . " " . $j->campus;


            break;
        }

        foreach ($time2 as &$j) {
            $nomeTime2 = $j->nomeTime . " " . $j->campus;


            break;
        }


        $placartime1 = $model->verPlacar($idTime1, $idJogo);
        $placartime2 = $model->verPlacar($idTime2, $idJogo);

        $nomeTime1 = $model->verNomeTime($idTime1);
        $nomeTime2 = $model->verNomeTime($idTime2);


        if (($placartime1 > 99) || ($placartime1 < 0)) {// pre carrega o score com o placar do time1
            $srcImg1T1 = 'none.png';
            $srcImg2T1 = 'none.png';
            $valor1 = ($placartime1 % 10);
            $valor2 = (floor($placartime1 / 10));
        } else {
            $valor1 = ($placartime1 % 10);
            $valor2 = (floor($placartime1 / 10));
            $srcImg1T1 = ($placartime1 % 10);
            $srcImg2T1 = (floor($placartime1 / 10));

// echo '<br>DEU CERTO' . $valor2 . $valor1;

            $img1 = $valor2;
            $img2 = $valor1;

//console.log(valor1 + valor2);
        }

        if (($placartime2 > 99) || ($placartime2 < 0)) {// pre carrega o score com o placar do time1
            $srcImg1T1 = 'none.png';
            $srcImg2T1 = 'none.png';
            $valor3 = ($placartime2 % 10);
            $valor4 = (floor($placartime2 / 10));
        } else {
            $valor3 = ($placartime2 % 10);
            $valor4 = (floor($placartime2 / 10));
            $srcImg1T1 = ($placartime2 % 10);
            $srcImg2T1 = (floor($placartime2 / 10));

//echo '<br>DEU CERTO' . $valor4 . $valor3;

            $img3 = $valor4;
            $img4 = $valor3;



//console.log(valor1 + valor2);
        }

        if ($this->sessao->get("logado") == "") {

            /* echo json_encode(
              array(
              "placartime1" => $placartime1,
              "placartime2" => $placartime2,
              "nomeTime1" => $nomeTime1,
              "nomeTime2" => $nomeTime2
              )
              ); */

//document.getElementById('mytext').value = 'My value'

            echo "
 <table style='border:1; width:1%; class='w-50' '>
 <tr >
<td ><div class='col-xs-6 col-md-6 inline-block w-75 ' style=' display:inline;'>

   
    <h2 width='20px' style='color:#0062cc;'>" . $nomeTime1 . "</h2>
   
    <img src='/img/" . $img1 . ".png' height='120' alt='' class='img-responsive' id='img2-time1'/>

    <img src='/img/" . $img2 . ".png' height='120' alt='' class='img-responsive' id='img1-time1'/>

</div></td>
 

<td><h5 class='row col-auto align-items-center'  style='margin: 0;'>
    <b style='margin: 0;' class=text-danger >VS</b>
</h5></td>

<td><div class='col-xs-6 col-md-6 inline-block'  style=' display:inline;' >

    <h2 >" . $nomeTime2 . "</h2>
  
    
    <img src='/img/" . $img3 . ".png' height='120' alt='' class='img-responsive' id='img2-time2'/>
    <img src='/img/" . $img4 . ".png' height='120' alt='' class='img-responsive' id='img1-time2'/>

</div></td>
</tr>

</table>


           ";

            echo '<div class="card bg-light mb-5 my-5 mr-4 ">
  <div class="card-header">Atualizações Recentes</div>
  <div class="card-body">
    <h5 class="card-title">Comentários em Tempo Real: </h5>
    <p class="card-text">
    ' . $mensagemBanco . '
    </p>
</div>
</div>';
        } else {

            $model->atualizaAdminPlacarJogo($idJogo, $idTime1, $idTime2, $scoreTime1, $scoreTime2);

            $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">

                <strong>Sucesso!</strong> placar atualizado!!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>';
            echo $msg;
            ?>  <script language="javascript">
                            window.location.href = "#";
                            window.location.reload();
                            //window.location.href
            </script>

            <?php

        }

        /* return new JsonResponse([
          'html' => $this->telaJogoView($idJogo)
          ]
          ); */
// imprime na tela em formato json
        /* echo json_encode(
          array(
          "email" => $email,
          "nome" => $nome
          )
          ); */
    }

    function atualizaSumula() {
// recebe os dados

        $idJogo = $this->request->get("idJogo");

        $file = $this->request->files->get('file');

        if ($file == null) {
            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atencão! </strong> Selecione arquivo de documento para Súmula!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
        } else {
            $target_file = __DIR__ . '/../../public_html/pdf/' . $idJogo . $file->getClientOriginalName();

            $nomeArquivo = $idJogo . $file->getClientOriginalName();

            $temp_file = $_FILES["file"]["tmp_name"];
            $name = $_FILES["file"]["name"];

            $type = $_FILES["file"]["type"];
            $sizes = $_FILES["file"]["size"];
            $errorMsg = $_FILES["file"]["error"];
            $explode = explode(".", $name);
            $extension = end($explode);


            $idTime1 = $this->request->get("idTime1Atual");
            $idTime2 = $this->request->get("idTime2Atual");

            $scoreTime1 = $this->request->get("scoreTime1Atual");
            $scoreTime2 = $this->request->get("scoreTime2Atual");


            $model = new ModelJogo();


            setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            date_default_timezone_set('America/Sao_Paulo');
            $time = strftime(' %H :%M ', strtotime('now'));

            $msg = $model->buscarJogo($idJogo);




            foreach ($msg as &$j) {
                $mensagemBanco = $j->mensagem;

                break;
            }


            if ($sizes > 10242880) {
                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Selecione arquivo menor que 10MB!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';


                exit();
            } else if (!preg_match("/\.(pdf|doc|docx|dotx|dot|rtf|txt|htm|docm|dotm|xml|mht|dic|dic|rtf)$/i", $name)) {

                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Selecione arquivo no formado de documento ou tente novamente!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';


                exit();
            } else if ($errorMsg == 1) {
                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong>Ocorreu algum erro ao processar o arquivo!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';


                exit();
            } elseif (move_uploaded_file($temp_file, $target_file)) {


                $model = new ModelJogo();
                $model->updateSumula($idJogo, $nomeArquivo);


                echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong>Sumula enviada!!! Para alterar envie outra!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            }
        }

        //setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        //date_default_timezone_set('America/Sao_Paulo');
        //echo strftime('%A, %d de %B de %Y', strtotime('today'));
//document.getElementById('mytext').value = 'My value'


        $model->atualizaAdminPlacarJogo($idJogo, $idTime1, $idTime2, $scoreTime1, $scoreTime2);
    }

    function atualizaTempoReal() {
// recebe os dados

        $idJogo = $this->request->get("idJogo");

        $mensagemLocal = $this->request->get("mensagem");



        $idTime1 = $this->request->get("idTime1Atual");
        $idTime2 = $this->request->get("idTime2Atual");

        $scoreTime1 = $this->request->get("scoreTime1Atual");
        $scoreTime2 = $this->request->get("scoreTime2Atual");

//$this->telaJogoView($idJogo);
        $model = new ModelJogo();

        $placartime1 = $model->verPlacar($idTime1, $idJogo);
        $placartime2 = $model->verPlacar($idTime2, $idJogo);

        $nomeTime1 = $model->verNomeTime($idTime1);
        $nomeTime2 = $model->verNomeTime($idTime2);

        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
        $time = strftime(' %H :%M ', strtotime('now'));

        $msg = $model->buscarJogo($idJogo);

        $msz = '- ' . $mensagemLocal . '<br>';


        foreach ($msg as &$j) {
            $mensagemBanco = $j->mensagem;

            break;
        }

        $mensagem = $time . $msz . $mensagemBanco;


        if ($mensagemLocal != '') {
            $model->updateMensagem($idJogo, $mensagem);
        }


        //setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        //date_default_timezone_set('America/Sao_Paulo');
        //echo strftime('%A, %d de %B de %Y', strtotime('today'));
//document.getElementById('mytext').value = 'My value'


        $model->atualizaAdminPlacarJogo($idJogo, $idTime1, $idTime2, $scoreTime1, $scoreTime2);
        $msg = '<div class = "alert alert-success alert-dismissible fade show" role = "alert">

<strong>Sucesso!</strong> dados atualizados!!
<button type = "button" class = "close" data-dismiss = "alert" aria-label = "Close">
<span aria-hidden = "true">&times;
</span>
</button>
</div>';
        echo $msg;
        ?>  <script language="javascript">
                    window.location.href = "#";
                    window.location.reload();
                    //window.location.href
        </script>

        <?php

        /* return new JsonResponse([
          'html' => $this->telaJogoView($idJogo)
          ]
          ); */
        // imprime na tela em formato json
        /* echo json_encode(
          array(
          "email" => $email,
          "nome" => $nome
          )
          ); */
    }

    function telaJogoView($idJogo) {

        $modelJogo = new ModelJogo();

        $jogo = $modelJogo->buscarJogo($idJogo);
        $placares = $modelJogo->listarPlacares();

        //echo '<br>';
        //echo '<br>';
        //echo '<br>';
        //echo '<br>';
        //print_r($jogo);
        //print_r('teste');

        $this->sessao->add("idJogo", $idJogo);

        $times = $modelJogo->listarTimes();

        if (empty($times) || empty($jogo)) {
            $this->telaListarJogos();
        } else {

            return $this->response->setContent($this->twig->render("/jogoView.twig", ["jogo" => $jogo, "times" => $times, "placares" => $placares]));
        }
    }

    function removerdoJogo() {
        $this->redirectSemPermissao();
        $qtd2 = 0;
        $ide = $this->request->get('check_id');
        $ra = $this->request->get('ra');
        //print_r($ide);

        if (!empty($ide)) {
            foreach ($ide as $key => $n) {
                $time = new Jogo();
                $time->setAluno($ra[$key]);
                //print_r($ide[$key]);

                $modelJogo = new ModelJogo();
                $result = $modelJogo->removerAlunoJogo($time);
                if ($result == 1) {
                    //echo $qtd2 . 'removido';
                }

                $qtd2++;
            }
            $msg = '<div class = "alert alert-success alert-dismissible fade show" role = "alert">

<strong>Sucesso!</strong> Atleta(s) removido(s)!!
<button type = "button" class = "close" data-dismiss = "alert" aria-label = "Close">
<span aria-hidden = "true">&times;
</span>
</button>
</div>';
            echo $msg;
            ?>  <script language="javascript">
                            window.location.href = "#";
                            window.location.reload();
                            //window.location.href
            </script>

            <?php

        } else {
            if (empty($ide)) {
                $msg = '<div class = "alert alert-warning alert-dismissible fade show" role = "alert">

<strong>Atenção!</strong> Por favor Marque as caixinhas dos Atletas que deseja remover!!
<button type = "button" class = "close" data-dismiss = "alert" aria-label = "Close">
<span aria-hidden = "true">&times;
</span>
</button>
</div>';
                echo $msg;
                ?>  <script language="javascript">
                                    window.location.href = "#";
                </script>

                <?php

            }
        }
    }

    /* function salvaJogo() {
      $this->redirectSemPermissao();
      $nome = $this->request->get('nome');
      $modalidade = $this->request->get('modalidade');
      $id = $this->request->get("idJogo");
      //echo 'iDE'.$id;
      //print_r($modalidade);

      if (!empty($nome) && !empty($modalidade)) {

      $time = new Jogo();
      $time->setNome($nome);
      $time->setModalidade($modalidade);

      $modelJogo = new ModelJogo();
      $result = $modelJogo->salvaJogo($time, $id);
      if ($result == 1) {
      $msg = '<div class = "alert alert-success alert-dismissible fade show" role = "alert">

      <strong>Sucesso!</strong>Dados do time atualizados com sucesso!!
      <button type = "button" class = "close" data-dismiss = "alert" aria-label = "Close">
      <span aria-hidden = "true">&times;
      </span>
      </button>
      </div>';
      echo $msg;
      ?>  <script language="javascript">
      setJogoout(function () {
      window.location.reload();
      }, 2500);
      </script>
      <?php

      } else {
      $msg = '<div class = "alert alert-danger alert-dismissible fade show" role = "alert">

      <strong>Atenção!</strong> Jogo não atualizado!!
      <button type = "button" class = "close" data-dismiss = "alert" aria-label = "Close">
      <span aria-hidden = "true">&times;
      </span>
      </button>
      </div>';
      echo $msg;
      ?>  <script language="javascript">

      window.location.href('#');

      </script>
      <?php
      }
      } else {
      $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">

      <strong>Atenção!</strong> Selecione os campos!!
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>';
      echo $msg;
      ?>  <script language="javascript">

      window.location.href('#');

      </script>
      <?php
      }
      } */

    function atualizaStatus() {

        $this->redirectSemPermissao();
        $idJogo = $this->request->get('idJogo');
        $status = $this->request->get('status');

// echo $status;
//$id = $this->request->get('idJogo');
//print_r($id);

        if (!empty($idJogo)) {
            $modelJogo = new ModelJogo();
            $result = $modelJogo->updateStatus($idJogo, $status);

            if ($status == 2) {

                $modelJogo->updateEnd($idJogo, date('Y-m-d H:i:s'));
            }

            if (!$result) {
                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        
                        <strong>Sucesso!</strong> Status do Jogo atualizado!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';

                $this->sessao->add("msgJogoView", $msg);
                ?>  <script language="javascript">

                                    window.location.href = 'view';

                </script>

                <?php

            } else {

                $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        
                        <strong>Falha!</strong> não foi possivel realizar a ação desejada!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';

                $this->sessao->add("msgJogoView", $msg);
                ?>  <script language="javascript">
                                    site = window.location.hostname;
                                    window.history.back();
                </script>

                <?php

//$this->telaJogoView($idJogo);
            }
        }
    }

    function removeJogo($id) {

        $this->redirectSemPermissao();
//$id = $this->request->get('idJogo');
//print_r($id);

        if (!empty($id)) {

            $modelJogo = new ModelJogo();
            $result = $modelJogo->deletaJogo($id);
            if ($result == 1) {
                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        
                        <strong>Sucesso!</strong> Jogo removido com sucesso!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';

                $this->sessao->add("msgJogo", $msg);

                $this->telaListarJogos();
            } else {

                $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        
                        <strong>Falha!</strong> Jogo não excluído!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';

                $this->sessao->add("msgJogo", $msg);

                $this->telaListarJogos();
            }
        }
    }

    function telaChangeJogo($idJogo) {

        $modelJogo = new ModelJogo();

        $jogo = $modelJogo->buscarJogo($idJogo);

//echo '<br>';
//echo '<br>';
//echo '<br>';
//echo '<br>';
//print_r($jogo);
//print_r('teste');

        foreach ($jogo as &$j) {
            $status = $j->status;
            if ($status == 0) {
                $status = true;
            }

            break;
        }

        if ($status) {

            $times = $modelJogo->listarTimes();

// $atletas = $modelAtleta->buscarAlunosCampus($campus);
//$usuario = $modelUser->buscarTecnico($this->sessao->get('cpf'));

            return $this->response->setContent($this->twig->render("/jogoChange.twig", ["jogo" => $jogo, "times" => $times]));
        } else {
            $this->telaListarJogos();
        }
    }

    function criarJogo() {
        $this->redirectSemPermissao();
        $qtd = 0;
        $result1 = false;
        $result2 = false;
        $idJogo = $this->request->get('idJogo');

        $update = $this->request->get('update');

        $dataInicio = $this->request->get('dataInicio');
        $nomeJogo = $this->request->get('nome');
        $time1 = $this->request->get('time1');
        $time2 = $this->request->get('time2');


        $local = $this->request->get('local');
        $cpf = $this->sessao->get('cpfLogado');

        $campus = $this->sessao->get('campusLogado');


        $model = new ModelJogo();
        $modalidade1 = $model->buscaModalidade($time1);
        $modalidade2 = $model->buscaModalidade($time2);

        $campus1 = $model->buscaCampus($time1);
        $campus2 = $model->buscaCampus($time2);

//$mas = 'Masculino';
//$fem = 'Feminino';
//$result = str_replace($mas, "", $nome);
//$nomeJogo = str_replace($fem, ".", $result);
//$quantidadeTotal = $this->request->get('quantidade');
//$nomeJogo = $this->request->get('nomeJogoClone');
//echo '</br>modalidade'.$modalidade;
//echo '</br>nome time'.$nomeJogo ;
//echo $quantidadeTotal;

        if (empty($time1) || empty($time2)) {
            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                        
                        <strong>Atenção!</strong> Por favor selecione os times!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
            if ($update == 'yes') {
                $this->sessao->add("msgChangeJogo", $msg);

                $this->telaChangeJogo($idJogo);
            } else {
                echo $msg;
                ?>  <script language="javascript">
                                    window.location.href = "#";
                </script>

                <?php

            }
        } elseif ($time1 == $time2) {
            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Atenção!</strong> Por favor selecione times diferentes, times iguais!!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
            if ($update == 'yes') {
                $this->sessao->add("msgChangeJogo", $msg);

                $this->telaChangeJogo($idJogo);
            } else {
                echo $msg;
                ?>  <script language="javascript">
                                    window.location.href = "#";
                </script>

                <?php

            }
        } elseif ($campus1 == $campus2) {
            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Atenção!</strong> Por favor selecione modalidade de Campus diferente!!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';

            if ($update == 'yes') {
                $this->sessao->add("msgChangeJogo", $msg);

                $this->telaChangeJogo($idJogo);
            } else {
                echo $msg;
                ?>  <script language="javascript">
                                    window.location.href = "#";
                </script>

                <?php

            }
        } elseif ($modalidade1 != $modalidade2) {
            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Atenção!</strong> Por favor selecione o mesmo esporte!!!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
            if ($update == 'yes') {
                $this->sessao->add("msgChangeJogo", $msg);

                $this->telaChangeJogo($idJogo);
            } else {
                echo $msg;
                ?>  <script language="javascript">
                                    window.location.href = "#";
                </script>

                <?php

            }
        } else {

            $dataInicio .= ':00';


            $data = explode(" ", $dataInicio);
            list($date, $hora) = $data;
            $data_sem_barra = array_reverse(explode("/", $date));
            $data_sem_barra = implode("-", $data_sem_barra);
            $start_sem_barra = $data_sem_barra . " " . $hora;


            $qtd2 = 0;


//$dateStart = new \DateTime($start_sem_barra, new \DateTimeZone('America/Recife'));
//$date->format('Y-m-d H:i:s');
//Converter para String// $dateStart->format('Y-m-d H:i:s');
//$dataInicio = date("d-m-Y H:i", strtotime($dataInicio));
//$date = date("Y-m-d H:i", $dataInicio);



            $jogo = new Jogo();
            $jogo->setDataInicio($start_sem_barra);
            $jogo->setTime1ID($time1);
            $jogo->setTime2ID($time2);
            $jogo->setLocal($local);
            $jogo->setIdJogo($idJogo);
            $modelJogo = new ModelJogo();

//  echo '<br>TIME1'.$time1;
// echo '<br>TIME2'.$time2;
//$verificaQtdJogohasAluno = $modelJogo->verificaLimite($id);
//$existeNomeJogo = $modelJogo->verificaJogo($nomeJogo);

            if ($update == 'yes') {
                $verificaExistencia = $modelJogo->buscarJogo($idJogo);

                foreach ($verificaExistencia as &$j) {
                    $time1Banco = $j->Time_idTime;
                    $time2Banco = $j->Time_idTime1;
                    break;
                }



                if ($time1Banco == $time1) {
                    $time1Banco = -1;
                } elseif ($time2Banco == $time2) {
                    $time2Banco = -1;
                }

                $lastInsertedID = $modelJogo->updateJogo($jogo, $time1Banco, $time2Banco);

//echo 'teste';
            } else {
                $lastInsertedID = $modelJogo->cadastrarJogo($jogo);
            }


            if ($lastInsertedID != FALSE) {

                if ($update == 'yes') {
                    $sucessoPlacar = $modelJogo->deletaPlacar($idJogo);
                    $modelJogo->cadastrarPlacarTime($time1, $idJogo);
                    $modelJogo->cadastrarPlacarTime($time2, $idJogo);
                } else {


                    $result1 = $modelJogo->cadastrarPlacarTime($time1, $lastInsertedID);
                    $result2 = $modelJogo->cadastrarPlacarTime($time2, $lastInsertedID);
                }

//echo $result1;
//echo $result2;
            } else {
                $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        
                        <strong>Falha!</strong> Jogo não foi criado!! Possivelmente esse jogo já foi criado! 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
                if ($update == 'yes') {
                    $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        
                        <strong>Falha!</strong> Jogo não foi Alterado!! Possivelmente esse jogo já existe! 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
                    $this->sessao->add("msgChangeJogo", $msg);

                    $this->telaChangeJogo($idJogo);
                } else {
                    echo $msg;
                }
            }


            if ($result1 && $result2) {
                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        
                        <strong>Êxito!</strong> Jogo Criado com Suceso!! 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';
                echo $msg;
                ?>  <script language="javascript">
                                    window.location.reload();
                </script>

                <?php

            }

            if ($lastInsertedID && ($sucessoPlacar == 1)) {
                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        
                        <strong>Êxito!</strong> Jogo Alterado com Suceso!! 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                         </div>';

                $this->sessao->add("msgChangeJogo", $msg);

                $this->telaChangeJogo($idJogo);
            }
        }
    }

}
