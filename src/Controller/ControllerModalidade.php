<?php

namespace JIF\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use JIF\Util\Sessao;
use JIF\Entity\Modalidade;
use JIF\Models\ModelModalidade;

class ControllerModalidade {

    private $twig;
    private $request;
    private $response;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    function redirectSemPermissao() {
        if (!$this->sessao->existe('administrador')) {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function cadastrarModalidade($id) {
        
        $this->redirectSemPermissao();
        return $this->response->setContent($this->twig->render("/CadastrarModalidade.twig", ["msg" => $id]));
    }

    function showPesquisarModalidade() {
         $this->redirectSemPermissao();
        return $this->response->setContent($this->twig->render("/PesquisaModalidade.twig"));
    }

    function salvarModalidade() {
         $this->redirectSemPermissao();
        $esporte = $this->request->get('esporte');
        $qtdatletas = $this->request->get('qtdatletas');
        $genero = $this->request->get('genero');
        $tipo = $this->request->get('tipo');

	
        if (isset($esporte) && !empty($esporte) && isset($qtdatletas) && !empty($qtdatletas) && isset($genero) && !empty($genero) && !empty($tipo) && isset($tipo)) {

            $modalidade = new Modalidade(null, $esporte, $tipo, $qtdatletas, $genero);
            $model = new ModelModalidade();
            $model->inserirModalidade($modalidade);

            $redirect = new RedirectResponse('/painel/modalidade/cadastrar/1');
            $redirect->send();
        } else {
            $redirect = new RedirectResponse('/painel/modalidade/cadastrar/2');
            $redirect->send();
        }
        
    }

    function atualizarModalidade() {
         $this->redirectSemPermissao();
        $id = $this->request->get('codHidden');
        $esporte = $this->request->get('esporte');
        $qtdatletas = $this->request->get('qtdatletas');
        $genero = $this->request->get('genero');
        $tipo = $this->request->get('tipo');

        if (isset($esporte) && !empty($esporte) && isset($qtdatletas) && !empty($qtdatletas) && isset($genero) && !empty($genero) && !empty($tipo) && isset($tipo)) {

            $modalidade = new Modalidade($id, $esporte, $tipo, $qtdatletas, $genero);
            $model = new ModelModalidade();
            $model->atualizarModalidades($modalidade);
            $redirect = new RedirectResponse('/painel/modalidade/exibir/' . $id);
            $redirect->send();
        } else {
            $redirect = new RedirectResponse('/painel/modalidade/atualizar/' . $id);
            $redirect->send();
        }
    }

    function selectModalidade() {
         $this->redirectSemPermissao();
        $genero = $this->request->get('genero');
        $tipo = $this->request->get('tipo');

        $modalidade = new Modalidade(NULL, NULL, $tipo, NULL, $genero);
        $model = new ModelModalidade();

        if (isset($genero) && !empty($genero) && !empty($tipo) && isset($tipo)) {
            $dados = $model->buscarModalidadesGenotipo($modalidade);
            $this->response->setContent($this->twig->render("/PesquisaModalidade.twig", ["dados" => $dados]));
        } else if (isset($genero) && !empty($genero)) {
            $dados = $model->buscarModalidadesGenero($genero);
            $this->response->setContent($this->twig->render("/PesquisaModalidade.twig", ["dados" => $dados]));
        } else if (isset($tipo) && !empty($tipo)) {
            $dados = $model->buscarModalidadesTipo($tipo);
            $this->response->setContent($this->twig->render("/PesquisaModalidade.twig", ["dados" => $dados]));
        } else {
            $dados = $model->buscarModalidades();
            $this->response->setContent($this->twig->render("/PesquisaModalidade.twig", ["dados" => $dados]));
        }
    }

    function atualizarModalidadeById($id) {
         $this->redirectSemPermissao();
        $model = new ModelModalidade();
        $dados = $model->pesquisarModalidade($id);
        $this->response->setContent($this->twig->render("/AtualizarModalidade.twig", ["modalidade" => $dados]));
    }

function deleteModalidade($id) {
         $this->redirectSemPermissao();
$msg = '';
        $model = new ModelModalidade();
        $sucesso = $model->deleteModalidade($id);
        
         $this->sessao->add("removeModalidade", "yes");

            $this->sessao->add("msgModalidade", $msg);
            
        if ($sucesso > 0) {

            $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Modalidade Removida com Sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
 


            $this->sessao->add("removeModalidade", "yes");

            $this->sessao->add("msgModalidade", $msg);

           
            $this->sessao->add("msgModalidade", $msg);
            
         
$this->selectModalidade();


        } else {

            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! Exclusão não efetuada!</strong> A modalidade foi preservada por algum motivo !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

            $this->sessao->add("msgModalidade", $msg);
            $this->selectModalidade();
        }
        
    }

    /*function removerModalidadeById() {
         $this->redirectSemPermissao();
        $idModalidade = $this->request->get('idModalidade');
       
        $nomeModalidade = $this->request->get('nomeModalidade');

        $modelModalidade = new ModelModalidade();
        //$sucesso = $modelModalidade->removeModalidade($idModalidade);

        $msg = '';

        if ($sucesso > 0) {

            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Modalidade Removida com Sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';


            $this->sessao->add("removeModalidade", "yes");

            $this->sessao->add("msgModalidade", $msg);

            $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Modalidade Removida com Sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

            $this->sessao->add("msgModalidade", $msg);
            
            $this->selectModalidade();
            


        } else {

            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! Exclusão não efetuada!</strong> A modalidade foi preservada por algum motivo !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

            $this->sessao->add("msgModalidade", $msg);
        }
    }*/

    function exibirModalidade($id) {
         $this->redirectSemPermissao();
        $model = new ModelModalidade();
        $dados = $model->pesquisarModalidade($id);
        $this->response->setContent($this->twig->render("/exibirModalidade.twig", ["modalidade" => $dados]));
    }

}
