<?php

namespace JIF\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use JIF\Models\ModelPostagem;
use JIF\Entity\Postagem;
use JIF\Models\ModelUsuario;
use JIF\Util\Sessao;
use Dompdf\Dompdf;
use Dompdf\Options;

class ControllerPostagem {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    /* function telaListarPostagens() {

      $this->redirectSemPermissaoTec();

      $campus = $this->sessao->get("campusLogado");

      $modelPostagem = new ModelPostagem();

      $postagensAtivas = $modelPostagem->listarPostagem(1);

      $postagensInativas = $modelPostagem->listarPostagem(0);

      return $this->response->setContent($this->twig->render("/listarPostagens.twig", ["postagensInativas" => $postagensInativas, "postagensAtivas" => $postagensAtivas]));

      } */

    public function telaChangePostagem($id) {

        $this->redirect();
        $model = new ModelPostagem();

        $postagem = $model->buscarPostagem($id);


        // $this->sessao->add('updatePostagem', 'yes');

        if ($id == "" || $id == NUll) {
            ?>  <script language="javascript">
                window.location.href = "/";
            </script>

            <?php

        } else {

            $this->response->setContent($this->twig->render("/postagensChange.twig", ['postagem' => $postagem]));
        }
    }

    public function telaPostagem($id) {


        $model = new ModelPostagem();
        $modelUsuario = new ModelUsuario();
        $usuarios = $modelUsuario->listarPessoas();

        if ($id == "" || $id == NUll) {
            ?>  <script language="javascript">
                window.location.href = "/";
            </script>

            <?php

        } else {

            $postagem = $model->buscarPostagem($id);

            $this->response->setContent($this->twig->render("/postagem.twig", ['postagem' => $postagem, "usuarios" => $usuarios]));
        }
    }

    public function telaNovaPostagem() {

        $this->redirect();
        $model = new ModelPostagem();

        $this->response->setContent($this->twig->render("/postagensCadastro.twig"));
    }

    /* public function telaRemovePostagem() {
      $this->redirectSemPermissaoTec();
      $this->removeCRUDMenuSessao();

      $this->sessao->add('removePostagem', 'yes');

      // $this->response->setContent($this->twig->render("/buscaPostagem.twig"));
      } */

    public function telaBuscaPostagem() {
        $this->redirect();

        $model = new ModelPostagem();

        //var_dump(gd_info());

        if (empty($this->sessao->get('administrador'))) {
            $alunos = $model->pesquisaPostagems($this->sessao->get('campusLogado'));
        } else {
            $alunos = $model->relatorioPostagems();
        }
        return $this->response->setContent($this->twig->render("/buscaPostagem.twig", ["alunos" => $alunos]));
        //return $this->response->setContent($this->twig->render("/buscaPostagem.twig", ["alunos" => $atletas, "modalidades" => $modalidades]));
    }

    //public function busca() {
    //     $this->redirect();
    //     $this->response->setContent($this->twig->render("/busca.twig"));
    // }

    function redirect() {
        if (!$this->sessao->existe('logado')) {
            //  $redirect = new RedirectResponse('/');
            //$redirect->send();
            ?>  <script language="javascript">
                window.location.href = "/";
            </script>

            <?php

        }
    }

    function redirectSemPermissaoTec() {
        if (!$this->sessao->existe('administrador')) {
            //$redirect = new RedirectResponse('/');
            // $redirect->send();

            if (!$this->sessao->existe('tecnico')) {
                ?>  <script language="javascript">
                    window.location.href = "/";
                </script>

                <?php

            }
        }
    }

    function redirectToPostagems() {

        //$redirect = new RedirectResponse('/');
        // $redirect->send();
        ?>  <script language="javascript">
            window.location.href = "/";
        </script>

        <?php

    }

    function redirectSemPermissaoAdmin() {
        if (empty($this->sessao->existe('administrador'))) {
            //$redirect = new RedirectResponse('/');
            // $redirect->send();
            ?>  <script language="javascript">
                window.location.href = "/";
            </script>

            <?php

        }
    }

    function redirectSemPermissao() {
        if (empty($this->sessao->existe('logado'))) {
            //$redirect = new RedirectResponse('/login');
            //$redirect->send();
            ?>  <script language="javascript">
                window.location.href = "/";
            </script>

            <?php

        }
    }

    function viewPostagem() {
        $this->response->setContent($this->twig->render("/cadastroPostagem.twig"));
    }

    //function pesquisarPostagem() {
    //  $this->response->setContent($this->twig->render("/pequisaPostagem.twig"));
    //}

    function resize_image($file, $w, $h, $crop = FALSE) {
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width - ($width * abs($r - $w / $h)));
            } else {
                $height = ceil($height - ($height * abs($r - $w / $h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w / $h > $r) {
                $newwidth = $h * $r;
                $newheight = $h;
            } else {
                $newheight = $w / $r;
                $newwidth = $w;
            }
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }

    function atualizarPostagem() {

        $id = $this->request->get('id');
        $titulo = $this->request->get('titulo');
        //$texto = $_POST[ 'editTextPost' ];
        $texto = $this->request->get('editTextPost');
        $imagem = $this->request->files->get('imagem');


        $usuarioCPF = $this->sessao->get('cpfLogado');

        $data_local = new \DateTime('now', new \DateTimeZone('-0300'));
        $data_ultimo_login = $data_local->format('Y-m-d H:i:s');

        $post = new Postagem();
        $post->setDataHora($data_ultimo_login);
        $post->setUsuario_cpf($usuarioCPF);
        $post->setTexto($texto);
        $post->setTitulo($titulo);
        $post->setId($id);
        
        $model = new ModelPostagem(); 
        $postagem = $model->buscarPostagem($id);
        
        foreach ($postagem as &$j) {
            $status = $j->status;
            
            $post->setUsuario_cpf($j->Usuario_cpf);

            break;
        }
        

        $modelsPostagem = new ModelPostagem();

        //$topo = $this->request->get('topo');
        //$topo = 0;
        //$date1 = strtr($dataHora, '/', '-');
        //$date_time = date('Y-m-d h:i:s', strtotime($date1));


        if (($imagem == NULL) || (($imagem == ""))) {
            //$data_ultimo_login,$usuarioCPF,$texto,$titulo,$id

            if ($this->sessao->get("assistente") != "") {
                if ($modelsPostagem->atualizarPostagemSemFotoPendente($post)) {
                    
                    echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Postagem atualiza com sucesso, foto mantida!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    ?>  <script language="javascript">
                        setTimeout(function () {
                            window.location.href = "/";
                        }, 2000);
                    </script>

                    <?php

                } else {
                    echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível atualizar a postagem !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                }
            } else if (($this->sessao->get("logado") != "")) {
                if ($modelsPostagem->atualizarPostagemSemFoto($post)) {

                    echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Postagem atualiza com sucesso, foto mantida!!! (Pendente aprovação!)
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    ?>  <script language="javascript">
                        setTimeout(function () {
                            window.location.href = "/";
                        }, 2000);
                    </script>

                    <?php

                } else {
                    echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível atualizar a postagem !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                }
            }
        } else {

            $target_file = __DIR__ . '/../../public_html/img/' . $usuarioCPF . $imagem->getClientOriginalName();
            $miniatura_file = __DIR__ . '/../../public_html/img/miniaturas/' . $usuarioCPF . $imagem->getClientOriginalName();
            $temp_file = $_FILES["imagem"]["tmp_name"];
            $name = $_FILES["imagem"]["name"];
            $type = $_FILES["imagem"]["type"];
            $sizes = $_FILES["imagem"]["size"];
            $errorMsg = $_FILES["imagem"]["error"];
            $explode = explode(".", $name);
            $extension = end($explode);

            $data_local = new \DateTime('now', new \DateTimeZone('-0300'));
            $data_ultimo_login = $data_local->format('Y-m-d H:i:s');

           
            $post->setFoto($usuarioCPF . $imagem->getClientOriginalName());
            

            $modelsPostagem = new ModelPostagem();

            if ($sizes > 5242880) {
                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Selecione arquivo menor que 5MB!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

                exit();
            } else if (!preg_match("/\.(gif|jpg|png|jpeg)$/i", $name)) {

                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Selecione arquivo menor e com outro formato e tente novamente!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                exit();
            } else if ($errorMsg == 1) {
                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong>Ocorreu algum erro ao processar a imagem tente com outra!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                exit();
            } elseif (move_uploaded_file($temp_file, $target_file)) {

                $source = imagecreatefromjpeg($target_file);

                list($width, $height) = getimagesize($target_file);

                $newwidth = $width / 3;
                //$newwidth = 200;
                $newheight = $height / 3;
                //$newheight = 200;

                $destination = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($destination, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                imagejpeg($destination, $miniatura_file, 100);

                if (($this->sessao->get('assistente') == "")) {
                    if ($modelsPostagem->atualizarPostagemComFoto($post)) {

                        echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Postagem atualizada no sistema!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                        ?>  <script language="javascript">
                            setTimeout(function () {
                                window.location.href = "/";
                            }, 2000);
                        </script>

                        <?php

                    } else {
                        echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível atualizar a postagem !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    }
                } else {

                    if ($modelsPostagem->atualizarPostagemComFotoPendente($post)) {

                        echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Postagem atualizada!!! (Pendente aprovação!)
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                        ?>  <script language="javascript">
                            setTimeout(function () {
                                window.location.href = "/";
                            }, 2000);
                        </script>

                        <?php

                    } else {
                        echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível atualizar a postagem !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    }
                }
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> A imagem não pode ser movida!!! 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                ?>  <script language="javascript">
                    window.location.href = "#";
                </script>

                <?php

            }
        }
    }

    function aceitarPendente($id) {
        $sucesso = 0;

        $msg = '';
        $postagem = new Postagem();
        $model = new ModelPostagem();

        $post = $model->buscarPostagem($id);
        $postagem->setId($id);
        foreach ($post as &$j) {
            $status = $j->status;
            if ($status == 0) {
                $postagem->setDataHora($j->dataHora);
                $postagem->setTitulo($j->titulo);
                $postagem->setTexto($j->texto);
                $postagem->setFoto($j->foto);
                $postagem->setUsuario_cpf($j->Usuario_cpf);
            }else if ($status == 1) {
                $postagem->setDataHora($j->dataHora);
                $postagem->setTitulo($j->titulo);
                $postagem->setTexto($j->texto);
                $postagem->setFoto($j->foto);
                $postagem->setUsuario_cpf($j->Usuario_cpf);
            } else if ($status == 2) {
                $postagem->setDataHora($j->dataModificadoPendente);
                $postagem->setTitulo($j->tituloPendente);
                $postagem->setTexto($j->textoPendente);
                $postagem->setFoto($j->fotoPendente);
                $postagem->setUsuario_cpf($j->Usuario_cpf);
            }

            break;
        }

        if ($status == 1) {

            $sucesso = $model->efetivaCadastroPostagem($postagem);

            if ($sucesso > 0) {

                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Postagem Aceita com Sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

                $this->sessao->add("msgChangePostagem", $msg);

                $this->redirectToPostagems();
            } else {

                $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! Cadastro não efetuado!</strong> !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                // echo $msg;
                $this->sessao->add("msgChangePostagem", $msg);

                //$this->sessao->add("msgPostagem", $msg);
                $this->redirectToPostagems();
            }
        } else if ($status == 2) {

            $postagem->setTopo(0);

            $sucesso = $model->efetivaAlteracaoPostagem($postagem);


            if ($sucesso > 0) {

                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Postagem Alterada com Sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

                $this->sessao->add("msgChangePostagem", $msg);

                $this->redirectToPostagems();
            } else {

                $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! Alteração não efetuada!</strong> !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                // echo $msg;
                $this->sessao->add("msgChangePostagem", $msg);

                // $this->sessao->add("msgPostagem", $msg);

                $this->redirectToPostagems();
            }
        } else if ($status == 3) {
            $sucesso = $model->deletePostagem($id);


            if ($sucesso > 0) {

                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Postagem Excluída com Sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

                $this->sessao->add("msgChangePostagem", $msg);

                $this->redirectToPostagems();
            } else {

                $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! Remoção não efetuada!</strong> !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                // echo $msg;
                $this->sessao->add("msgChangePostagem", $msg);

                // $this->sessao->add("msgPostagem", $msg);
                $this->redirectToPostagems();
            }
        }
    }

    function gravarPostagem() {

        $this->redirect();
        $titulo = $this->request->get('titulo');
        $texto = $this->request->get('editTextPost');

        $usuarioCPF = $this->sessao->get('cpfLogado');
        //$topo = $this->request->get('topo');
        //$topo = 0;
        //$date1 = strtr($dataHora, '/', '-');
        //$date_time = date('Y-m-d h:i:s', strtotime($date1));

        $imagem = $this->request->files->get('imagem');


        if ($imagem == NULL || $imagem == "") {
            /* echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">

              <strong>Atenção! </strong> A imagem não foi selecionada!!!
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
              </div>';
              ?>  <script language="javascript">
              window.location.href = "#";
              </script>

              <?php */

            $data_local = new \DateTime('now', new \DateTimeZone('-0300'));
            $data_ultimo_login = $data_local->format('Y-m-d H:i:s');

            $post = new Postagem();
            $post->setDataHora($data_ultimo_login);
            $post->setUsuario_cpf($usuarioCPF);
            $post->setTexto($texto);
            $post->setTitulo($titulo);


            $modelsPostagem = new ModelPostagem();

            if ($this->sessao->get('assistente')) {
                $status = 1;
                if ($modelsPostagem->cadastrarPostagemSemFotoPendente($post, $status)) {



                    echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Postagem Submetida com sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    ?>  <script language="javascript">
                        setTimeout(function () {
                            window.location.href = "/";
                        }, 2000);
                    </script>

                    <?php

                } else {
                    echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível submeter a postagem !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                }
            } else {
                if ($modelsPostagem->cadastrarPostagemSemFoto($post)) {

                    echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Postagem Cadastrada no sistema!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    ?>  <script language="javascript">
                        setTimeout(function () {
                            window.location.href = "/";
                        }, 2000);
                    </script>

                    <?php

                } else {
                    echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível cadastrar a postagem !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                }
            }
        } else {



            $target_file = __DIR__ . '/../../public_html/img/' . $usuarioCPF . $imagem->getClientOriginalName();
            $miniatura_file = __DIR__ . '/../../public_html/img/miniaturas/' . $usuarioCPF . $imagem->getClientOriginalName();
            $temp_file = $_FILES["imagem"]["tmp_name"];
            $name = $_FILES["imagem"]["name"];
            $type = $_FILES["imagem"]["type"];
            $sizes = $_FILES["imagem"]["size"];
            $errorMsg = $_FILES["imagem"]["error"];
            $explode = explode(".", $name);
            $extension = end($explode);


            if ($sizes > 5242880) {
                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Selecione arquivo menor que 5MB!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

                exit();
            } else if (!preg_match("/\.(gif|jpg|png|jpeg)$/i", $name)) {

                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong> Selecione arquivo menor e tente novamente!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                exit();
            } else if ($errorMsg == 1) {
                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>ERRO! </strong>Ocorreu algum erro ao processar a imagem tente com outra!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                exit();
            } elseif (move_uploaded_file($temp_file, $target_file)) {

                $source = imagecreatefromjpeg($target_file);
                list($width, $height) = getimagesize($target_file);

                $newwidth = $width / 3;
                //$newwidth = 200;
                $newheight = $height / 3;
                //$newheight = 200;

                $destination = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($destination, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                imagejpeg($destination, $miniatura_file, 100);


                //$date1 = strtr($dataHora, '/', '-');
                //$date_time = date('Y-m-d h:i:s', strtotime($date1));
                $data_local = new \DateTime('now', new \DateTimeZone('-0300'));
                $data_ultimo_login = $data_local->format('Y-m-d H:i:s');

                $post = new Postagem();
                $post->setDataHora($data_ultimo_login);
                $post->setUsuario_cpf($usuarioCPF);
                $post->setTexto($texto);
                $post->setTitulo($titulo);

                $post->setFoto($usuarioCPF . $imagem->getClientOriginalName());


                $modelsPostagem = new ModelPostagem();

                if ($this->sessao->get('assistente')) {

                    if ($modelsPostagem->cadastrarPostagemPendente($post)) {

                        echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Postagem Submetida com sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                        ?>  <script language="javascript">
                            setTimeout(function () {
                                window.location.href = "/";
                            }, 2000);
                        </script>

                        <?php

                    } else {
                        echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível submeter a postagem !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    }
                } else {


                    if ($modelsPostagem->cadastrarPostagem($post)) {

                        echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Postagem Cadastrada no sistema!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                        ?>  <script language="javascript">
                            setTimeout(function () {
                                window.location.href = "/";
                            }, 2000);
                        </script>

                        <?php

                    } else {
                        echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>FALHA! </strong> Não foi possível cadastrar a postagem !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    }
                }
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> A imagem não pode ser movida!!! 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                ?>  <script language="javascript">
                    window.location.href = "#";
                </script>

                <?php

            }
        }
    }

    /* function buscarPostagem() {
      $ra = $this->request->get('ra');
      $modelPostagem = new ModelPostagem();
      $dados = $modelPostagem->buscarPostagem($ra);

      $this->response->setContent($this->twig->render("/alterarPostagem.twig", ["dados" => $dados]));
      }
     */

    function removePostagem($id) {
        $sucesso = 0;

        $msg = '';
        $aluno = new Postagem();
        $model = new ModelPostagem();

        if ($this->sessao->get('assistente')) {
            $sucesso = $model->deletePostagemPendente($id);
        } elseif ($this->sessao->get('logado')) {
            $sucesso = $model->deletePostagem($id);
        }

        if ($sucesso > 0) {
            // $model->buscarPostagemFoto($aluno);
            //$img = $aluno->getImagem();
            //echo $img;
            //$FilePath = __DIR__ . '/../../public_html/img/';
            //$FilePath2 = __DIR__ . '/../../public_html/img/miniaturas/';
            //chdir($FilePath);
            //chown($img, 465);
            //chdir($FilePath2);
            //chown($img, 465);
            //$do = unlink($img);
            //if ($do == "1") {
            //    echo "A foto foi deletada com sucesso!.";
            //} else {
            //    echo "Ocorreu um erro ao tentar excluir a imagem!";
            //}

            if ($this->sessao->get('assistente')) {
                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Remoção submetida com Sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            } elseif ($this->sessao->get('logado')) {
                $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Postagem Removida com Sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            }


            //echo $msg;
            $this->sessao->add("msgChangePostagem", $msg);

            // $this->sessao->add("removePostagem", "yes");
            //$this->sessao->add("msgPostagem", $msg);
            //$this->sessao->add("msgPostagem", $msg);


            $this->redirectToPostagems();
        } else {

            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! Exclusão não efetuada!</strong> A postagem foi preservada por algum motivo !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            // echo $msg;
            $this->sessao->add("msgChangePostagem", $msg);

            // $this->sessao->add("msgPostagem", $msg);
            $this->redirectToPostagems();
        }
    }

}
