<?php

namespace JIF\Controller;

use JIF\Entity\Usuario;
use JIF\Models\ModelUsuario;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use JIF\Util\Sessao;
use Swift_Transport;

class ControllerUsuario {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function login() {
        //$this->redirect();
        return $this->response->setContent($this->twig->render("/login.twig"));
    }

    public function telaTecnicoAdmin() {
        $this->redirectSemPermissaoAdmin();
        return $this->response->setContent($this->twig->render("/cadastroUsuario.twig"));
    }

    public function telaSenha() {
        $this->redirectSemPermissao();
        return $this->response->setContent($this->twig->render("/alterarSenhaUsuario.twig"));
    }

    public function removeCRUDMenuSessao() {

        if (!empty($this->sessao->get('remove'))) {
            $this->sessao->add('remove', '');
        } elseif (!empty($this->sessao->get('update'))) {
            $this->sessao->add('update', '');
            $this->sessao->add("msg", '');
        } elseif (!empty($this->sessao->get('view'))) {
            $this->sessao->add('view', '');
            $this->sessao->add("msg", '');
        }
    }

    public function telaAtualizaTecnicoAdmin() {
        $this->redirectSemPermissaoAdmin();
        $this->removeCRUDMenuSessao();


        $this->sessao->add('update', 'yes');

        $this->response->setContent($this->twig->render("/busca.twig"));
    }

    public function telaRemoveTecnicoAdmin() {
        $this->redirectSemPermissaoAdmin();
        $this->removeCRUDMenuSessao();

        $this->sessao->add('remove', 'yes');


        $this->response->setContent($this->twig->render("/busca.twig"));
    }

    public function telaBuscaTecnicoAdmin() {
        $this->redirectSemPermissaoAdmin();
        $this->removeCRUDMenuSessao();
        $msg = '';
        $this->sessao->add('view', 'yes');


        $this->response->setContent($this->twig->render("/busca.twig"));
    }

    //public function busca() {
    //     $this->redirect();
    //     $this->response->setContent($this->twig->render("/busca.twig"));
    // }

    function redirect() {
        if (empty($this->sessao->existe('logado'))) {
            //  $redirect = new RedirectResponse('/');
            //$redirect->send();
            ?>  <script language="javascript">
                window.location.href = "/";
            </script>

            <?php

        }
    }

    function redirectSemPermissaoAdmin() {
        if (empty($this->sessao->existe('administrador'))) {
            //$redirect = new RedirectResponse('/');
            // $redirect->send();
            ?>  <script language="javascript">
                window.location.href = "/";
            </script>

            <?php

        }
    }

    function redirectSemPermissao() {
        if (empty($this->sessao->existe('logado'))) {
            //$redirect = new RedirectResponse('/login');
            //$redirect->send();
            ?>  <script language="javascript">
                window.location.href = "/";
            </script>

            <?php

        }
    }

    function removeTecnicoAdmin() {
        $this->redirectSemPermissaoAdmin();
        $cpf = $this->request->get('cpf');
        $usuario = new Usuario();
        $usuario->setCpf($cpf);
        $model = new ModelUsuario;
        $sucesso = $model->removeTecnico($usuario);
        $msg = '';

        if ($sucesso == 2) {


            $this->sessao->add("buscaEfetivadaRemove", "");
            $this->sessao->add("remove", "");
            $this->sessao->add("msg", $msg);
            $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Usuário com o CPF: ' . $cpf . ' excluído !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

            $this->sessao->add("msg", $msg);
        } else {

            $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! Exclusão não efetuada!</strong> O usuário do CPF: ' . $cpf . ' foi preservado por algum motivo !!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

            $this->sessao->add("msg", $msg);
        }
    }

    public function atualizaTecnicoAdmin() {

        $this->redirectSemPermissaoAdmin();

        $cpf = $this->request->get('cpf');
        $nome = $this->request->get('nome');
        $telefone = $this->request->get('telefone');
        if ($telefone == "") {
            $telefone = null;
        }

        $siape = $this->request->get('siape');
        if ($siape == "") {
            $siape = null;
        }

        $email = $this->request->get('email');

        $campus = $this->request->get('campus');

        $permissao = $this->request->get('permissao');

        $senha = $this->gerar_senha(8, true, true, true, false);

        $data_local = new \DateTime('now', new \DateTimeZone('-0300'));
        //$data_cadastro = $data_local->format('Y-m-d H:i:s');


        if ($cpf == $this->sessao->get('cpf') && $nome == $this->sessao->get('nome') && $email == $this->sessao->get('email') && $permissao == $this->sessao->get('permissao') && $siape == $this->sessao->get('siape') && $campus == $this->sessao->get('campus') && $permissao == $this->sessao->get('permissao')) {

            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Dados inalterados!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
        } else {
            if (!empty($cpf) && !empty($nome) && !empty($email) && !empty($permissao) && !empty($campus)) {

                $usuario = new Usuario();
                $usuario->setCpf($cpf);
                $usuario->setEmail($email);
                $usuario->setNome($nome);
                $usuario->setSiape($siape);
                $usuario->setTelefone($telefone);
                $usuario->setPermissao($permissao);
                $usuario->setCampus($campus);
                //$usuario->setData_cadastro($data_cadastro);
                $password_hash = password_hash($senha, PASSWORD_BCRYPT);

                $usuario->setSenha($password_hash);

                $model = new ModelUsuario;

                $sucesso = $model->atualizarTecnico($usuario);


                if ($sucesso == 2) {


                    echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Usuário Atualizado com Sucesso!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

                    $this->sessao->add('nome', $nome);
                    $this->sessao->add('telefone', $telefone);
                    $this->sessao->add('siape', $siape);
                    $this->sessao->add('email', $email);
                    $this->sessao->add('permissao', $permissao);
                    $this->sessao->add('campus', $campus);
                    $this->sessao->add('cpf', $cpf);
                    ?>  <script language="javascript">
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    </script>

                    <?php

                } else if ($sucesso == 1) {

                    echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Algo não deu certo, algum dados já existentes (email, cpf ou siape)!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                }
            } else {

                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Algum campo em branco!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            }
        }
    }

    //Essa classe abaixo é pra ser implementada para alterar senha do usuario
    public function formAlteraSenha() {

        $cpf = $this->request->get('cpf');
        $nome = $this->request->get('nome');
        $telefone = $this->request->get('telefone');
        $siape = $this->request->get('siape');
        $email = $this->request->get('email');
        $senha = $this->request->get('senha');
        $conf_password = $this->request->get('conf_password');
        $atual = $this->request->get('atual');

        $permissao = $this->sessao->get('permissaoLogado');
        
        $data_local = new \DateTime('now', new \DateTimeZone('-0300'));
        $data_cadastro = $data_local->format('Y-m-d H:i:s');
        $data_ultimo_login = $data_local->format('Y-m-d H:i:s');

        if (strlen($senha) < 4 || strlen($conf_password) < 4) {

            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> As senhas tem que ter no minimo 4 digitos!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
        } else if (empty($conf_password)) {
            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Digite a senha de confirmação!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
        } elseif ($senha != $conf_password) {
            echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> As senhas digitadas não batem!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
        } else {

            if (!empty($cpf) && !empty($nome) && !empty($email) && !empty($senha) && !empty($conf_password)) {

                $usuario = new Usuario();
                $usuario->setCpf($cpf);
                $usuario->setEmail($email);
                $usuario->setNome($nome);
                $usuario->setSiape($siape);
                $usuario->setTelefone($telefone);

                $usuario->setData_cadastro($data_cadastro);
                $usuario->setData_ultimo_login($data_ultimo_login);
                $password_hash = password_hash($conf_password, PASSWORD_BCRYPT);
                
                //echo $conf_password.'<br/>';
                
                //echo $password_hash;

                $usuario->setSenha($password_hash);

                $model = new ModelUsuario;

                $confereSenhaAtual = $model->existeSenha($usuario, $atual);


                if ($confereSenhaAtual == 1) {

                    $sucesso = $model->atualizarTecnicoBefore($usuario);

                    if ($sucesso == 2) {

                        $this->sessao->add('siapeLogado', $siape);
                        $this->sessao->add('telefoneLogado', $telefone);
                        $this->sessao->add('emailLogado', $email);
                        $this->sessao->add('nomeLogado', $nome);
                        
                        if ($permissao == 'tecnico') {
                            $msg = '<p><strong>Passo 1 -</strong> Acesse <a href="https://www.jiftm.com">www.jiftm.com</a>&nbsp;</p>

<p><strong>Passo 2 - </strong>Clique no meu superior <span style="font-size:14px"><strong><span style="color:#f39c12"><span style="background-color:#4e5f70">Fazer Login.</span></span></strong></span></p>

<p><strong>Passo 3 - </strong>Digite seu CPF e essa senha que foi lhe enviada por e-mail.</p>

<p><strong>Passo 4 -</strong> Se n&atilde;o for redirecionado para p&aacute;gina de altera&ccedil;&atilde;o de senha favor clicar no painel -> painel esquerdo em <strong>Usu&aacute;rio -&gt; Atualizar Meus Dados</strong> (A sua senha sempre ser&aacute; enviada para seu e-mail<br />
caso esque&ccedil;a).</p>

<p><strong>Passo 5 </strong>- Em Atleta Clique em <strong>Cadastro </strong>para cadastrar atletas, clique <strong>Pesquisar </strong>caso deseje ver os alunos cadastrados ou at&eacute; o &uacute;ltimo que foi cadastrado.</p>

<p><strong>Passo 6 -</strong> Com os Atletas cadastrados clique em <strong>Modalidade-&gt; formar</strong> , selecione a Modalidade desejada e o sexo na caixinha seletora escrita<strong> <span style="color:#000000">(Modalidade e Sexo:)</span>,</strong><br />
Selecione os atletas e clique em <strong>Formar</strong>, se desejar visualizar, alterar, gerar crach&aacute;s por time clique em visualizar no menu <strong><em>Modalidades</em></strong>, para excluir uma modalidade &eacute;&nbsp;<br />
necess&aacute;rio <span style="color:#c0392b">remover </span>todos os atletas da modalidade!</p>

<p><strong>Passo&nbsp;7 </strong>- Caso deseja Gerar Crach&aacute;s de todo o Campus favor clicar em<strong> Gerar Crach&aacute;s.</strong></p>';
                        } else {
                            $msg = '<p><strong>Passo 1 -</strong> Acesse <a href="https://www.jiftm.com">www.jiftm.com</a>&nbsp;</p>

<p><strong>Passo 2 - </strong>Clique no meu superior <span style="font-size:14px"><strong><span style="color:#f39c12"><span style="background-color:#4e5f70">Fazer Login.</span></span></strong></span></p>

<p><strong>Passo 3 - </strong>Digite seu CPF e essa senha que foi lhe enviada por e-mail.</p>

<p><strong>Passo 4 -</strong> Se n&atilde;o for redirecionado para p&aacute;gina de altera&ccedil;&atilde;o de senha favor clicar no painel esquerdo em <strong>Usu&aacute;rio -&gt; Atualizar Meus Dados</strong> (A sua senha sempre ser&aacute; enviada para seu e-mail<br />
caso esque&ccedil;a).</p>

<p><strong>Passo 5</strong> - No site principal voc&ecirc; poder&aacute; <em><strong>Criar postagens (Nova postagem)</strong></em>, <em><strong>Excluir </strong></em>ou <strong><em>Alterar</em></strong>&nbsp;(<span style="color:#c0392b">Pendente de aprova&ccedil;&atilde;o do Administrador</span>).</p>

<p><strong>Passo 6</strong> - Na parte jogos voc&ecirc; poder&aacute; clicar em um jogo para <strong><em>Alterar placar </em></strong>em tempo real e <strong><em>Adicionar coment&aacute;rios em tempo real</em></strong>! (Clique em salvar sempre que alterar placar ou adicionar um coment&aacute;rio).</p>

<p><strong>Passo 7</strong>&nbsp;- Qualquer d&uacute;vida contate um administrador na parte de desenvolvedores!&nbsp;</p>

<p><strong>Obrigado</strong>.&nbsp;<img alt="wink" src="https://cdn.ckeditor.com/4.11.4/full/plugins/smiley/images/wink_smile.png" style="height:23px; width:23px" title="wink" /></p>

<p>&nbsp;</p>

';
                        }

                        $transport = (new \Swift_SmtpTransport('jiftm.com', 465, 'ssl'))
                                ->setUsername('senhas@jiftm.com')
                                ->setPassword('rifxletgaiqsufur');


                        $mailer = new \Swift_Mailer($transport);


                        $data = date('d/m/Y');
                        $message = (new \Swift_Message('Seu Login e Senha no Site do JIF IFTM ' . $data))
                                ->setFrom(['senhas@jiftm.com' => 'JIF IFTM PARACATU'])
                                ->setTo([$email])
                                ->setBody('
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <style>


           

            /* Some resets and issue fixes */
            #outlook a { padding:0; }
            body{ width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; }     
            .ReadMsgBody { width: 100%; }
            .ExternalClass {width:100%;} 
            .backgroundTable {margin:0 auto; padding:0; width:100%;} 
            table td {border-collapse: collapse;}
            .ExternalClass * {line-height: 115%;}    

            /* End reset */


            /* These are our tablet/medium screen media queries */
            @media screen and (max-width: 630px){


                /* Display block allows us to stack elements */                      
                *[class="mobile-column"] {display: block;} 

                /* Some more stacking elements */
                *[class="mob-column"] {float: none !important;width: 100% !important;}     

                /* Hide stuff */
                *[class="hide"] {display:none !important;}          

                /* This sets elements to 100% width and fixes the height issues too, a god send */
                *[class="100p"] {width:100% !important; height:auto !important;}                    

                /* For the 2x2 stack */            
                *[class="condensed"] {padding-bottom:40px !important; display: block;}

                /* Centers content on mobile */
                *[class="center"] {text-align:center !important; width:100% !important; height:auto !important;}            

                /* 100percent width section with 20px padding */
                *[class="100pad"] {width:100% !important; padding:20px;} 

                /* 100percent width section with 20px padding left & right */
                *[class="100padleftright"] {width:100% !important; padding:0 20px 0 20px;} 

                /* 100percent width section with 20px padding top & bottom */
                *[class="100padtopbottom"] {width:100% !important; padding:20px 0px 20px 0px;} 


            }


        </style>



        <style type="text/css">

        </style>

        <style type="text/css">

        </style>
    </head>


    <div style="background:#ffffff;">

    </div><body style="padding:0; margin:0;" bgcolor="#ffffff">

        <table border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0" width="100%">
            <tr>
                <td align="center" valign="top">

                    <table width="640" border="0" cellspacing="0" cellpadding="0" class="hide">
                        <tr>
                            <td height="20"></td>
                        </tr>
                    </table>

                    <table width="640" cellspacing="0" cellpadding="0" bgcolor="#" class="100p">
                        <tr>
                            <td bgcolor="#3b464e" width="640" valign="top" class="100p">

                                <div>
                                    <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p">
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                    <tr>


                                                        <td align="center" width="50%" class="100p"><img src="https://lh3.googleusercontent.com/iH0EKP0Jg9mbCkXxBFVItsbnonrc2W9UTKc3tRqhp7QSN69F2nqt9MLMGqJaG1xSTVu27MVM9bMz3FzPrhJ8BPNuxT6Lky5lNjQq5vVIWClng17M-Q5CnbZGcgh74wuL6fl-2cD6W5H1cvRmZAwt9PGEVnkXIqXCz0SAvNkS3GgOfXvaGnRmKi1QO_5Odm4aqB6O7HGT5FkUol0-0PaxoQmabNky1HXg61WVe8JkJjN01wxR4kidfVUOCBs3b6sQgPMAe5k7nMt7_4A0bNhUw-oakT7x_kmi57pyZasabQazoz-0Ua4QNLYdw6bDC17I8wcxYzs1uyU_xhIfqYhskcNoZY2RWElB2ZQg8sO00BzW2L8f3QUgcHAtZA6qYPxDmVAUVzyMWtezuUpyAgf6oFK10kIHbgQQrytYyRvnWtdql7-B0Q4xnPIuvSn0GHG9KQIutKPrNhZhlgdt367rfMmRErQibR06VhN6xclAdxrLwJI6XIB5ECfSokskviY8OVoPCs3hOS_N7nx7oB5I-w6njKtZQRbCeqPZbHW0VVUozgD7EC7ag5TFetAwTsGUk3ld0AwznyDFJsRvznFhE5A87sjyWhEvnV_8ZL_ksM1Ef-SSiivEXxJeSVLqW4EACiSzC8C9gWhkTS_bGX7JC6uJ8aRVLwc=w998-h937-no" width="100%" height="50%" />                                                    
                                                        </td></tr>
                                                </table>
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                    <tr>
                                                        <td height="35"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="color:#FFFFFF; font-size:24px;">
                                                            <font face="Arial, sans-serif">
                                                                <span style="font-size:44px;">SUA SENHA NO SITE JIF</span><br />
                                                                <br />
                                                                <span style="font-size:14px;">(Jogos dos Institutos Federais 2019) </span>
                                                                <br />
                                                                <br />
                                                                <span style="font-size:24px;">Anote sua senha! </span>
                                                                <br /><br />

                                                                <a href="##" style="color:#FFFFFF; text-decoration:none;">
                                                                </a></font>


                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="35"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </td>
                        </tr>
                    </table>
                    <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#2a8e9d" class="100p">
                        <tr>
                            <td align="center" style="font-size:24px; color:#FFFFFF;"><font face="Arial, sans-serif">DADOS: </font></td>
                        </tr>
                    </table>
                    <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p" bgcolor="#FFFFFF">
                        <tr>
                            <td align="center" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" class="100padtopbottom" width="600">
                                    <tr>
                                        <td align="center" style="font-size:24px; color:#FFFFFF;">

                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="290">
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td valign="top" style=" width: 100%;" class="100padleftright">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td width="135" align="center"><img src="https://lh3.googleusercontent.com/RVL9pzRkyY_89EmMvlpbuYbFoXSxqnnKVxlTnY8Xbd45NAubjN4clNKm9L5T74vAVWF1wS6m9CyzMHsAlZDeXyOzQOR9mUXLgrApdTR_rWEEwQd2jQXg3zKXuvb2fnvqPsvcQarwkYPf2oQC4wFkWNwzOHOBcp08cDpyTZbR8RL7SJrQs6HBeuMuf7kj19AMm9YirX9-VabFEPRk5r0Pp6TjiOCHhD-Zavfgk1Ml5mvu_bLabiOUYZ8RvhOADC7y5V8wtPPi-7FUnDDlv-ol7S2vtPd5z2sbNhw5oNQow02OfW09Ss7ObzUrzqSHQBop1CkLLFIhxVo5zP_DJtZTGPxJAyH60woRvIWz4QLciJpxomhW8XRUypCeUtrsYnv_ar76egZ4tYhUzJSr03GVrih0pfQfeROYWsbB__Ye2ymkEqsOvRJvAg79s99KKWrJoahuGvkKzIlxFKHH7fnf2x5C9wIzEBnCGyiUWfnbjGw_Wl_QjtotkGBGVJWwJRAvGtkitRFWZP3BBGG-4ZipxzA_dx7OxwGoIAC7pjkKXXYUKS9fGHvtasYPWL4rLP2cAaoxuHN7Uq93J_DkbvY02wxKBDoDLf57riSisG1vT8ltUr5bOxQBysvFdIshMFpzFFHbHImxy0-lTNRhEOHKpFt__q3J1Yo=s64-no" border="0" style="display:block;" /></td>
                                                                            <td valign="top" width="135" align="center" style="font-size:16px; color:#2a8e9d;"><font face="Arial, sans-serif">CPF: </font></td>
                                                                            <td style="padding:10; color:gray;" width="1" >' . $cpf . '</td>
                                                                            <td width="20"></td>
                                                                            <td style="padding:10; width: 100px; padding-left:40;"  width="135" align="center"><img src="https://lh3.googleusercontent.com/cUDN3isQ1hv6tu53bPy_FvTazlSksJVj6lQjBvywpb9wSEUGYq7Yw-FvwV3ZJia9iI47EoM0TfD9eRUWtqpaG3h6gfM8aJyJYbS1HFlsedeXke3FfR-xZK52Sf-jp08_8jCSH0Dk3jSUuZ6UpRb1dDWzZLjFveuoc5rU6SQnkfMFfh_w3COF2fwQU1VWUhup562XaX7HGgkJDULi19rqPbPCG7s3WRP26oMryDLkO0VHPLaEn03CyxM6R7GFqtIudMAUJ8j5RZlMbQO4RlTdYbkBM7CMdefNJSFEPINkVePDKLQDPnUkWEzH07Qg67dX01bKot4n3KijA8u0oMRVGZiYK61YA56X4termH_TRX4M2K9N6bnwpZRveMtMsIsxZKD6tweptaQHXuuaR5W3xb_geqS0ZIHYj5zZHO5qMNDRfXtfvF8CJhjMmoxQ_vHHukcl9EJTm1EkcFjqTbxiW0p0TKG71m1gE5uU-PUUcA06Y8LLOMGPzjvgwDBJHGcPOhDJ3x0K0XqLF8qTN51EmJzFRcYLSKt35NsQP_OT-zUHP42Co4ZnjN7YilhRSXPg1ek9_AdV9qUSrDRyLUU33DIWnFuLM8y8BEP8_WXA2dEiWyi_kSmcDiwiG8DTOVKVO26LAn_Fx7SVS0SfoLNKDVASmdq76mw=s64-no" border="0" style="display:block;" /></td>
                                                                            <td valign="top" width="135" align="center" style="font-size:16px; color:#2a8e9d;"><font face="Arial, sans-serif">SENHA: </font></td>
                                                                            <td style="padding:10; color:gray;" width="1">' . $senha . '</td>




                                                                            <td width="20"></td>

                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>


                                            
                                    </tr>
                                </table>
                            </td>
                            <tr>
                                            '.$msg.'</tr>
                        </tr>
                    </table>
                    <table width="640" border="0" cellspacing="0" cellpadding="0" bgcolor="#b2d6db" class="100p" height="1">
                        <tr>
                            <td width="20" bgcolor="#FFFFFF"></td>
                            <td align="center" height="1" style="line-height:0px; font-size:1px;">&nbsp;</td>
                            <td width="20" bgcolor="#ffffff"></td>
                        </tr>
                    </table>



                    <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                        <tr>
                            <td align="left" width="50%" style="font-size:14px; color:#848484;"><font face="Arial, sans-serif">&copy; IFTM ©2019</font></td>

                        </tr>
                    </table>

                    <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                        <tr>


                            <td align="right" width="50%" style="font-size:14px; color:#848484; "> <div align="center" ><a href="https://www.instagram.com/iftmptu/" target="__blank"><img src="https://lh3.googleusercontent.com/TmRXghisESH7MF9l8LrxEt0iifEfUw5C_UPGpnN5Yog2D_I3KKanDSrvEimloOEHf_TUHTex0JXG2GB2D3pY_lZTQGP597g8hGMemrkVl8pE3iHq3uTLOOPwi6Hsz6_vXdMvSdcF-p4VY2v6J90_udFXKALgCPJbhzYVdQuFhI7pJfHJ5sXbbRzGDsBi-5hJvfVNaW8H4vN3bp4v5j-K2YFjV7GNxdpVLrbO9e4r6MBVr6dYEPdTU0mYGQX4JtSQyAx2Hj5BLPWTrBvQcq83BklDHLUgg-fKh6GyjSxUF2fzw87vskqDpJlutdpvNF0FVp9KEDi8Yb_k8L1aT8_aY9nc9X4L-SUsgMSBz1Uk0XLHJDrqWNff2ApybJCcWs4Uqi-mRGKUps_gXIXM0jOw92uE4kwF0Vza__T-bdhrfz8s84yyQzwli2cdSaiDEwCpPLMkUFglmeJBGdKcYpF_F9EVxlT6jCquKx7C1G9ABkl03iqhpvx-nkkwLN190N3nkStRPxtfeTBc1uz2-6fE6uDh6KnyOtgZ0cHoH46niZFCVEvS4KPYxA6s_Q_vzyt7xOYeF_H4D2wgwUTn063G65ULIfnOsx3HcD1p6AFp9qYgYb2_79vsND8cm98gY0YZ5fCfYl1mNjc_QffjyK0A9QHZnw9pm3VrrVtZRA9urcu-bqh5B78l8KU-N2p8z8oupWsShemAzTg6FktdG_zg3GIY=s85-no" width="50" height="50" style="margin:2%;" /></a><a href="http://www.iftm.edu.br" target="__blank"><img src="https://lh3.googleusercontent.com/urszkwuXzguRFg9LG5sR6zYdWnW7zT9pTMWhbRlRbzfv13LTOSBvmbtCdMmj9R0q_6lpZeMwTVBY9UsOycAW6MSqptLboBh-cxtk1Fm2D4h8mZCGfSm9Sp9wiBZxq9CzII5UG-kWDEQS1eIlMMBr1eTr2KboDCNwMg_y-Is17WSLpuGOLeCEMknJRlCHUS75_6gAJN-GVwrjMT3EjzgC6wQirCmoNruHAgh1g5QgIwzQl17153nY0dJZDPSlxSn_US5S9gu8W1LHNxAsnDtqtFNlkrxCq_unQJTqUJFG-kz5H06-I2f91ra803zy1DPW3w6bH63JzvcnVarPNQImbTYVwuN9BKFPtHHsz-6-5MfSzf0JkiDtRxlLX4eHDlH36ikNZjlQ0J2DL4rtOhAeIJCWe0TkA1WPoxPOVKiPvgfAflb6Bc3_5iSe0zpnlt_605agIzDGG5tlNVpQ0K5FOB4FWitYvuFt1ywGQvYdxmYN1d17fNpnwKWJN3K3TCv2ll6ZQ3pMmGP3NImkDjci8oRi3owVGCDHyG_-jePn2OJILstovnUNTPCiO5oLvA1OlT6yd86h8vzEF6L3UjZ07QEKpynl6-urY-LLlKq_SlQa81i7hE8IxWJxCNGYdSAB1Q_gyPWJIgdILfc79wo2ZnRYDbFLvfI=s225-no" width="50" height="50"style="margin:2%;"  /></a><a href="https://www.facebook.com/IftmCampusParacatu/" target="__blank"><img style="margin:2%;" src="https://lh3.googleusercontent.com/4jWPbxfKAXAd-rndA4SiXBXjUI7055G9qiGeL0n_LWJySFW0DVMt1n8gWFbscB1ClCcerh_4Px98Txyk-3io5UvazoiErn3QrzA9eZk-R32PJlfCFM8cKFmGTBifsVBJZXfO3y4PSRwygqTwDbTtSxNwOptjYm93d5XqaH0eWZRrFDqIq0gCDrpg0lL4ZgKva3OlFiEnGYDC1bTF6KrAmdsg8W1BTg9I2T1Tv3EvdEmUIMzTHT-oX--D7yP9gvQJVh_F-5m-X5e61944F7g7XV9MrGbygftZEli-Lpvq8PKjBW7v0gW_QdF_bkuBNMqD_VLaQQwrRuC29UqWl_FVSNBbxknADu3f5KAAkdRwh9yd8d69Yxwwujvv3M9_7EKTGnL4cDvW-v4NumFdjmyjHinM5N0EN4csQAqo9QaOehKOwYr5t-JGR-NcQJiOxDXtQUOdcv6mxShdKJCeR_ZzY0nfSlJYwTlso19YjEd38VmkEDUp7izYj495cLdjgTg7pod7e6TPMFzTQ6VIVbxWIoHVm5AK-wbWHsPqq-QLCJ1wMNLIHqYEfIm_a7ier0C8zhvF3XvYaGQzh9tAqdInvNrQd2SMM_ot6FG4lqmhQPn0YrbQROUbMIl3_yYWztBdtJcj5sWPciOwMAKHsQ1lJShcTbMFGQY=s85-no" width="50" height="50" /></a>
                                </div></td></tr>
                    </table>

                    <table width="640" class="100p" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="50">

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </body>
</html>', 'text/html')
                        ;

                        $result = $mailer->send($message);


                        echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Dados Atualizados!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';

                        //$this->sessao->del();
                        $model->ultimoLogin($usuario);
                      ?>  <script language="javascript">
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        </script>

                        <?php

                    } else if ($sucesso == 4) {
                        echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Usuário já existente!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    } else if ($sucesso == 1) {
                        echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Falha na execução!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                    }
                } else {
                    echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Senha Atual não confere!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                }
            } else {
                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Algum campo em branco!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            }
        }
    }

    function gerar_senha($tamanho, $maiusculas, $minusculas, $numeros, $simbolos) {
        $ma = "ABCDEFGHIJKLMNOPQRSTUVYXWZ"; // $ma contem as letras maiúsculas
        $mi = "abcdefghijklmnopqrstuvyxwz"; // $mi contem as letras minusculas
        $nu = "0123456789"; // $nu contem os números
        $si = "!@#$%¨&*()_+="; // $si contem os símbolos
        $senha = 0;

        if ($maiusculas) {
            // se $maiusculas for "true", a variável $ma é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($ma);
        }

        if ($minusculas) {
            // se $minusculas for "true", a variável $mi é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($mi);
        }

        if ($numeros) {
            // se $numeros for "true", a variável $nu é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($nu);
        }

        if ($simbolos) {
            // se $simbolos for "true", a variável $si é embaralhada e adicionada para a variável $senha
            $senha .= str_shuffle($si);
        }

        // retorna a senha embaralhada com "str_shuffle" com o tamanho definido pela variável $tamanho
        return substr(str_shuffle($senha), 0, $tamanho);
    }

    public function inserirTecnicoAdmin() {

        $this->redirectSemPermissaoAdmin();


        $cpf = $this->request->get('cpf');
        $nome = $this->request->get('nome');
        $telefone = $this->request->get('telefone');
        if ($telefone == "") {
            $telefone = null;
        }

        $siape = $this->request->get('siape');
        if ($siape == "") {
            $siape = null;
        }

        $email = $this->request->get('email');

        $campus = $this->request->get('campus');
        if ($campus == "") {
            $campus = null;
        }
        $permissao = $this->request->get('permissao');

        $senha = $this->gerar_senha(8, true, true, true, false);

        $data_local = new \DateTime('now', new \DateTimeZone('-0300'));
        $data_cadastro = $data_local->format('Y-m-d H:i:s');

        if (!empty($cpf) && !empty($nome) && !empty($email) && !empty($permissao)) {

            $usuario = new Usuario();
            $usuario->setCpf($cpf);
            $usuario->setEmail($email);
            $usuario->setNome($nome);
            $usuario->setSiape($siape);
            $usuario->setTelefone($telefone);
            $usuario->setPermissao($permissao);
            $usuario->setCampus($campus);
            $usuario->setData_cadastro($data_cadastro);
            $password_hash = password_hash($senha, PASSWORD_BCRYPT);


            $usuario->setSenha($password_hash);

            $model = new ModelUsuario;




            $sucesso = $model->cadastrarTecnico($usuario);


            if ($sucesso == 2) {

                if ($permissao == 'tecnico') {
                    $msg = '<p><strong>Passo 1 -</strong> Acesse <a href="https://www.jiftm.com">www.jiftm.com</a>&nbsp;</p>

<p><strong>Passo 2 - </strong>Clique no meu superior <span style="font-size:14px"><strong><span style="color:#f39c12"><span style="background-color:#4e5f70">Fazer Login.</span></span></strong></span></p>

<p><strong>Passo 3 - </strong>Digite seu CPF e essa senha que foi lhe enviada por e-mail.</p>

<p><strong>Passo 4 -</strong> Se n&atilde;o for redirecionado para p&aacute;gina de altera&ccedil;&atilde;o de senha favor clicar no painel esquerdo em <strong>Usu&aacute;rio -&gt; Atualizar Meus Dados</strong> (A sua senha sempre ser&aacute; enviada para seu e-mail<br />
caso esque&ccedil;a).</p>

<p><strong>Passo 5 </strong>- Em Atleta Clique em <strong>Cadastro </strong>para cadastrar atletas, clique <strong>Pesquisar </strong>caso deseje ver os alunos cadastrados ou at&eacute; o &uacute;ltimo que foi cadastrado.</p>

<p><strong>Passo 6 -</strong> Com os Atletas cadastrados clique em <strong>Modalidade-&gt; formar</strong> , selecione a Modalidade desejada e o sexo na caixinha seletora escrita<strong> <span style="color:#000000">(Modalidade e Sexo:)</span>,</strong><br />
Selecione os atletas e clique em <strong>Formar</strong>, se desejar visualizar, alterar, gerar crach&aacute;s por time clique em visualizar no menu <strong><em>Modalidades</em></strong>, para excluir uma modalidade &eacute;&nbsp;<br />
necess&aacute;rio <span style="color:#c0392b">remover </span>todos os atletas da modalidade!</p>

<p><strong>Passo&nbsp;7 </strong>- Caso deseja Gerar Crach&aacute;s de todo o Campus favor clicar em<strong> Gerar Crach&aacute;s.</strong></p>';
                } else {
                    $msg = '<p><strong>Passo 1 -</strong> Acesse <a href="https://www.jiftm.com">www.jiftm.com</a>&nbsp;</p>

<p><strong>Passo 2 - </strong>Clique no meu superior <span style="font-size:14px"><strong><span style="color:#f39c12"><span style="background-color:#4e5f70">Fazer Login.</span></span></strong></span></p>

<p><strong>Passo 3 - </strong>Digite seu CPF e essa senha que foi lhe enviada por e-mail.</p>

<p><strong>Passo 4 -</strong> Se n&atilde;o for redirecionado para p&aacute;gina de altera&ccedil;&atilde;o de senha favor clicar no painel esquerdo em <strong>Usu&aacute;rio -&gt; Atualizar Meus Dados</strong> (A sua senha sempre ser&aacute; enviada para seu e-mail<br />
caso esque&ccedil;a).</p>

<p><strong>Passo 5</strong> - No site principal voc&ecirc; poder&aacute; <em><strong>Criar postagens (Nova postagem)</strong></em>, <em><strong>Excluir </strong></em>ou <strong><em>Alterar</em></strong>&nbsp;(<span style="color:#c0392b">Pendente de aprova&ccedil;&atilde;o do Administrador</span>).</p>

<p><strong>Passo 6</strong> - Na parte jogos voc&ecirc; poder&aacute; clicar em um jogo para <strong><em>Alterar placar </em></strong>em tempo real e <strong><em>Adicionar coment&aacute;rios em tempo real</em></strong>! (Clique em salvar sempre que alterar placar ou adicionar um coment&aacute;rio).</p>

<p><strong>Passo 7</strong>&nbsp;- Qualquer d&uacute;vida contate um administrador na parte de desenvolvedores!&nbsp;</p>

<p><strong>Obrigado</strong>.&nbsp;<img alt="wink" src="https://cdn.ckeditor.com/4.11.4/full/plugins/smiley/images/wink_smile.png" style="height:23px; width:23px" title="wink" /></p>

<p>&nbsp;</p>

';
                }


                $transport = (new \Swift_SmtpTransport('jiftm.com', 465, 'ssl'))
                        ->setUsername('senhas@jiftm.com')
                        ->setPassword('rifxletgaiqsufur');


                $mailer = new \Swift_Mailer($transport);


                $data = date('d/m/Y');
                $message = (new \Swift_Message('Seu Login e Senha no Site do JIF IFTM ' . $data))
                        ->setFrom(['senhas@jiftm.com' => 'JIF IFTM PARACATU'])
                        ->setTo([$email])
                        ->setBody('
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <style>


           

            /* Some resets and issue fixes */
            #outlook a { padding:0; }
            body{ width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; }     
            .ReadMsgBody { width: 100%; }
            .ExternalClass {width:100%;} 
            .backgroundTable {margin:0 auto; padding:0; width:100%;} 
            table td {border-collapse: collapse;}
            .ExternalClass * {line-height: 115%;}    

            /* End reset */


            /* These are our tablet/medium screen media queries */
            @media screen and (max-width: 630px){


                /* Display block allows us to stack elements */                      
                *[class="mobile-column"] {display: block;} 

                /* Some more stacking elements */
                *[class="mob-column"] {float: none !important;width: 100% !important;}     

                /* Hide stuff */
                *[class="hide"] {display:none !important;}          

                /* This sets elements to 100% width and fixes the height issues too, a god send */
                *[class="100p"] {width:100% !important; height:auto !important;}                    

                /* For the 2x2 stack */            
                *[class="condensed"] {padding-bottom:40px !important; display: block;}

                /* Centers content on mobile */
                *[class="center"] {text-align:center !important; width:100% !important; height:auto !important;}            

                /* 100percent width section with 20px padding */
                *[class="100pad"] {width:100% !important; padding:20px;} 

                /* 100percent width section with 20px padding left & right */
                *[class="100padleftright"] {width:100% !important; padding:0 20px 0 20px;} 

                /* 100percent width section with 20px padding top & bottom */
                *[class="100padtopbottom"] {width:100% !important; padding:20px 0px 20px 0px;} 


            }


        </style>



        <style type="text/css">

        </style>

        <style type="text/css">

        </style>
    </head>


    <div style="background:#ffffff;">

    </div><body style="padding:0; margin:0;" bgcolor="#ffffff">

        <table border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0" width="100%">
            <tr>
                <td align="center" valign="top">

                    <table width="640" border="0" cellspacing="0" cellpadding="0" class="hide">
                        <tr>
                            <td height="20"></td>
                        </tr>
                    </table>

                    <table width="640" cellspacing="0" cellpadding="0" bgcolor="#" class="100p">
                        <tr>
                            <td bgcolor="#3b464e" width="640" valign="top" class="100p">

                                <div>
                                    <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p">
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                    <tr>


                                                        <td align="center" width="50%" class="100p"><img src="https://lh3.googleusercontent.com/iH0EKP0Jg9mbCkXxBFVItsbnonrc2W9UTKc3tRqhp7QSN69F2nqt9MLMGqJaG1xSTVu27MVM9bMz3FzPrhJ8BPNuxT6Lky5lNjQq5vVIWClng17M-Q5CnbZGcgh74wuL6fl-2cD6W5H1cvRmZAwt9PGEVnkXIqXCz0SAvNkS3GgOfXvaGnRmKi1QO_5Odm4aqB6O7HGT5FkUol0-0PaxoQmabNky1HXg61WVe8JkJjN01wxR4kidfVUOCBs3b6sQgPMAe5k7nMt7_4A0bNhUw-oakT7x_kmi57pyZasabQazoz-0Ua4QNLYdw6bDC17I8wcxYzs1uyU_xhIfqYhskcNoZY2RWElB2ZQg8sO00BzW2L8f3QUgcHAtZA6qYPxDmVAUVzyMWtezuUpyAgf6oFK10kIHbgQQrytYyRvnWtdql7-B0Q4xnPIuvSn0GHG9KQIutKPrNhZhlgdt367rfMmRErQibR06VhN6xclAdxrLwJI6XIB5ECfSokskviY8OVoPCs3hOS_N7nx7oB5I-w6njKtZQRbCeqPZbHW0VVUozgD7EC7ag5TFetAwTsGUk3ld0AwznyDFJsRvznFhE5A87sjyWhEvnV_8ZL_ksM1Ef-SSiivEXxJeSVLqW4EACiSzC8C9gWhkTS_bGX7JC6uJ8aRVLwc=w998-h937-no" width="100%" height="50%" />                                                    
                                                        </td></tr>
                                                </table>
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                    <tr>
                                                        <td height="35"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="color:#FFFFFF; font-size:24px;">
                                                            <font face="Arial, sans-serif">
                                                                <span style="font-size:44px;">SUA SENHA NO SITE JIF</span><br />
                                                                <br />
                                                                <span style="font-size:14px;">(Jogos dos Institutos Federais 2019) </span>
                                                                <br />
                                                                <br />
                                                                <span style="font-size:24px;">Por favor se preferir altere a senha no painel do site! ou no menu Usuário</span>
                                                                <br /><br />

                                                                <a href="##" style="color:#FFFFFF; text-decoration:none;">
                                                                </a></font>


                                                        </td>
                                                    </tr>
                                               
                                                    <tr>
                                                                       
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </td>
                        </tr>
                    </table>
                    <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#2a8e9d" class="100p">
                        <tr>
                            <td align="center" style="font-size:24px; color:#FFFFFF;"><font face="Arial, sans-serif">DADOS: </font></td>
                        </tr>
                    </table>
                    <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p" bgcolor="#FFFFFF">
                        <tr>
                            <td align="center" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" class="100padtopbottom" width="600">
                                    <tr>
                                        <td align="center" style="font-size:24px; color:#FFFFFF;">

                                            <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="290">
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td valign="top" style=" width: 100%;" class="100padleftright">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td width="135" align="center"><img src="https://lh3.googleusercontent.com/RVL9pzRkyY_89EmMvlpbuYbFoXSxqnnKVxlTnY8Xbd45NAubjN4clNKm9L5T74vAVWF1wS6m9CyzMHsAlZDeXyOzQOR9mUXLgrApdTR_rWEEwQd2jQXg3zKXuvb2fnvqPsvcQarwkYPf2oQC4wFkWNwzOHOBcp08cDpyTZbR8RL7SJrQs6HBeuMuf7kj19AMm9YirX9-VabFEPRk5r0Pp6TjiOCHhD-Zavfgk1Ml5mvu_bLabiOUYZ8RvhOADC7y5V8wtPPi-7FUnDDlv-ol7S2vtPd5z2sbNhw5oNQow02OfW09Ss7ObzUrzqSHQBop1CkLLFIhxVo5zP_DJtZTGPxJAyH60woRvIWz4QLciJpxomhW8XRUypCeUtrsYnv_ar76egZ4tYhUzJSr03GVrih0pfQfeROYWsbB__Ye2ymkEqsOvRJvAg79s99KKWrJoahuGvkKzIlxFKHH7fnf2x5C9wIzEBnCGyiUWfnbjGw_Wl_QjtotkGBGVJWwJRAvGtkitRFWZP3BBGG-4ZipxzA_dx7OxwGoIAC7pjkKXXYUKS9fGHvtasYPWL4rLP2cAaoxuHN7Uq93J_DkbvY02wxKBDoDLf57riSisG1vT8ltUr5bOxQBysvFdIshMFpzFFHbHImxy0-lTNRhEOHKpFt__q3J1Yo=s64-no" border="0" style="display:block;" /></td>
                                                                            <td valign="top" width="135" align="center" style="font-size:16px; color:#2a8e9d;"><font face="Arial, sans-serif">CPF: </font></td>
                                                                            <td style="padding:10; color:gray;" width="1" >' . $cpf . '</td>
                                                                            <td width="20"></td>
                                                                            <td style="padding:10; width: 100px; padding-left:40;"  width="135" align="center"><img src="https://lh3.googleusercontent.com/cUDN3isQ1hv6tu53bPy_FvTazlSksJVj6lQjBvywpb9wSEUGYq7Yw-FvwV3ZJia9iI47EoM0TfD9eRUWtqpaG3h6gfM8aJyJYbS1HFlsedeXke3FfR-xZK52Sf-jp08_8jCSH0Dk3jSUuZ6UpRb1dDWzZLjFveuoc5rU6SQnkfMFfh_w3COF2fwQU1VWUhup562XaX7HGgkJDULi19rqPbPCG7s3WRP26oMryDLkO0VHPLaEn03CyxM6R7GFqtIudMAUJ8j5RZlMbQO4RlTdYbkBM7CMdefNJSFEPINkVePDKLQDPnUkWEzH07Qg67dX01bKot4n3KijA8u0oMRVGZiYK61YA56X4termH_TRX4M2K9N6bnwpZRveMtMsIsxZKD6tweptaQHXuuaR5W3xb_geqS0ZIHYj5zZHO5qMNDRfXtfvF8CJhjMmoxQ_vHHukcl9EJTm1EkcFjqTbxiW0p0TKG71m1gE5uU-PUUcA06Y8LLOMGPzjvgwDBJHGcPOhDJ3x0K0XqLF8qTN51EmJzFRcYLSKt35NsQP_OT-zUHP42Co4ZnjN7YilhRSXPg1ek9_AdV9qUSrDRyLUU33DIWnFuLM8y8BEP8_WXA2dEiWyi_kSmcDiwiG8DTOVKVO26LAn_Fx7SVS0SfoLNKDVASmdq76mw=s64-no" border="0" style="display:block;" /></td>
                                                                            <td valign="top" width="135" align="center" style="font-size:16px; color:#2a8e9d;"><font face="Arial, sans-serif">SENHA: </font></td>
                                                                            <td style="padding:10; color:gray;" width="1">' . $senha . '</td>




                                                                            <td width="20"></td>

                                                                        </tr>
                                                                        
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>



                                    </tr>
                                    <tr>
                                                                        ' . $msg . '
                                                                        </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table width="640" border="0" cellspacing="0" cellpadding="0" bgcolor="#b2d6db" class="100p" height="1">
                        <tr>
                            <td width="20" bgcolor="#FFFFFF"></td>
                            <td align="center" height="1" style="line-height:0px; font-size:1px;">&nbsp;</td>
                            <td width="20" bgcolor="#ffffff"></td>
                        </tr>
                    </table>



                    <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                        <tr>
                            <td align="left" width="50%" style="font-size:14px; color:#848484;"><font face="Arial, sans-serif">&copy; IFTM ©2019</font></td>

                        </tr>
                    </table>

                    <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                        <tr>


                            <td align="right" width="50%" style="font-size:14px; color:#848484; "> <div align="center" ><a href="https://www.instagram.com/iftmptu/" target="__blank"><img src="https://lh3.googleusercontent.com/TmRXghisESH7MF9l8LrxEt0iifEfUw5C_UPGpnN5Yog2D_I3KKanDSrvEimloOEHf_TUHTex0JXG2GB2D3pY_lZTQGP597g8hGMemrkVl8pE3iHq3uTLOOPwi6Hsz6_vXdMvSdcF-p4VY2v6J90_udFXKALgCPJbhzYVdQuFhI7pJfHJ5sXbbRzGDsBi-5hJvfVNaW8H4vN3bp4v5j-K2YFjV7GNxdpVLrbO9e4r6MBVr6dYEPdTU0mYGQX4JtSQyAx2Hj5BLPWTrBvQcq83BklDHLUgg-fKh6GyjSxUF2fzw87vskqDpJlutdpvNF0FVp9KEDi8Yb_k8L1aT8_aY9nc9X4L-SUsgMSBz1Uk0XLHJDrqWNff2ApybJCcWs4Uqi-mRGKUps_gXIXM0jOw92uE4kwF0Vza__T-bdhrfz8s84yyQzwli2cdSaiDEwCpPLMkUFglmeJBGdKcYpF_F9EVxlT6jCquKx7C1G9ABkl03iqhpvx-nkkwLN190N3nkStRPxtfeTBc1uz2-6fE6uDh6KnyOtgZ0cHoH46niZFCVEvS4KPYxA6s_Q_vzyt7xOYeF_H4D2wgwUTn063G65ULIfnOsx3HcD1p6AFp9qYgYb2_79vsND8cm98gY0YZ5fCfYl1mNjc_QffjyK0A9QHZnw9pm3VrrVtZRA9urcu-bqh5B78l8KU-N2p8z8oupWsShemAzTg6FktdG_zg3GIY=s85-no" width="50" height="50" style="margin:2%;" /></a><a href="http://www.iftm.edu.br" target="__blank"><img src="https://lh3.googleusercontent.com/urszkwuXzguRFg9LG5sR6zYdWnW7zT9pTMWhbRlRbzfv13LTOSBvmbtCdMmj9R0q_6lpZeMwTVBY9UsOycAW6MSqptLboBh-cxtk1Fm2D4h8mZCGfSm9Sp9wiBZxq9CzII5UG-kWDEQS1eIlMMBr1eTr2KboDCNwMg_y-Is17WSLpuGOLeCEMknJRlCHUS75_6gAJN-GVwrjMT3EjzgC6wQirCmoNruHAgh1g5QgIwzQl17153nY0dJZDPSlxSn_US5S9gu8W1LHNxAsnDtqtFNlkrxCq_unQJTqUJFG-kz5H06-I2f91ra803zy1DPW3w6bH63JzvcnVarPNQImbTYVwuN9BKFPtHHsz-6-5MfSzf0JkiDtRxlLX4eHDlH36ikNZjlQ0J2DL4rtOhAeIJCWe0TkA1WPoxPOVKiPvgfAflb6Bc3_5iSe0zpnlt_605agIzDGG5tlNVpQ0K5FOB4FWitYvuFt1ywGQvYdxmYN1d17fNpnwKWJN3K3TCv2ll6ZQ3pMmGP3NImkDjci8oRi3owVGCDHyG_-jePn2OJILstovnUNTPCiO5oLvA1OlT6yd86h8vzEF6L3UjZ07QEKpynl6-urY-LLlKq_SlQa81i7hE8IxWJxCNGYdSAB1Q_gyPWJIgdILfc79wo2ZnRYDbFLvfI=s225-no" width="50" height="50"style="margin:2%;"  /></a><a href="https://www.facebook.com/IftmCampusParacatu/" target="__blank"><img style="margin:2%;" src="https://lh3.googleusercontent.com/4jWPbxfKAXAd-rndA4SiXBXjUI7055G9qiGeL0n_LWJySFW0DVMt1n8gWFbscB1ClCcerh_4Px98Txyk-3io5UvazoiErn3QrzA9eZk-R32PJlfCFM8cKFmGTBifsVBJZXfO3y4PSRwygqTwDbTtSxNwOptjYm93d5XqaH0eWZRrFDqIq0gCDrpg0lL4ZgKva3OlFiEnGYDC1bTF6KrAmdsg8W1BTg9I2T1Tv3EvdEmUIMzTHT-oX--D7yP9gvQJVh_F-5m-X5e61944F7g7XV9MrGbygftZEli-Lpvq8PKjBW7v0gW_QdF_bkuBNMqD_VLaQQwrRuC29UqWl_FVSNBbxknADu3f5KAAkdRwh9yd8d69Yxwwujvv3M9_7EKTGnL4cDvW-v4NumFdjmyjHinM5N0EN4csQAqo9QaOehKOwYr5t-JGR-NcQJiOxDXtQUOdcv6mxShdKJCeR_ZzY0nfSlJYwTlso19YjEd38VmkEDUp7izYj495cLdjgTg7pod7e6TPMFzTQ6VIVbxWIoHVm5AK-wbWHsPqq-QLCJ1wMNLIHqYEfIm_a7ier0C8zhvF3XvYaGQzh9tAqdInvNrQd2SMM_ot6FG4lqmhQPn0YrbQROUbMIl3_yYWztBdtJcj5sWPciOwMAKHsQ1lJShcTbMFGQY=s85-no" width="50" height="50" /></a>
                                </div></td></tr>
                    </table>

                    <table width="640" class="100p" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="50">

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </body>
</html>', 'text/html')
                ;

                $result = $mailer->send($message);


                echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Técnico cadastrado dados enviados para o e-mail!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                ?>  <script language="javascript">
                    setTimeout(function () {
                        window.location.href = "/painel/cadastro";
                    }, 2000);
                </script>

                <?php

            } else if ($sucesso == 4) {

                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Usuário já existente!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            } else if ($sucesso == 1) {

                echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Algo não deu certo, algum dados já existentes (email, cpf ou siape)!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            }
        } else {

            echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> Algum campo em branco!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
        }
    }

    public function fechaCRUD() {
        if ($this->sessao->get('update') != "") {
            $this->sessao->add('buscaEfetivadaUpdate', 'sucesso');
            $this->sessao->add('buscaEfetivadaRemove', '');
            $this->sessao->add('buscaEfetivadaView', '');
        } elseif ($this->sessao->get('remove') != "") {
            $this->sessao->add('buscaEfetivadaUpdate', '');
            $this->sessao->add('buscaEfetivadaRemove', 'sucesso');
            $this->sessao->add('buscaEfetivadaView', '');
        } elseif ($this->sessao->get('view') != "") {
            $this->sessao->add('buscaEfetivadaUpdate', '');
            $this->sessao->add('buscaEfetivadaRemove', '');
            $this->sessao->add('buscaEfetivadaView', 'sucesso');
        }
    }

    public function buscaEfetivar() {

        $this->redirectSemPermissaoAdmin();

        $cpf = $this->request->get('cpf');

        $permissao = 'administrador';

        if (!empty($cpf)) {

            $usuario = new Usuario();
            $usuario->setCpf($cpf);
            $usuario->setPermissao($permissao);
            $model = new ModelUsuario();

            $sucesso = $model->procuraUsuario($usuario);

            if ($sucesso > 0) {
                echo $msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Achou um usuário com esse CPF!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';


                $this->sessao->add('cpf', $cpf);
                $this->sessao->add('siape', $usuario->getSiape());
                $this->sessao->add('campus', $usuario->getCampus());
                $this->sessao->add('nome', $usuario->getNome());
                $this->sessao->add('telefone', $usuario->getTelefone());
                $this->sessao->add('email', $usuario->getEmail());
                $this->sessao->add('permissao', $usuario->getPermissao());

                $this->fechaCRUD();
                ?>  <script language="javascript">

                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);

                </script>



                <?php

                ;
            } else {
                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Atenção! </strong> CPF não encontrado!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
                //$sucesso = $model->procurarAjudante($usuario);
            }
        }
    }

    public function loginEfetivar() {


        if ($this->sessao->existe('logado')) {
            
        } else {

            echo $this->sessao->get('logado');

            $senha = $this->request->get('senha');

            $cpf = $this->request->get('cpf');

            $data_local = new \DateTime('now', new \DateTimeZone('-0300'));
            $data_ultimo_login = $data_local->format('Y-m-d H:i:s');


            if (!empty($senha) && !empty($cpf)) {


                $usuario = new Usuario();
                $usuario->setSenha($senha);

                $usuario->setCpf($cpf);

                $usuario->setData_ultimo_login($data_ultimo_login);


                $model = new ModelUsuario();

                $resultado = $model->autenticar($usuario);


                $nivel = $usuario->getPermissao();

                if ($resultado == 1 && $nivel == 'tecnico') {

                    //echo $msg = 'sucesso <hr>';

                    $this->sessao->add('tecnico', 'tecnico');
                    $this->sessao->add('logado', 'yes');
                    $this->sessao->add('username', $usuario->getNome());

                    $this->sessao->add('cpfLogado', $usuario->getCpf());
                    $this->sessao->add('nomeLogado', $usuario->getNome());
                    $this->sessao->add('siapeLogado', $usuario->getSiape());
                    $this->sessao->add('emailLogado', $usuario->getEmail());
                    $this->sessao->add('telefoneLogado', $usuario->getTelefone());
                    $this->sessao->add('campusLogado', $usuario->getCampus());
                    $this->sessao->add('senhaLogado', $usuario->getSenha());
                    $this->sessao->add('permissaoLogado', $usuario->getPermissao());


                    $model->senhaSemAlterar($usuario);
                    $primeiroLogin = $usuario->getData_ultimo_login();

                    //echo $primeiroLogin;

                    if ($primeiroLogin == '1000-01-01 00:00:00' || $primeiroLogin == 'null') {
                        ?>  <script language="javascript">
                            window.location.href = "/painel/profile";
                        </script>

                        <?php

                    } else {
                        ?>  <script language="javascript">
                            window.location.href = "/painel/";
                        </script>

                        <?php

                    }
                } else if (($resultado == 1 && $nivel == 'assistente')) {

                    //echo $msg = 'sucesso <hr>';

                    $this->sessao->add('aluno', 'aluno');
                    $this->sessao->add('assistente', 'assistente');
                    $this->sessao->add('logado', 'yes');
                    $this->sessao->add('username', $usuario->getNome());

                    $this->sessao->add('cpfLogado', $usuario->getCpf());
                    $this->sessao->add('nomeLogado', $usuario->getNome());
                    $this->sessao->add('siapeLogado', $usuario->getSiape());
                    $this->sessao->add('emailLogado', $usuario->getEmail());
                    $this->sessao->add('telefoneLogado', $usuario->getTelefone());
                    $this->sessao->add('campusLogado', $usuario->getCampus());
                    $this->sessao->add('senhaLogado', $usuario->getSenha());
                    $this->sessao->add('permissaoLogado', $usuario->getPermissao());
                    $model->ultimoLogin($usuario);
                    ?>  <script language="javascript">
                        window.location.href = "/";
                    </script>

                    <?php

                } else if (($resultado == 1 && $nivel == 'administrador')) {

                    //echo $msg = 'sucesso <hr>';

                    $this->sessao->add('administrador', 'administrador');
                    $this->sessao->add('logado', 'yes');
                    $this->sessao->add('username', $usuario->getNome());
                    $this->sessao->add('cpf', $usuario->getCpf());
                    $this->sessao->add('cpfLogado', $usuario->getCpf());
                    $this->sessao->add('nomeLogado', $usuario->getNome());
                    $this->sessao->add('siapeLogado', $usuario->getSiape());
                    $this->sessao->add('emailLogado', $usuario->getEmail());
                    $this->sessao->add('telefoneLogado', $usuario->getTelefone());
                    $this->sessao->add('campusLogado', $usuario->getCampus());
                    $this->sessao->add('senhaLogado', $usuario->getSenha());
                    $this->sessao->add('permissaoLogado', $usuario->getPermissao());
                    $model->ultimoLogin($usuario);
                    ?>  <script language="javascript">
                        window.location.href = "/painel";
                    </script>

                    <?php

                } else {
                    echo $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> CPF ou senha inválida!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div> <hr>';
                }
            } else {
                echo $msg = '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                
                    <strong>Falha! </strong> Algum campo em branco!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div> <hr>';
            }
        }
    }

    function procurarTecnico() {
        $this->redirect();

        $cpf = $this->request->get('cpf');
        $permissao = 'tecnico';

        if (!empty($cpf)) {
            $usuario = new Usuario();
            $usuario->setCpf($cpf);
            $usuario->setPermissao($permissao);
            $model = new ModelUsuario();
            $sucesso = $model->procurarTecnico($usuario);

            if ($sucesso > 0) {
                echo $msg = '<div class="alert alert-sucess alert-dismissible fade show" role="alert">
                
                    <strong>SUCESSO! </strong> Achou o técnico!!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            }
        }
    }

    public function logoff() {
        $this->sessao->del();
        $redirect = new RedirectResponse('/login');
        $redirect->send();
    }

}
