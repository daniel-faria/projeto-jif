<?php

namespace JIF;

require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use JIF\Util\Sessao;
use JIF\Controller\ControllerIndex;

//require __DIR__.'\..\vendor\swiftmailer\swiftmailer\lib\swift_required.php';



$sessao = new Sessao();
$sessao->start();


//$sessao->add("Nome", 'Chris');
//$sessao->add("Email", 'email@email.com');
//$sessao->add($chave, $valor);

include 'rotas.php';

$loader = new FilesystemLoader(__DIR__ . '/View');
$twig = new Environment($loader);
$twig->addGlobal("sessao", $sessao);



//$twig->addGlobal('mensagem', 'GLOBAL MENSAGEM');
//ultima aula https://symfony.com/doc/current/components/routing.html

$request = Request::createFromGlobals();

$reponse = Response::create();
//$reponse->setContent('qualquer coisa');

$context = new RequestContext();
$context->fromRequest($request);

$matcher = new UrlMatcher($rotas, $context);

try {


    $atributos = $matcher->match($context->getPathInfo());
    //print_r($atributos);
    $controller = $atributos['_controller'];
    $method = $atributos['_method'];

    if (isset($atributos['id'])) {
        $parametros = $atributos['id'];
    } else {
        $parametros = '';
    }

    $obj = new $controller($reponse, $twig, $request, $sessao);
    $obj->$method($parametros);
} catch (ResourceNotFoundException $ex) {

    $endereco = $_SERVER ['REQUEST_URI'];
    $agulha = 'painel';

    $pos = strpos($endereco, $agulha);

    if ($pos === false) {
        
        
        $response = new Response();
        $rot = new ControllerIndex($response, $twig, $request, $sessao);
        $rot->erro404();
        //$response->setStatusCode(Response::HTTP_OK);
        //$response->headers->set('Content-Type', 'text/html');
        
        $response->send();
        
        
    } else {

        $response = new Response();
        $rot = new ControllerIndex($response, $twig, $request, $sessao);
        $rot->erro404AdminPage();
        //$response->setStatusCode(Response::HTTP_OK);
        //$response->headers->set('Content-Type', 'text/html');
        
        $response->send();
    }
}
//Enviar
//$reponse = Response::create();
//$reponse->setContent('qualquer coisa');
//$reponse->send();
$reponse->send();
//Como funciona o ID 
//print_r($request->query->get('id','35'));
//print_r($request->getPathInfo());
//print_r($request->getContent());
//echo "<h1> Olá do Bootstrap</h1>";
//print_r($_SERVER);