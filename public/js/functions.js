$(document).ready(function () {
    $("#loginEfetivar").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/login/efetivar',
            data: $("#loginEfetivar").serializeArray(),

            success: function (json) {

                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        });
    });
});


$(document).ready(function () {
    $('#showPassword').on('click', function () {

        var passwordField = $('#senha');

        var passwordFieldType = passwordField.attr('type');


        if (passwordFieldType === 'password')
        {
            passwordField.attr('type', 'text');



            $(this).val('Esconder');
        } else {

            passwordField.attr('type', 'password');
            $(this).val('Mostrar');
        }
    });
});


$(document).ready(function () {
    $('#showPassword2').on('click', function () {

        var passwordField = $('#atual');

        var passwordFieldType = passwordField.attr('type');


        if (passwordFieldType === 'password')
        {
            passwordField.attr('type', 'text');



            $(this).val('Esconder');
        } else {

            passwordField.attr('type', 'password');
            $(this).val('Mostrar');
        }
    });
});

$(document).ready(function () {
    $("#modalidade").change(function () {
        $("#nome").val($("#modalidade option:selected").text());

    });
});



/*$(document).ready(function () {
 
 
 $("#modalidade").change(function () {
 var sex = $("#modalidade option:selected").text();
 //var combo = document.getElementById("sexo");
 //var tit = sex.replace('Masculino', '');
 // var titulo = tit.replace('Feminino', '');
 var n = sex.search("Masculino");
 
 if(n === -1){
 n = "Feminino"; 
 
 
 
 $("#sexo").val('Masculino').change();
 
 
 
 }
 else{ 
 n = "Masculino";
 
 $("#sexo").val("Masculino");
 }
 
 
 
 });
 });*/



$(document).ready(function () {
    $("#cpf").mask("000.000.000-00");
    $("#ra").mask("00000000000-0");
});


$(document).ready(function () {

    // ##################################################################################################################
    // ##### Sample2 - inline elements / HREF #####
    // ##################################################################################################################
    jQuery("#nanoGallery2").nanoGallery({thumbnailWidth: 160, thumbnailHeight: 160,

        thumbnailHoverEffect: [{'name': 'scaleLabelOverImage', 'duration': 300}, {'name': 'borderLighter'}],
        colorScheme: 'clean',
        locationHash: false,
        thumbnailLabel: {display: true, position: 'overImageOnTop', align: 'center'},
        viewerDisplayLogo: true
    });

});

/**
 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
 var disqus_config = function () {
 this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
 this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
 };
 */
var disqus_config = function () {
    this.language = "pt";
};

(function () { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');

    s.src = 'https://https-www-jiftm-com.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
})();

$(document).ready(function () {
    $('#atualizarAluno').submit(function (evt) {
        evt.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            type: 'POST',
            url: '/painel/aluno/atualizar/efetivar',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});

$(document).ready(function () {
    $('#gravarPostagem').submit(function (evt) {
        evt.preventDefault();

        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        ;

        var formData = new FormData(this);

        //var comment = $.trim($("#editTextPost").val());

        //formData.append("nossa",comment);
        //alert(formData);
        //console.log(formData);

        $.ajax({
            type: 'POST',
            url: '/gravar/postagem',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});

$(document).ready(function () {
    $('#postatualiza').submit(function (evt) {
        evt.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        ;
        var myForm = document.getElementById('postatualiza');
        formData = new FormData(myForm);

        //var formData = new FormData(this);

        $.ajax({
            type: 'POST',
            url: '/alterar/postagem',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});

$(document).ready(function () {
    $('#gravarAluno').submit(function (evt) {
        evt.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            type: 'POST',
            url: '/painel/aluno/gravar',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});


$(document).ready(function () {
    $('#gravarJogo').submit(function (evt) {
        evt.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            type: 'POST',
            url: '/jogo/novo',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});

$(document).ready(function () {
    $('#gerarCracha').submit(function (evt) {
        evt.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            type: 'POST',
            url: '/painel/gerarCrachas',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});

/*var form = document.getElementById('#enviarSumula');
 form.onsubmit = function () {
 var formData = new FormData(form);
 
 formData.append('file', file);
 
 var xhr = new XMLHttpRequest();
 // Add any event handlers here...
 xhr.open('POST', form.getAttribute('action'), true);
 xhr.send(formData);
 
 return false; // To avoid actual submission of the form
 }*/


$(document).ready(function () {
    $('#enviarSumula').submit(function (evt) {
        evt.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '/jogo/sumula/enviar',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});

$(document).ready(function () {
    $('#enviarSumul').submit(function (evt) { //alterarpra sumula
        evt.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            type: 'POST',
            url: '/jogo/sumula/enviar',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});


// Scroll to top button appear
$(document).on('scroll', function () {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
        $('.scroll-to-top').fadeIn();
    } else {
        $('.scroll-to-top').fadeOut();
    }
});

// Smooth scrolling using jQuery easing
$(document).on('click', 'a.scroll-to-top', function (e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
});

$(document).ready(function () {
    $('#gravarFoto').submit(function (evt) {
        evt.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            type: 'POST',
            url: '/fotos/gravar',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});


$(document).ready(function () {
    $('#buscarCracha').submit(function (evt) {
        evt.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            type: 'POST',
            url: '/cracha/search',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }
        });
    });
});
/*
$(document).ready(function () {
    $("#removePostagem").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/postagem/excluir',
            data: $("#removePostagem").serializeArray(),

            success: function (json) {

                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        });
    });
});*/

$(document).ready(function () {
    $("#buscarAluno").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/painel/aluno/busca/efetivar',
            data: $("#buscarAluno").serializeArray(),

            success: function (json) {

                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        });
    });
});


$(document).ready(function () {
    $("#formBusca").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/painel/busca/efetivar',
            data: $("#formBusca").serializeArray(),

            success: function (json) {

                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        });
    });
});

$(document).ready(function () {
    $("#inserirTecnicoAdmin").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/painel/cadastro/inserir',
            data: $("#inserirTecnicoAdmin").serializeArray(),

            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        });
    });
});

$(document).ready(function () {
    $("#atualizaTecnicoAdmin").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/painel/update/efetivar',
            data: $("#atualizaTecnicoAdmin").serializeArray(),

            success: function (json) {
                $("#div_retorno2").html(json);
            },
            beforeSend: function () {
                $("#processando2").css({display: "block"});
            },
            complete: function () {
                $("#processando2").css({display: "none"});
            },
            error: function () {
                $("#div_retorno2").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno2").css({display: "none"});
                }, 5000);
            }

        });
    });
});

$(document).ready(function () {
    $("#alteraSenhaAndDados").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/painel/profile/efetivar',
            data: $("#alteraSenhaAndDados").serializeArray(),

            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        });
    });
});

function fechar() {
    $('#change').modal('hide');
    $('#confirmar').modal('hide');
    $('#remover').modal('hide');
    $('#remove').modal('hide');
    $('#delete').modal('hide');
}
;

$(document).ready(function () {
    $("#removerdoTime").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/painel/time/removeralunos',
            data: $("#removerdoTime").serializeArray(),

            success: function (json) {

                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        }),
                $("#delete").removeClass("in");
        $(".modal-backdrop").remove();
        $('body').removeClass('modal-open');
        $('body').css('padding-right', '');
        $("#delete").hide();

    });
});
$(document).ready(function () {
    $("#salvaTime").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/painel/time/salva',
            data: $("#salvaTime").serializeArray(),

            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        });

    });
});

$(document).ready(function () {
    $("#deletarTime").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/painel/time/delete',
            data: $("#deletarTime").serializeArray(),

            success: function (json) {

                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        }),
                $("#deleteModal").removeClass("in");
        $(".modal-backdrop").remove();
        $('body').removeClass('modal-open');
        $('body').css('padding-right', '');
        $("#deleteModal").hide();

    });
});

$(document).ready(function () {
    const checkboxes = document.querySelectorAll('input[type="checkbox"]');

    let inBetween = false;
    let lastChecked;

    function handleCheck(e) {

        if (e.shiftKey && this.checked) {
            checkboxes.forEach(checkbox => {

                if (checkbox === this || checkbox === lastChecked) {
                    inBetween = !inBetween;
                }
                if (inBetween) {
                    checkbox.checked = true;
                }
            });
        }

        lastChecked = this;
    }
    checkboxes.forEach(checkbox => checkbox.addEventListener('click', handleCheck));
});

$(document).ready(function () {
    const checkboxes = document.querySelectorAll('.table input[type="checkbox"]');

    let inBetween = false;
    let lastChecked;

    function handleCheck(e) {

        if (e.shiftKey && this.checked) {
            checkboxes.forEach(checkbox => {

                if (checkbox === this || checkbox === lastChecked) {
                    inBetween = !inBetween;
                }
                if (inBetween) {
                    checkbox.checked = true;
                }
            });
        }

        lastChecked = this;
    }
    checkboxes.forEach(checkbox => checkbox.addEventListener('click', handleCheck));
});


function marcar() {
    $('.check').each(function () {
        if (this.checked)
            this.checked = true;
        else
            this.checked = true;
    });
}
function desmarcar() {
    $('.check').each(function () {
        if (this.checked)
            this.checked = false;
        else
            this.checked = false;
    });
}


function marcar2() {
    $('.check2').each(function () {
        if (this.checked)
            this.checked = true;
        else
            this.checked = true;
    });
}
function desmarcar2() {
    $('.check2').each(function () {
        if (this.checked)
            this.checked = false;
        else
            this.checked = false;
    });
}


$(document).ready(function () {

    $('#formarTime').on('submit', function (e) {
        // Prevent actual form submission
        e.preventDefault();

        // Serialize form data

        //var data = $("#formarTime").serializeArray();
        // modalidade = document.getElementById("modalidade");
        // nomeTime = document.getElementById("nomeTime");

        //sessionStorage.setItem("modalidadeTime", modalidade);
        //sessionStorage.setItem("nomeTime", nomeTime);

        // Include extra data if necessary
        //data.push({'nomeTime': 'nomeTime', 'modalidade': 'modalidade'});
        //data.push({data: $("#formarTime").serializeArray()});

        // Submit form data via Ajax
        $.post({
            url: '/painel/time/formar',
            data: $("#formarTime").serializeArray(),
            success: function (json) {

                $("#div_retorno").html(json);
                //setTimeout(function () {
                //     window.location.reload();
                // }, 2500);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        });
    });
});




$(document).ready(function () {


    $('#addAlunoTime').on('submit', function (e) {
        // Prevent actual form submission
        e.preventDefault();

        // Serialize form data


        //var data = $("#formarTime").serializeArray();
        // modalidade = document.getElementById("modalidade");
        // nomeTime = document.getElementById("nomeTime");

        //sessionStorage.setItem("modalidadeTime", modalidade);
        //sessionStorage.setItem("nomeTime", nomeTime);

        // Include extra data if necessary
        //data.push({'nomeTime': 'nomeTime', 'modalidade': 'modalidade'});
        //data.push({data: $("#formarTime").serializeArray()});

        // Submit form data via Ajax
        $.post({
            url: '/painel/time/studentadd',
            data: $("#addAlunoTime").serializeArray(),
            success: function (json) {

                $("#div_retorno").html(json);
                //setTimeout(function () {
                //     window.location.reload();
                // }, 2500);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        });
    });
});





$(document).ready(function () {
    $("#removeAluno").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/painel/aluno/remove/efetivar',

            data: $("#removeAluno").serializeArray(),

            success: function (json) {

                $("#div_retorno2").html(json),
                        setTimeout(function () {
                            window.location.reload();
                        }, 2500);
            },
            beforeSend: function () {
                $("#processando2").css({display: "block"});
            },
            complete: function () {
                $("#processando2").css({display: "none"});
            },
            error: function () {
                $("#div_retorno2").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno2").css({display: "none"});
                }, 5000);
            }
        }),
                $("#modal").removeClass("in");
        $(".modal-backdrop").remove();
        $('body').removeClass('modal-open');
        $('body').css('padding-right', '');
        $("#modal").hide();


    });
});


$(document).ready(function () {
    $("#removeModalidade").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/painel/modalidade/remove/',
            data: $("#removeModalidade").serializeArray(),

            success: function (json) {

                $("#div_retorno").html(json);

            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }
        })


    });
});





$(document).ready(function () {
    $("#removeTecnicoAdmin").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/painel/remove/efetivar',
            data: $("#removeTecnicoAdmin").serializeArray(),

            success: function (json) {

                $("#div_retorno2").html(json),
                        setTimeout(function () {
                            window.location.reload();
                        }, 2500);
            },
            beforeSend: function () {
                $("#processando2").css({display: "block"});
            },
            complete: function () {
                $("#processando2").css({display: "none"});
            },
            error: function () {
                $("#div_retorno2").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno2").css({display: "none"});
                }, 5000);
            }
        })


    });
});



$(document).ready(function () {
    $("#formCadastroTecnico").submit(function (e) {
        e.preventDefault();
        $.ajax({

            type: 'POST',
            url: '/cadastro/efetivar',
            data: $("#formCadastroTecnico").serializeArray(),

            success: function (json) {
                $("#div_retorno").html(json);
            },
            beforeSend: function () {
                $("#processando").css({display: "block"});
            },
            complete: function () {
                $("#processando").css({display: "none"});
            },
            error: function () {
                $("#div_retorno").html("Erro em chamar a função.");
                setTimeout(function () {
                    $("#div_retorno").css({display: "none"});
                }, 5000);
            }

        });
    });
});

