﻿// Call the dataTables jQuery plugin



        $(document).ready(function () {

    $('#dataTable').DataTable({
        "scrollX": true,
        "oLanguage": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Filtrar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    }),
            $('#dataTable').css('display', 'table');
    $(".dataTables_filter").hide();

    var table = $('#tabelaAlunos').DataTable({

        pageLength: 10,
        //"scrollX": true,
        "lengthChange": false,
        "lengthMenu": [[-1], ["Todos"]],
        "pageLength": -1,
        "bPaginate": false,
        "searching": true,
        "oLanguage": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Filtrar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });

    $('#bookSearch').keyup(function () {
        //table.search($(this).val()).draw();

        var sex = $("#modalidade option:selected").text();


        if (sex === 'Selecione...') {
            n = '';
        } else {
            var n = sex.search("Masculino");

        }

        if (n === -1) {

            str1 = "Feminino";

        } else if (n === "") {

            str1 = " ";

        } else if (n > 0) {

            str1 = "Masculino";
        }

        var res = str1 + ' ' + $(this).val();


        table.search(res).draw();


        /* val = $("#sexo option:selected").text();
         val = "Masculino"
         
         console.log(val);
         table.search(val ? '^' + $(this).val() + '$' : '', true, false)
         .draw();*/

    });

    $('#bookSearchSexo').keyup(function () {
        //table.search($(this).val()).draw();

        var sex = $("#modalidade").val();




        var n = sex.search("Masculino");


        if (n === -1) {

            str1 = "Feminino";

        } else if (n > 0) {

            str1 = "Masculino";
        }

        var res = str1 + ' ' + $(this).val();
        console.log(res);
        table.search(res).draw();

    });

    /*$('#modalidade').on('change', function(){
     
     
     
     // If you want totally refresh the datatable use this 
     table.ajax.reload();
     
     // If you want to refresh but keep the paging you can you this
     table.ajax.reload( null, false );
     });*/

    $("#tabelaAlunos tfoot th").each(function (i) {

        var url_atual = window.location.pathname;

        if (url_atual === "/painel/time/edit/show") {
            var sex = $("#modalidade").val();
            var n = sex.search("Masculino");
            var str1 = '';

            if (n === -1) {

                str1 = "Feminino";

            } else if (n > 0) {

                str1 = "Masculino";
            }

            table.search(str1).draw();
        }


        var column = this;

        var select = $('<select id="sexo" name="sexo" class="sexo form-control" style=" width:200px; display:none; margin-right:-160px;  " ><option value="">Selecione o Sexo</option></select>')
                .appendTo($(this).empty())
                .on('change', function () {
                    var val = $(this).val();

                    table.column(i)
                            .search(val ? '^' + $(this).val() + '$' : val, true, false)

                            .draw();
                });

        table.column(i).data().unique().sort().each(function (d, j) {


            select.append('<option value="' + d + '">' + d + '</option>')

			
            $("#modalidade").change(function () {

                var sex = $("#modalidade option:selected").text();


                //var combo = document.getElementById("sexo");
                //var tit = sex.replace('Masculino', '');
                // var titulo = tit.replace('Feminino', '');

                if (sex === 'Selecione...') {
                    n = '';
                } else {
                    var n = sex.search("Masculino");

                }


                if (n === -1 && "Feminino" === d) {

                    //select.append('<option SELECTED value="' + d + '">' + d + '</option>')

                    $("#sexo option[value='Feminino']").prop("selected", true);

                    n = "Feminino";
                    /* $("#sexo option[value='Feminino']").click();
                     $("select#elem").get(0).selectedIndex = 0;*/


                    /*table
                     .search(n)
                     
                     .draw();*/

                    table
                            .search('Feminino')
                            .draw();




                } else if (n === "") {


                    n = "";

                    // select.append('<option value="' + d + '">' + d + '</option>')
                    $("#sexo option[value='Feminino']").prop("selected", false);

                    $("#sexo option[value='Masculino']").prop("selected", false);

                    //$("#sexo option:selected").click();

                    table
                            .search("")

                            .draw();

                    /* table.columns().every(function () {
                     var that = this;
                     
                     $('select', this.footer()).on('click', function () {
                     if (that.search() !== this.value) {
                     that
                     .search(this.value)
                     .draw();
                     }
                     });
                     });*/

                } else if (n > 0 && "Masculino" === d) {
                    n = "Masculino";

                    //select.append('<option SELECTED value="' + d + '">' + d + '</option>')

                    $("#sexo option[value='Masculino']").prop("selected", true);


                    table
                            .search("Masculino")

                            .draw();

                    /*table.columns().every(function () {
                     var that = this;
                     
                     $("#se").change(function () {
                     if (that.search() !== this.value) {
                     that
                     .search(this.value)
                     .draw();
                     }
                     });
                     });*/


                }







                /*$('#sexo').keyup(function () {
                 table.draw();
                 });*/

                /*$(table).ready(function () {
                 column.search( n ? '^'+n+'$' : '', true, false ).draw();
                 //table.search( n ).draw();
                 });*/





            });

        });


    });




});
