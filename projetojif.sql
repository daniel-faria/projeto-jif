--CREATE DATABASE  IF NOT EXISTS `projetojif` /*!40100 DEFAULT CHARACTER SET latin1 */;
--USE `projetojif`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: projetojif
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aluno`
--

DROP TABLE IF EXISTS `aluno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aluno` (
  `ra` varchar(15) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `data_nascimento` date NOT NULL,
  `sexo` varchar(2) NOT NULL,
  `rg` varchar(20) NOT NULL,
  `campus` varchar(50) NOT NULL,
  `telefone` varchar(45) NOT NULL,
  `foto` varchar(200) NOT NULL,
  PRIMARY KEY (`ra`),
  UNIQUE KEY `rg` (`rg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aluno`
--

LOCK TABLES `aluno` WRITE;
/*!40000 ALTER TABLE `aluno` DISABLE KEYS */;
INSERT INTO `aluno` VALUES ('10201034649','Jesus','2000-09-16','m','1111111172','Paracatu','(11) 11111-1115','10201034649PrimeiraImagemJPG.jpg'),('10201034650','Jesus','2000-09-16','m','1111111125','Paracatu','(11) 11111-1115','10201034650PrimeiraImagemJPG.jpg'),('10201034651','Jesus','2000-09-16','m','1111111115','Paracatu','(11) 11111-1115','10201034651PrimeiraImagemJPG.jpg'),('10201034652','Jesus','2000-09-16','m','1111111116','Paracatu','(11) 11111-1115','10201034652PrimeiraImagemJPG.jpg'),('10201034654','Jesus','2000-09-16','m','1111111113','Paracatu','(11) 11111-1115','10201034654PrimeiraImagemJPG.jpg'),('10201034656','Jesus','2000-09-16','m','1111111112','Paracatu','(11) 11111-1115','10201034656PrimeiraImagemJPG.jpg'),('10201034674','Jesus','2000-09-16','m','1111111986','Paracatu','(11) 11111-1115','10201034674PrimeiraImagemJPG.jpg'),('10201034675','Jesus','2000-09-16','m','1111111987','Paracatu','(11) 11111-1115','10201034675PrimeiraImagemJPG.jpg'),('10201034678','Jesus','2000-09-16','m','1111111989','Paracatu','(11) 11111-1115','10201034678PrimeiraImagemJPG.jpg'),('10201034698','11113131','2000-09-16','m','123123123123','Paracatu','(11) 11111-1115','10201034698PrimeiraImagemJPG.jpg'),('11111111111','TESTE','2000-11-11','m','1111111111','Paracatu','(38) 99724-8602','11111111111thumb-1920-19177.jpg'),('12345678910','Kano','2005-05-16','m','1111111114','Paracatu','(11) 11111-1115','12345678910PrimeiraImagemJPG.jpg'),('12345678911','Rodinei','2001-02-16','m','12345678911','Paracatu','(11) 11111-1114','12345678911PrimeiraImagemJPG.jpg'),('12345678912','Rodinei','2005-09-16','m','5825223','Paracatu','(11) 11111-1115','12345678912PrimeiraImagemJPG.jpg');
/*!40000 ALTER TABLE `aluno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jogo`
--

DROP TABLE IF EXISTS `jogo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jogo` (
  `dataHoraInicio` datetime NOT NULL,
  `dataHoraFim` datetime NOT NULL,
  `local` varchar(100) NOT NULL,
  `Time_idTime` int(11) NOT NULL,
  `Time_idTime1` int(11) NOT NULL,
  `idJogo` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idJogo`),
  KEY `fk_jogo_Time2_idx` (`Time_idTime1`),
  KEY `fk_jogo_Time1` (`Time_idTime`),
  CONSTRAINT `fk_jogo_Time1` FOREIGN KEY (`Time_idTime`) REFERENCES `time` (`idTime`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_jogo_Time2` FOREIGN KEY (`Time_idTime1`) REFERENCES `time` (`idTime`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jogo`
--

LOCK TABLES `jogo` WRITE;
/*!40000 ALTER TABLE `jogo` DISABLE KEYS */;
/*!40000 ALTER TABLE `jogo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modalidade`
--

DROP TABLE IF EXISTS `modalidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modalidade` (
  `idModalidade` int(11) NOT NULL AUTO_INCREMENT,
  `esporte` varchar(45) NOT NULL,
  `coletivo` tinyint(1) NOT NULL,
  `qtdAtleta` int(11) NOT NULL,
  `masculino` tinyint(1) NOT NULL,
  PRIMARY KEY (`idModalidade`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modalidade`
--

LOCK TABLES `modalidade` WRITE;
/*!40000 ALTER TABLE `modalidade` DISABLE KEYS */;
INSERT INTO `modalidade` VALUES (16,'teste',1,12,1),(17,'Futebol',1,12,1);
/*!40000 ALTER TABLE `modalidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `postagem`
--

DROP TABLE IF EXISTS `postagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postagem` (
  `idpostagem` int(11) NOT NULL,
  `dataHora` datetime NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `texto` longtext NOT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `Usuario_cpf` varchar(15) NOT NULL,
  `aprovada` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idpostagem`),
  KEY `fk_postagem_Usuario1_idx` (`Usuario_cpf`),
  CONSTRAINT `fk_postagem_Usuario1` FOREIGN KEY (`Usuario_cpf`) REFERENCES `usuario` (`cpf`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `postagem`
--

LOCK TABLES `postagem` WRITE;
/*!40000 ALTER TABLE `postagem` DISABLE KEYS */;
/*!40000 ALTER TABLE `postagem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `time`
--

DROP TABLE IF EXISTS `time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time` (
  `idTime` int(11) NOT NULL AUTO_INCREMENT,
  `placar` int(11) DEFAULT NULL,
  `nomeTime` varchar(50) NOT NULL,
  `Tecnico_cpf` varchar(15) NOT NULL,
  `Modalidade_idModalidade` int(11) NOT NULL,
  PRIMARY KEY (`idTime`),
  KEY `fk_Time_Tecnico1_idx` (`Tecnico_cpf`),
  KEY `fk_Time_Modalidade1_idx` (`Modalidade_idModalidade`),
  CONSTRAINT `fk_Time_Modalidade1` FOREIGN KEY (`Modalidade_idModalidade`) REFERENCES `modalidade` (`idModalidade`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Time_Tecnico1` FOREIGN KEY (`Tecnico_cpf`) REFERENCES `usuario` (`cpf`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time`
--

LOCK TABLES `time` WRITE;
/*!40000 ALTER TABLE `time` DISABLE KEYS */;
INSERT INTO `time` VALUES (15,NULL,'Futsal Paracatu','102.010.346-96',17);
/*!40000 ALTER TABLE `time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `time_has_aluno`
--

DROP TABLE IF EXISTS `time_has_aluno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_has_aluno` (
  `time_idTime` int(11) NOT NULL,
  `aluno_ra` varchar(15) NOT NULL,
  PRIMARY KEY (`time_idTime`,`aluno_ra`),
  KEY `fk_time_has_aluno_aluno1_idx` (`aluno_ra`),
  KEY `fk_time_has_aluno_time1_idx` (`time_idTime`),
  CONSTRAINT `fk_time_has_aluno_aluno1` FOREIGN KEY (`aluno_ra`) REFERENCES `aluno` (`ra`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_time_has_aluno_time1` FOREIGN KEY (`time_idTime`) REFERENCES `time` (`idTime`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time_has_aluno`
--

LOCK TABLES `time_has_aluno` WRITE;
/*!40000 ALTER TABLE `time_has_aluno` DISABLE KEYS */;
/*!40000 ALTER TABLE `time_has_aluno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `cpf` varchar(15) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `siape` varchar(15) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `campus` varchar(50) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `senha` varchar(60) NOT NULL,
  `permissao` varchar(45) NOT NULL,
  `data_ultimo_login` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `data_cadastro` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`cpf`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `cpf` (`cpf`),
  UNIQUE KEY `siape` (`siape`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('102.010.346-96','Daniel','1111113','danielfariaptu@gmail.com','Paracatu','(11) 1111-11115','$2y$10$HTdx20S55zteQe1rw6Zo1OTGg9krkczs.BcAgRI/ymN.nwq77IOH2','tecnico','2019-05-06 03:45:46','2019-05-03 01:52:09'),('102.010.346-97','Administrador',NULL,'danielfari4@gmail.com','','38997248602','$2y$12$h4kBKVbcxucI0lq0fsTdqu8dH7dfxjbRV0Hf95zNuomCoFTtmM76y','administrador','2019-04-16 05:32:59','1000-01-01 00:00:00'),('111.111.111-11','teste','1111111','teste@gmail.com','Paracatu','(38) 99724-8602','$2y$12$h4kBKVbcxucI0lq0fsTdqu8dH7dfxjbRV0Hf95zNuomCoFTtmM76y','tecnico','1000-01-01 00:00:00','2019-05-03 02:26:10'),('313.131.313-13','teste1','1111231','13131313@2313A.AD','Uberaba','(11) 1111-11115','$2y$12$h4kBKVbcxucI0lq0fsTdqu8dH7dfxjbRV0Hf95zNuomCoFTtmM76y','tecnico','1000-01-01 00:00:00','2019-04-21 02:00:03');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-06 11:57:56
