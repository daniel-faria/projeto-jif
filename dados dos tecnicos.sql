-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 24-Mar-2019 às 04:14
-- Versão do servidor: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projeto-jif`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tecnico`
--

DROP TABLE IF EXISTS `tecnico`;
CREATE TABLE IF NOT EXISTS `tecnico` (
  `cpf` varchar(15) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `siape` varchar(15) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `campus` varchar(50) NOT NULL,
  `telefone` varchar(45) NOT NULL,
  PRIMARY KEY (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tecnico`
--

INSERT INTO `tecnico` (`cpf`, `nome`, `siape`, `email`, `campus`, `telefone`) VALUES
('024.373.746-71', 'Karla Queiroz Gontijo', NULL, '', 'Patos de Minas', ''),
('034.172.095-01', 'Carolina Drumond Porto Carreiro Caldas', NULL, '', 'Ituiutaba', ''),
('045.570.956-40', 'Fernanda Arantes Moreira', NULL, '', 'Uberlândia Fazenda', ''),
('046.282.646-51', 'Karina Estela Costa', '1872967', '', 'Uberlândia Centro', ''),
('062.045.486-54', 'Cassiano Orion Canaverde Beleti', NULL, '', 'Ituiutaba', ''),
('081.358.106-01', 'Juscélia Cristina Pereira', NULL, '', 'Paracatu', ''),
('222.992.032-49', 'Paulo Henrique Soares de Carvalho', NULL, '', 'Uberaba', ''),
('396.632.578-06', 'Natalia Papacidero Magrin', NULL, '', 'Avançado Uberaba Parque Tecnológico', ''),
('433.325.871-53', 'Elaine Faria da Silva', NULL, '', 'Uberlândia Fazenda', ''),
('680.921.306-63', 'Divaldo Soares de Oliveira', '2220177', '', 'Patrocínio', ''),
('951.474.036-04', 'Firmiano Alexandre dos Reis Silva', NULL, '', 'Campina Verde', '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
